/**
 * 
 */
package com.td.em.qa.domain;

import com.td.em.qa.constants.*

/**
 * @author Jin Chen
 *
 */
class DaemonGroup extends EcosystemConfigurationObject{
    HaPattern haPattern;
    List<HaTransition> validHaTransitions;
    ExecutableName executableName;
    String otherInput;
    String executeUser;
    String executePassword;
    IdentifyDaemonsBy identifyDaemonsBy;
    String globalSearchPatternValue;
    String stateChangeControlScript;
    List<Daemon> daemons;
}
