/**
 * 
 */
package com.td.em.qa.domain;

import com.td.em.qa.constants.ActionType;

/**
 * @author Jin Chen
 *
 */
public class EMActionES extends Action {
    Boolean executionVisibleInExplorerJobView;
    Boolean outputDownloadable;
    String executeHost;
    String executeUsername;
    String executePassword;
    String executeScript;
    String executeParams;
    final ActionType actionType=ActionType.ES;
}
