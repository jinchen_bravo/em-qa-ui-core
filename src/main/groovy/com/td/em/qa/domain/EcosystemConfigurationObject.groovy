/**
 * 
 */
package com.td.em.qa.domain

/**
 * @author Jin Chen
 *
 */
class EcosystemConfigurationObject {
    String name
    EcosystemConfigurationObject withName(String name){
        this.name=name;
        return this;
    }
}
