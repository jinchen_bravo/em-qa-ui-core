/**
 * 
 */
package com.td.em.qa.domain

import com.td.em.qa.constants.DaemonState

/**
 * @author Jin Chen
 *
 */
class Daemon extends EcosystemConfigurationObject{
    String server;
    DaemonState currentState;
    String daemonSearchPatternValue;
    String customSearchPatternValue;
}
