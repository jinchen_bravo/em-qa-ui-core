package com.td.em.qa.domain;

import com.td.em.qa.constants.ServerType

class Server extends EcosystemConfigurationObject{
    String dataCenter;
    String scriptExecUser;
    String scriptExecPwd;
    ServerType serverType;
}
