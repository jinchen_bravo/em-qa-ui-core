/**
 * 
 */
package com.td.em.qa.domain

import com.td.em.qa.constants.ActionType

/**
 * @author Jin Chen
 *
 */
class Action extends EcosystemConfigurationObject{
    Boolean enabled;
    ActionType actionType;
}
