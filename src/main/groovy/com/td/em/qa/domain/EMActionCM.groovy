/**
 * 
 */
package com.td.em.qa.domain

import com.td.em.qa.constants.ActionType;

/**
 * @author Jin Chen
 *
 */
class EMActionCM extends Action {
    TDAlertActionSet teradataAlertsAction
    String textSubject
    String textBody
    final ActionType actionType=ActionType.CM;
}
