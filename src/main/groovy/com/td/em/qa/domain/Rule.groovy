/**
 * 
 */
package com.td.em.qa.domain

import com.td.em.qa.constants.Severity

/**
 * @author Jin Chen
 *
 */
class Rule {
    List<Action> actions
    Severity severity
    String alertcode
}
