/**
 * 
 */
package com.td.em.qa.pages

import groovy.util.logging.Slf4j
import com.td.em.qa.utils.BrowserUtils;
import com.td.em.qa.utils.HtmlWebTable
import com.google.common.collect.ByFunctionOrdering;
import com.td.em.qa.constants.*
import com.td.em.qa.domain.DaemonGroup

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.How
import org.openqa.selenium.support.PageFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * @author Jin Chen
 *
 */
@Slf4j(value="logger")
abstract class AbstractDaemonGroupPage implements EcosystemConfigurationObjectPage {
    protected WebDriver webdriver;

    @FindBy(how = How.CSS, using = "div.formThrobber")
    protected List<WebElement> formThrobber;

    @FindBy(how = How.CSS, using = "button.v-button.add-content-cancel")
    protected WebElement closeButton;

    @FindBy(how = How.ID, using ="drillBackArrow")
    protected WebElement drillBackArrow

    @FindBy(how = How.CSS, using = "div.dottedSection:nth-of-type(2) div.jquery-selectbox")
    protected WebElement haPattern;

    protected By byHaPattern = By.cssSelector("div.dottedSection:nth-of-type(2) div.jquery-selectbox")

    @FindBy(how = How.CSS, using = "div.dottedSection:nth-of-type(3) table")
    protected WebElement validHaTransitionsTable

    @FindBy(how = How.ID, using = "dType")
    protected WebElement executableName;

    @FindBy(how = How.ID, using = "otherInput")
    protected WebElement otherInput;

    @FindBy(how = How.ID, using = "execUser")
    protected WebElement execUser;

    @FindBy(how = How.ID, using = "execUserPass")
    protected WebElement execUserPass;

    @FindBy(how = How.ID, using = "globalSearchPatternValue")
    protected WebElement globalSearchPatternValue;

    @FindBy(how = How.ID, using = "stateChangeControlScript")
    protected WebElement stateChangeControlScript;
    // the plus sign at the right bottom of the page to add the daemon
    @FindBy(how = How.CSS, using = "div[title='Add Daemon']")
    protected WebElement addDaemonButton;

    @FindBy(how = How.ID, using = "applyBttn")
    protected WebElement applyButton;

    protected By selectMoreButton = By.cssSelector("div.jquery-selectbox-moreButton")
    protected By selectSubLocator=By.cssSelector("div.jquery-selectbox-list span")

    public AbstractDaemonGroupPage(WebDriver d) {
        BrowserUtils.waitJqueryCallDone(d)
        this.webdriver = d;
        PageFactory.initElements(d, this)
    }

    protected void ifNull(def n){
        if (null==n)
            throw new NullPointerException(n + " can NOT be Null!!!")
    }

    private void clearpHaTransitions(){
        BrowserUtils.waitForElementsInvisible(webdriver,formThrobber,20);
        List<WebElement> rows = validHaTransitionsTable.findElements(By.tagName("tr"))
        for(WebElement t:rows){
            if(t.isDisplayed()){
                WebElement checkbox = t.findElement(By.cssSelector("input"))
                if(checkbox.isSelected()&&checkbox.isEnabled())
                    checkbox.click()
            }
        }
    }

    protected void choseValidHaTransitions(List<HaTransition> transitions){
        clearpHaTransitions()
        HtmlWebTable wt = new HtmlWebTable(validHaTransitionsTable)
        for(HaTransition t:transitions){
            WebElement checkbox = wt
                    .getCell(wt.getIndexOfRowWithColumnText(1,t.toString()),0)
                    .findElement(By.cssSelector("input"))
            if(!checkbox.isSelected())
                checkbox.click()
            else{logger.warn("Check box "+t.toString()+" already checked")}
        }
    }

    protected void addDaemons(DaemonGroup dg , WebElement scrollElement){
        logger.info("Start to add daemons...")
        for(int i=0;i<dg.daemons.size();i++){
            this.addDaemonButton.click()
            //click sortableRow to scroll down the page
            scrollElement.click()
            logger.info("Daemon " + dg.daemons.get(i).name + " on Server " + dg.daemons.get(i).server)
            //"div#newDaemonTable div.sortableRow:nth-of-type(2) select.newDaemonServer"
            //"div#newDaemonTable div.sortableRow:nth-of-type(2) select.newDaemonState"
            String x = Integer.toString(i+2)
            String cssServer =
                    "div#newDaemonTable div.dottedSection:nth-of-type(" + x + ") select.newDaemonServer"
            String cssDaemonState =
                    "div#newDaemonTable div.dottedSection:nth-of-type(" + x + ") select.newDaemonState"
            WebElement wserver = webdriver.findElement(By.cssSelector(cssServer))
            WebElement wdms = webdriver.findElement(By.cssSelector(cssDaemonState))
            BrowserUtils
                    .getDropDownOption(wserver,
                    dg.daemons.get(i).server)
                    .click();
            BrowserUtils
                    .getDropDownOptionText(wdms,
                    dg.daemons.get(i).currentState.name())
                    .click();
            if(null!=dg.daemons.get(i).customSearchPatternValue){
                String cssFlagUseCusomeValue =
                        "div#newDaemonTable div.dottedSection:nth-of-type(" + x + ") div.colSearchPaternValue input"
                String cssTextAreaCustomeValue =
                        "div#newDaemonTable div.dottedSection:nth-of-type(" + x + ") div.colSearchPaternValue textarea"
                webdriver.findElement(By.cssSelector(cssFlagUseCusomeValue)).click()
                webdriver.findElement(By.cssSelector(cssTextAreaCustomeValue)).clear()
                webdriver.findElement(By.cssSelector(cssTextAreaCustomeValue)).
                        sendKeys(dg.daemons.get(i).customSearchPatternValue)
            }
        }
    }

    @Override
    abstract def clickApply()

    ManageDaemonGroupsPage clickBack(){
        drillBackArrow.click()
        return new ManageDaemonGroupsPage(webdriver)
    }
}
