package com.td.em.qa.pages

import java.lang.NullPointerException
import java.util.List
import java.util.NoSuchElementException

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver


import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.How
import org.openqa.selenium.support.PageFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory

import com.td.em.qa.domain.Daemon
import com.td.em.qa.domain.DaemonGroup
import com.td.em.qa.utils.BrowserUtils
import com.td.em.qa.utils.WebUtils
import com.td.em.qa.constants.*

import groovy.util.logging.Slf4j

/**
 * @author Jin Chen
 *
 */
@Slf4j(value="logger")
public class AddDaemonGroupPage extends AbstractDaemonGroupPage{
    @FindBy(how = How.ID, using = "id")
    private WebElement id

    @FindBy(how = How.ID, using = "daemonNameField")
    private WebElement daemonNameField

    @FindBy(how = How.CSS, using = "div.dottedSection:nth-of-type(7) div.jquery-selectbox")
    private WebElement identifyDaemonsBy

    private By byIdentifyDaemonBy = By.cssSelector("div.dottedSection:nth-of-type(7) div.jquery-selectbox")

    @FindBy(how = How.CSS, using = "div#newDaemonTable div.appDelete")
    private WebElement appDelete

    @FindBy(how = How.CSS, using = "div.sortableRow")
    private WebElement sortableRow

    @FindBy(how = How.CSS, using = "div.sortableRow>div.colServer")
    private WebElement colServer

    public AddDaemonGroupPage(WebDriver d) {
        super(d)
    }

    public ManageDaemonGroupsPage addDaemonGroup(DaemonGroup dg){
        ifNull(dg.name)
        ifNull(dg.haPattern)
        ifNull(dg.executableName)
        ifNull(dg.executeUser)
        ifNull(dg.executePassword)
        ifNull(dg.identifyDaemonsBy)
        ifNull(dg.globalSearchPatternValue)
        ifNull(dg.daemons)
        this.id.sendKeys(dg.name)

        WebUtils.selectOptionUseCSS(webdriver,
                byHaPattern,
                selectMoreButton,
                selectSubLocator,
                dg.haPattern.toString()){ WebElement webelement, String match ->
                    webelement.getText().trim().equalsIgnoreCase(match)
                }.click()
        if(null!=dg.validHaTransitions)
            choseValidHaTransitions(dg.validHaTransitions)
        BrowserUtils
                .getDropDownOptionText(this.executableName,
                dg.executableName.toString())
                .click();
        if(dg.executableName.equals(ExecutableName.OTHER)){
            ifNull(dg.otherInput)
            this.otherInput.clear()
            this.otherInput.sendKeys(dg.otherInput)
        }
        this.execUser.sendKeys(dg.executeUser);
        this.execUserPass.sendKeys(dg.executePassword);
        WebUtils.selectOptionUseCSS(webdriver,
                byIdentifyDaemonBy,
                selectMoreButton,
                selectSubLocator,
                dg.identifyDaemonsBy.toString()) {  WebElement webelement, String match ->
                    webelement.getText().trim().equalsIgnoreCase(match)
                }.click()
        this.globalSearchPatternValue.sendKeys(dg.globalSearchPatternValue);
        if(null!=dg.stateChangeControlScript)
            this.stateChangeControlScript.sendKeys(dg.stateChangeControlScript)
        if(dg.daemons.size()>0){
            logger.info("Fill daemon name")
            this.daemonNameField.sendKeys(dg.daemons.get(0).name);
            addDaemons(dg,sortableRow)
        }
        clickApply()
    }

    @Override
    public ManageDaemonGroupsPage clickApply() {
        logger.info("Click Apply button")
        applyButton.click()
        BrowserUtils.waitForElementsInvisible(webdriver,formThrobber)
        return new ManageDaemonGroupsPage(webdriver)
    }
}
