/**
 * 
 */
package com.td.em.qa.pages

import groovy.util.logging.Slf4j
import com.td.em.qa.domain.DaemonGroup
import java.util.List

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement
import org.openqa.selenium.support.FindBy
import org.openqa.selenium.support.How
import org.openqa.selenium.support.PageFactory
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import com.td.em.qa.utils.BrowserUtils
import com.td.em.qa.utils.WebUtils
import com.td.em.qa.constants.*

/**
 * @author Jin Chen
 *
 */
@Slf4j(value="logger")
class EditDaemonGroupPage extends AbstractDaemonGroupPage{

    @FindBy(how = How.CSS, using = "div#portletContent li:nth-of-type(1) a")
    WebElement generalTab;

    @FindBy(how = How.CSS, using = "div#portletContent li:nth-of-type(2) a")
    WebElement actionRulesTab;

    @FindBy(how = How.CSS, using = "div.dottedSection:nth-of-type(4) div.jquery-selectbox")
    WebElement identifyDaemonsBy

    @FindBy(how = How.CSS, using = "div.newDaemonRow")
    private WebElement newDaemonRow

    private By byIdentifyDaemonBy = By.cssSelector("div.dottedSection:nth-of-type(4) div.jquery-selectbox")

    private DaemonGroup dg

    EditDaemonGroupPage(WebDriver webdriver, DaemonGroup dg) {
        super(webdriver)
        this.dg=dg
    }

    ActionRulesPage createActionRulesPage(){
        actionRulesTab.click()
        BrowserUtils.waitForElementsInvisible(webdriver,formThrobber)
        return new ActionRulesPage(webdriver,dg)
    }
    /*
     * TODO update Daemon on different servers, This is currently not working
     *
     The reason is that
     Step 1
     Click X on the UI one by one for all Daemons ( without click the apply button)
     Step 2
     Add new Daemon
     Step3
     Click the Apply button
     ActualResult
     UI is constantly spin and After logout and re-login, 
     I see the DaemonGroup is disappear.
     */
    EditDaemonGroupPage updateDaemonGroup(){
        if(null!=dg.haPattern)
            WebUtils.selectOptionUseCSS(webdriver,
                    byHaPattern,
                    selectMoreButton,
                    selectSubLocator,
                    dg.haPattern.toString()){  WebElement webelement, String match ->
                        webelement.getText().trim().equalsIgnoreCase(match)
                    }.click()
        if(null!=dg.validHaTransitions)
            choseValidHaTransitions(dg.validHaTransitions)
        if(null!=dg.executableName){
            BrowserUtils.
                    getDropDownOptionText(executableName,
                    dg.executableName.toString())
                    .click();
            if(dg.executableName.equals(ExecutableName.OTHER)){
                ifNull(dg.otherInput)
                this.otherInput.clear()
                this.otherInput.sendKeys(dg.otherInput)
            }
        }
        if(null!=dg.executeUser){
            execUser.clear()
            execUser.sendKeys(dg.executeUser);
        }
        if(null!=dg.executePassword){
            execUserPass.clear()
            execUserPass.sendKeys(dg.executePassword);
        }
        if(null!=dg.identifyDaemonsBy)
            WebUtils.selectOptionUseCSS(webdriver,
                    byIdentifyDaemonBy,
                    selectMoreButton,
                    selectSubLocator,
                    dg.identifyDaemonsBy.toString()){ WebElement webelement, String match ->
                        webelement.getText().trim().equalsIgnoreCase(match)
                    }.click()
        if(null!=dg.globalSearchPatternValue) {
            globalSearchPatternValue.clear()
            globalSearchPatternValue.sendKeys(dg.globalSearchPatternValue);
        }
        if(null!=dg.stateChangeControlScript){
            stateChangeControlScript.clear()
            stateChangeControlScript.sendKeys(dg.stateChangeControlScript)
        }
        if(dg.daemons.size()>0){
            logger.info("Delete Daemons for DaemonGroup "+this.dg.getName())
            deleteAllDaemons()
            addDaemons(dg,newDaemonRow)
        }
        clickApply()
    }

    private void deleteAllDaemons(){
        List<WebElement> list = webdriver.
                findElements(By.cssSelector("form#editDGForm div.dottedSection:not(.startHidden) div.appDelete"))
        logger.info("Total of  "+list.size() + " Daemons to be Deleted")
        for(WebElement r:list){
            r.click()
        }
    }

    @Override
    EditDaemonGroupPage clickApply() {
        applyButton.click()
        BrowserUtils.waitForElementsInvisible(webdriver,formThrobber)
        return new EditDaemonGroupPage(webdriver,dg)
    }
}
