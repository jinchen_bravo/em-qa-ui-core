/**
 * 
 */
package com.td.em.qa.pages

import groovy.util.logging.Slf4j
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.Action
import com.td.em.qa.domain.DaemonGroup
import com.td.em.qa.domain.Rule
import com.td.em.qa.utils.BrowserUtils;
import com.td.em.qa.utils.ViewpointWebTable
import com.td.em.qa.constants.*


/**
 * @author Jin Chen
 *
 */
@Slf4j(value="logger")
class ActionRulesPage{
    private WebDriver webdriver
    @FindBy(how = How.ID, using ="drillBackArrow")
    WebElement drillBackArrow

    @FindBy(how = How.CSS, using ="div.formThrobber")
    List<WebElement> formThrobber

    @FindBy(how = How.CSS, using ="div.emFormThrobber")
    List<WebElement> emFormThrobber

    @FindBy(how = How.CSS, using = "div#theActionArea select")
    private WebElement groupActionSelector

    @FindBy(how = How.CSS, using = "div[title='Add Rule']")
    private WebElement addRuleButton

    @FindBy(id="theActionArea")
    private WebElement theActionArea

    //below WebElements are on the modal which will only visible after
    // click the addRuleButton
    @FindBy(how = How.CSS, using = "div.radio1 input.emFormMessageRadio")
    private WebElement radioButton1

    @FindBy(how = How.CSS, using = "div.radio1 select.severityField")
    private WebElement severitySelect1

    @FindBy(how = How.CSS, using = "input.alertcodeField")
    private WebElement alertCodeText

    @FindBy(how = How.CSS, using = "div.radio2 input.emFormMessageRadio")
    private WebElement radioButton2

    @FindBy(how = How.CSS, using = "div.radio2 select.severityField")
    private WebElement severitySelect2

    @FindBy(how = How.CSS, using = "div.bulkAssignmentArrows div.bulkArrow1")
    private WebElement bulkArrow1Button

    @FindBy(how = How.CSS, using = "div.bulkAssignmentArrows div.bulkArrow2")
    private WebElement bulkArrow2Button

    @FindBy(how = How.ID, using = "compAddResponseSubmitBttn")
    private WebElement okButton

    @FindBy(how = How.CSS, using = "button#compAddResponseSubmitBttn + button")
    private WebElement cancelButton

    @FindBy(how = How.CSS, using = "div.bulkAssignmentBucket.gr1")
    private WebElement availableActionsTable

    @FindBy(how = How.CSS, using = "div.bulkAssignmentBucket.gr2")
    private WebElement selectedActionsTable

    private By rows = By.cssSelector("div.hasData.row");
    private By cols = By.cssSelector("div.col");
    private By applyButton = By.cssSelector("button#applyBttnCER")

    private DaemonGroup dg

    private Rule rule

    private Map<String,Rule> rules

    ActionRulesPage(WebDriver d, DaemonGroup dg) {
        this.webdriver = d
        this.dg = dg
        PageFactory.initElements(webdriver, this)
    }

    ActionRulesPage(WebDriver d, DaemonGroup dg, Map<String,Rule> rules) {
        this.webdriver = d
        this.dg = dg
        this.rules = rules
        PageFactory.initElements(webdriver, this)
    }

    void setRules(Map<String,Rule> rules){
        this.rules=rules
    }

    Map<String,Rule> getRules(){
        return this.rules
    }

    void addRule(String actionGroup, Rule rule){
        logger.info("Add rule for ActionGroup " + actionGroup)
        BrowserUtils.waitForElementsInvisible(webdriver, formThrobber)
        choseActionGroup(actionGroup)
        this.addRuleButton.click()
        BrowserUtils.waitForElementVisible(webdriver,bulkArrow2Button)
        if(null!=rule.alertcode){
            this.radioButton1.click()
            BrowserUtils.getDropDownOptionText(this.severitySelect1,rule.severity.toString())
            this.alertCodeText.sendKeys(rule.alertcode)
        }else{
            this.radioButton2.click()
            BrowserUtils.getDropDownOptionText(this.severitySelect2,rule.severity.toString())
        }
        //add all Actions in the List<Action>
        for(Action a:rule.actions){
            this.addAction(a)
        }
        this.clickOk()
        this.clickApply()
    }

    void clearRuleforActionGroup(String actionGroup){
        logger.info("Clear rule for ActionGroup " + actionGroup)
        BrowserUtils.waitForElementsInvisible(webdriver, formThrobber)
        choseActionGroup(actionGroup)
        By a = By.cssSelector("div#theActionArea div.appDelete")
        List<WebElement> allAppDelete = webdriver.findElements(a)
        if (allAppDelete.size() > 0){
            for(WebElement x:allAppDelete){
                x.click()
                BrowserUtils.waitForElementsInvisible(webdriver, formThrobber)
            }
            clickApply()
        }else{
            logger.info("No rule to clear up for ActionGroup "+actionGroup)
        }
    }

    void clickAddRules(){
        for (Map.Entry<String, Rule> entry : rules.entrySet()) {
            String actionGroup = entry.getKey()
            clearRuleforActionGroup(actionGroup)
        }
        for (Map.Entry<String, Rule> entry : rules.entrySet()) {
            String actionGroup = entry.getKey()
            Rule rule = entry.getValue()
            this.addRule(actionGroup,rule)
        }
    }

    ManageDaemonGroupsPage clickBack(){
        drillBackArrow.click()
        return new ManageDaemonGroupsPage(webdriver)
    }

    void clickOk(){
        okButton.click()
        By m = By.cssSelector("div.unityModal")
        List<WebElement>  l = webdriver.findElements(m)
        BrowserUtils.waitForElementsInvisible(webdriver, l)
        BrowserUtils.waitForElementsInvisible(webdriver, formThrobber)
    }

    void clickCancel(){
        cancelButton.click()
        BrowserUtils.waitForElementsInvisible(webdriver, formThrobber)

    }

    void clickApply(){
        BrowserUtils.ignoreStaleElementReferenceExceptionClick(webdriver,applyButton)
        BrowserUtils.waitForElementsInvisible(webdriver, formThrobber)
        //return new ActionRulesPage(webdriver,dg)
    }

    void addAction(Action action){
        ViewpointWebTable vtable = new ViewpointWebTable(availableActionsTable)
        vtable.getCell(vtable.getIndexOfRowWithColumnText(2,action.name),0).click()
        bulkArrow1Button.click()
    }

    void removeAction(Action action){
        ViewpointWebTable rtable = new ViewpointWebTable(selectedActionsTable)
        rtable.getCell(rtable.getIndexOfRowWithColumnText(2,action.name),0).click()
        bulkArrow2Button.click()
    }

    void choseActionGroup(String server){
        BrowserUtils.getDropDownOptionText(groupActionSelector,server).click()
        BrowserUtils.waitForElementsInvisible(webdriver, emFormThrobber)
    }
}
