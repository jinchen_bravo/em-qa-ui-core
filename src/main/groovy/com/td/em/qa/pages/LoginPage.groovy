package com.td.em.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.td.em.qa.domain.Credential
import groovy.util.logging.Slf4j

/**
 * @author Jin Chen
 *
 */
@Slf4j(value="logger")
public class LoginPage {

    private Credential u
    private WebDriver d

    @FindBy(how = How.ID, using = "portal-login-username-input")
    private WebElement username

    @FindBy(how = How.ID, using = "portal-login-password-input")
    private WebElement password

    @FindBy(how = How.ID, using = "portal-login-button")
    private WebElement loginButton

    public LoginPage(WebDriver d, Credential x) {
        this(d)
        this.setCredential(x)
    }

    public LoginPage(WebDriver webdriver) {
        this.d = webdriver
        PageFactory.initElements(d, this)
    }

    public setCredential(Credential u) {
        this.u = u
    }

    public ViewpointPage signIn() {
        this.username.sendKeys(u.username)
        this.password.sendKeys(u.password)
        this.loginButton.click()
        logger.info(String.format("Sign In as user %s", u.username))
        return new ViewpointPage(d)
    }
}
