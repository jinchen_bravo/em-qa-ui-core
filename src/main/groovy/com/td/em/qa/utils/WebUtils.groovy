/**
 * 
 */
package com.td.em.qa.utils

import java.util.List

import org.openqa.selenium.By
import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

/**
 * @author Jin Chen
 *
 */
class WebUtils {
    static WebElement selectOptionUseCSS(WebDriver webdriver, By root, By button,
            By sub_locator, String matchString, Closure c){
        //popup the drop down options
        webdriver.findElement(root).findElement(button).click()
        List<WebElement> list = webdriver.findElement(root)
                .findElements(sub_locator)
        for (WebElement r : list) {
            if (c(r,matchString)) {
                return r
            } else {
                continue
            }
        }
        throw new IllegalArgumentException(matchString)
    }
}
