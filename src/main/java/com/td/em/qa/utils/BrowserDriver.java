package com.td.em.qa.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jin Chen
 *
 */
public class BrowserDriver {
    private static final Logger logger = LoggerFactory
            .getLogger(BrowserDriver.class);
    private static WebDriver mDriver;

    public static WebDriver getCurrentDriver() {
        if (mDriver == null) {
            mDriver = BrowserFactory.getBrowser();
        }
        return mDriver;
    }

    public static void close() {
        try {
            getCurrentDriver().quit();
            mDriver = null;
            logger.info("closing the browser");
        } catch (UnreachableBrowserException e) {
            logger.info("cannot close browser: unreachable browser");
        }
    }

    public static void loadPage(String url) {
        getCurrentDriver();
        logger.info("Directing browser to:" + url);
        logger.info("try to loadPage [" + url + "]");
        getCurrentDriver().get(url);
    }

    public static void reopenAndLoadPage(String url) {
        mDriver = null;
        getCurrentDriver();
        loadPage(url);
    }

    public static void refreshCurrentPage() {
        getCurrentDriver().navigate().refresh();
    }

}
