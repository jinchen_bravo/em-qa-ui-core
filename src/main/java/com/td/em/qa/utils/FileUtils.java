package com.td.em.qa.utils;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * @author Jin Chen
 *
 */
public class FileUtils {

    private FileUtils() {
        throw new AssertionError();
    }

    /**
     * The source file must be located in the classpath. The format of the
     * destination file name will be prefix + epoc + suffix and it will be put
     * into system temp directory.
     * 
     * @param srcfile
     *            - The source file
     * @param prefix
     *            - The prefix of the destination file
     * @param suffix
     *            - The suffix of the destination file
     * @return The path of the destination file
     * @throws IOException
     * @throws URISyntaxException
     */
    public final static Path makeCopy(String srcfile, String prefix,
            String suffix) throws IOException, URISyntaxException {
        Path orig = FileUtils.getFile(srcfile).toPath();
        String dests = prefix + System.currentTimeMillis() + suffix;
        Path dest = new File(System.getProperty("java.io.tmpdir") + dests)
                .toPath();
        return Files.copy(orig, dest);
    }

    /**
     * The file must be located in the classpath. The input parameter is a
     * String class and the output is a File class
     * 
     * @param filename
     *            - The filename as a String
     * @return File - The filename as a File
     * @throws URISyntaxException
     */
    public final static File getFile(String filename)
            throws URISyntaxException {
        if (null == FileUtils.class.getClassLoader().getResource(filename)) {
            try {
                throw new FileNotFoundException(
                        "File " + filename + " does NOT exist on classpath!");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } else {
            File file = new File(FileUtils.class.getClassLoader()
                    .getResource(filename).getPath());
            return file;
        }
        return null;
    }

    public final static int getExcelFileTotalNumberOfRows(File file,
            int sheetIndex) throws FileNotFoundException, IOException {
        HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(file));
        HSSFSheet sheet = workbook.getSheetAt(sheetIndex);
        workbook.close();
        return sheet.getPhysicalNumberOfRows();
    }

    public final static String getExcelFileCellValue(File file, int sheetIndex,
            int rowIndex, int cellIndex)
                    throws FileNotFoundException, IOException {
        HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(file));
        HSSFSheet sheet = workbook.getSheetAt(sheetIndex);
        workbook.close();
        return sheet.getRow(rowIndex).getCell(cellIndex).getStringCellValue();
    }

    public final static File getTheNewestFile(File dir, String prefix)
            throws FileNotFoundException {
        File theNewestFile = null;

        FileFilter fileFilter = new WildcardFileFilter(prefix + "*");
        File[] files = dir.listFiles(fileFilter);

        if (files.length > 0) {
            /** The newest file comes first **/
            Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
            theNewestFile = files[0];
        }
        if (theNewestFile == null) {
            throw new FileNotFoundException(
                    "Dir: " + dir.toString() + " File: " + prefix + "*");
        }
        return theNewestFile;
    }

}
