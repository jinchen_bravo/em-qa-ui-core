/**
 * 
 */
package com.td.em.qa.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * @author Jin Chen
 *
 */
public class ViewPointTab extends AbstractWebTab {

    private static final By selectClick = By.cssSelector("a");
    private static final By selectText = By.cssSelector("a");    
    private static final By selected = By.cssSelector("li.ui-tabs-active a");
    /**
     * @param webTab
     */
    public ViewPointTab(WebElement webTab) {
        super(webTab, selectClick, selectText, selected);
    }

}
