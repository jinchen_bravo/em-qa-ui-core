/**
 * 
 */
package com.td.em.qa.utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * @author Jin Chen
 *
 */
public abstract class AbstractWebTab {
    private WebElement webTab;

    private By selectText;
    private By selectClick;
    private By selected;

    private By byLiElement = By.cssSelector("li");

    public AbstractWebTab(WebElement webTab, By selectClick, By selectText,
            By selected) {
        this.webTab = webTab;
        this.setSelectText(selectText);
        this.setSelectClick(selectClick);
        this.setSelected(selected);
    }

    public int getTabCount() {
        List<WebElement> tabs = this.webTab.findElements(byLiElement);
        return tabs.size();
    }

    public String getSelectedTab() {
        WebElement tab = this.webTab.findElement(selected);
        return tab.getText();
    }

    public void selectTab(String tabName) {
        boolean found = false;
        List<WebElement> tabs = webTab.findElements(byLiElement);
        for (WebElement tab : tabs) {
            if (tab.findElement(getSelectText()).getText()
                    .equalsIgnoreCase(tabName)) {
                tab.findElement(getSelectClick()).click();
                found = true;
                break;
            }
        }
        if (found == false)
            throw new IllegalArgumentException(
                    "The Tab " + tabName + " does NOT Exist");
    }

    /**
     * @return the webTab
     */
    public WebElement getWebTab() {
        return webTab;
    }

    /**
     * @param webTab
     *            the webTab to set
     */
    public void setWebTab(WebElement webTab) {
        this.webTab = webTab;
    }

    /**
     * @return the selected
     */
    public By getSelected() {
        return selected;
    }

    /**
     * @param selected
     *            the selected to set
     */
    public void setSelected(By selected) {
        this.selected = selected;
    }

    /**
     * @return the selectText
     */
    public By getSelectText() {
        return selectText;
    }

    /**
     * @param selectText
     *            the selectText to set
     */
    public void setSelectText(By selectText) {
        this.selectText = selectText;
    }

    /**
     * @return the selectClick
     */
    public By getSelectClick() {
        return selectClick;
    }

    /**
     * @param selectClick
     *            the selectClick to set
     */
    public void setSelectClick(By selectClick) {
        this.selectClick = selectClick;
    }

}
