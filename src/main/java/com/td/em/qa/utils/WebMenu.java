package com.td.em.qa.utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jin Chen
 *
 */
public class WebMenu {
    private static final Logger logger = LoggerFactory.getLogger(WebMenu.class);
    private static final By defaultMenuItems = By.cssSelector("li");
    private static final By defaultMenuItem = By.cssSelector("a");

    private WebElement menuRoot;
    private By byMenuItems;
    private WebElement enableButton;

    public WebMenu(WebElement enableButton, WebElement menuRoot) {
        this(enableButton, menuRoot, defaultMenuItems);
    }

    public WebMenu(WebElement enableButton, WebElement menuRoot, By byMenuItems) {
        this.menuRoot = menuRoot;
        this.byMenuItems = byMenuItems;
        this.enableButton = enableButton;
    }

    public void clickEnableButton() {
        enableButton.click();
        logger.info("click menu enable button");
    }

    public void select(String menuText) throws IllegalArgumentException {
        select(menuText, defaultMenuItem);
    }

    public void select(String menuText, By byMenuItem) throws IllegalArgumentException {
        List<WebElement> menuItems = menuRoot.findElements(byMenuItems);
        WebElement found = null;
        String target = null;
        for (WebElement menu : menuItems) {
            target = menu.findElement(byMenuItem).getText();
            if (target.equalsIgnoreCase(menuText)) {
                found = menu;
                break;
            }
        }
        if (found != null) {
            logger.info(String.format("select menu item \"%s\"", menuText));
            found.click();
        } else {
            throw new IllegalArgumentException(String.format("menu item \"%s\" not found", menuText));
        }
    }
}
