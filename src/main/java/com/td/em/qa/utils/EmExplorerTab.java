/**
 * 
 */
package com.td.em.qa.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * @author Jin Chen
 *
 */
public class EmExplorerTab extends AbstractWebTab {
    private static final By selectClick = By.cssSelector("a");
    private static final By selectText = By.cssSelector("span");
    private static final By selected = By.cssSelector("li.tabs-selected span");

    /**
     * @param webTab
     */
    public EmExplorerTab(WebElement webTab) {
        super(webTab, selectClick, selectText, selected);
    }
}
