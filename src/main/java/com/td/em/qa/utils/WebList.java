package com.td.em.qa.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
/**
 * @author Jin Chen
 *
 */
public class WebList extends WebTable {
    private static final By defaultRows = By.cssSelector("div.portal-page-link");
    private static final By defaultColumns = By.cssSelector("span");

    public WebList(WebElement webTable) {
        super(webTable, defaultRows, defaultColumns);
    }
}
