package com.td.em.qa.utils;

import org.openqa.selenium.WebDriver;

import com.td.em.qa.pages.LoginPage;
import com.td.em.qa.pages.ViewpointAdminPage;
import com.td.em.qa.pages.ViewpointPage;
import com.td.vp.qa.pages.DatalabsPage;
import com.td.vp.qa.pages.QueryGridPage;
import com.td.vp.qa.pages.SqlScratchpadPage;

public class PageObjectManager {
    private WebDriver driver;

    private DatalabsPage datalabsPage;
    private LoginPage loginPage;
    private ViewpointPage viewpointPage;

    public PageObjectManager(WebDriver driver) {
        this.driver = driver;
    }

    public LoginPage getLoginPage() {
        return (loginPage == null) ? loginPage = new LoginPage(driver) : loginPage;
    }

    public DatalabsPage getDatalabsPage() {
        return (datalabsPage == null) ? datalabsPage = new DatalabsPage(driver) : datalabsPage;
    }

    public ViewpointPage getViewpointPage() {
        return (viewpointPage == null) ? viewpointPage = new ViewpointPage(driver) : viewpointPage;
    }

    private ViewpointAdminPage viewpointAdminPage;

    public ViewpointAdminPage getViewpointAdminPage() {
        return (viewpointAdminPage == null) ? viewpointAdminPage = new ViewpointAdminPage(driver) : viewpointAdminPage;
    }

    private QueryGridPage queryGridPage;

    public QueryGridPage getQueryGridPage() {
        return (queryGridPage == null) ? queryGridPage = new QueryGridPage(driver) : queryGridPage;
    }

    private SqlScratchpadPage sqlScratchpadPage;

    public SqlScratchpadPage getsqlScratchpadPage() {
        return (sqlScratchpadPage == null) ? sqlScratchpadPage = new SqlScratchpadPage(driver) : sqlScratchpadPage;
    }

}
