/**
 * 
 */
package com.td.em.qa.utils;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

/**
 * @author Jin Chen
 *
 */
public class BrowserUtils {

    public final static int TIMEOUTVALUE = 10;

    private BrowserUtils() {
        throw new AssertionError();
    }

    public final static void waitUsingThreadSleep(int millsecond) {
        try {
            Thread.sleep(millsecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public final static void waitJqueryCallDone(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUTVALUE);
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) driver).executeScript("return jQuery.active==0");
            }
        });
    }

    public final static void waitPageLoadComplete(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUTVALUE);
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                return (Boolean) ((JavascriptExecutor) driver).executeScript("return document.readyState")
                        .equals("complete");
            }
        });
    }

    public final static Boolean waitForTitle(WebDriver driver, String t) {
        WebDriverWait wait = new WebDriverWait(driver, TIMEOUTVALUE);
        return wait.until(ExpectedConditions.titleContains(t));
    }

    public final static WebElement waitForElementClickble(WebDriver driver, WebElement webElement) {
        return waitForElementClickble(driver, webElement, null);
    }

    public final static WebElement waitForElementClickble(WebDriver driver, WebElement webElement, Integer waitTimeInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeInSeconds == null ? TIMEOUTVALUE : waitTimeInSeconds);
        return wait.until(ExpectedConditions.elementToBeClickable(webElement));
    }

    public final static WebElement waitForElementVisible(WebDriver driver, By by, Integer waitTimeInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeInSeconds == null ? TIMEOUTVALUE : waitTimeInSeconds);
        return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public final static WebElement waitForElementVisible(WebDriver driver, By by) {
        return waitForElementVisible(driver, by, null);
    }

    public final static WebElement waitForElementPresent(WebDriver driver, By by, Integer waitTimeInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeInSeconds == null ? TIMEOUTVALUE : waitTimeInSeconds);
        return wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }

    public final static WebElement waitForElementPresent(WebDriver driver, By by) {
        return waitForElementPresent(driver, by, null);
    }

    public final static Boolean waitForElementInvisible(WebDriver driver, By by, Integer waitTimeInSeconds) {
        WebDriverWait wait = new WebDriverWait(driver, waitTimeInSeconds == null ? TIMEOUTVALUE : waitTimeInSeconds);
        return wait.until(ExpectedConditions.invisibilityOfElementLocated(by));
    }

    public final static Boolean waitForElementInvisible(WebDriver driver, By by) {
        return waitForElementInvisible(driver, by, null);
    }

    public final static Boolean waitForElementsInvisible(WebDriver driver, List<WebElement> WebElements) {
        return waitForElementsInvisible(driver, WebElements, null);
    }

    public final static Boolean waitForElementsInvisible(WebDriver driver, List<WebElement> WebElements,
            Integer waitTimeInSeconds) {
        if (waitTimeInSeconds == null) {
            waitTimeInSeconds = TIMEOUTVALUE;
        }

        WebDriverWait wait = new WebDriverWait(driver, waitTimeInSeconds);
        return wait.until(ExpectedConditions.invisibilityOfAllElements(WebElements));
    }

    public final static WebElement getParent(WebElement element) {
        return element.findElement(By.xpath(".."));
    }

    public final static List<WebElement> getDropDownOptions(WebElement webElement) {
        Select select = new Select(webElement);
        return select.getOptions();
    }

    public final static WebElement getDropDownOptionText(WebElement webElement, String text) {
        List<WebElement> options = getDropDownOptions(webElement);
        for (WebElement element : options) {
            if (element.getText().trim().equalsIgnoreCase(text)) {
                return element;
            }
        }
        throw new IllegalArgumentException("The Option " + text + " does NOT exist");
    }

    public final static WebElement getDropDownOption(WebElement webElement, String value) {
        List<WebElement> options = getDropDownOptions(webElement);
        for (WebElement element : options) {
            if (element.getAttribute("value").trim().equalsIgnoreCase(value)) {
                return element;
            }
        }
        throw new IllegalArgumentException("The Option " + value + " does NOT exist");
    }

    public static void ignoreStaleElementReferenceExceptionClick(WebDriver d, final By locator) {
        WebDriverWait wait = (WebDriverWait) new WebDriverWait(d, TIMEOUTVALUE)
                .ignoring(StaleElementReferenceException.class).ignoring(WebDriverException.class);
        wait.until(new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver webDriver) {
                webDriver.findElement(locator).click();
                return Boolean.TRUE;
            }
        });
    }

    public static WebElement ignoreStaleElementReferenceExceptionFindWebElement(WebDriver d, By locator) {
        WebDriverWait wait = (WebDriverWait) new WebDriverWait(d, TIMEOUTVALUE)
                .ignoring(StaleElementReferenceException.class).ignoring(NoSuchElementException.class);
        return wait.until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public static List<WebElement> ignoreStaleElementReferenceExceptionFindWebElements(WebDriver d, By locator,
            By sub_locator) {
        WebDriverWait wait = (WebDriverWait) new WebDriverWait(d, TIMEOUTVALUE)
                .ignoring(StaleElementReferenceException.class);
        return wait.until(ExpectedConditions.presenceOfNestedElementsLocatedBy(locator, sub_locator));
    }

    public static Boolean isWebElementExist(WebElement element) {
        try {
            if (element.isEnabled() && element.isDisplayed())
                return Boolean.TRUE;
        } catch (NoSuchElementException e) {
            return Boolean.FALSE;
        }
        return Boolean.FALSE;
    }

    public static Boolean isWebElementExist(WebDriver driver, By locator) {
        try {
            driver.findElement(locator);
            return Boolean.TRUE;
        } catch (NoSuchElementException e) {
            return Boolean.FALSE;
        }
    }
}
