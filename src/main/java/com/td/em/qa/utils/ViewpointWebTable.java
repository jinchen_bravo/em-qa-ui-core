/**
 * 
 */
package com.td.em.qa.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * @author Jin Chen
 *
 */
public class ViewpointWebTable extends WebTable {

    private static final String CSSROWS = "div.hasData.row";
    private static final String CSSCOLS = "div.col";
    private static final By rows = By.cssSelector(CSSROWS);
    private static final By columns = By.cssSelector(CSSCOLS);

    public ViewpointWebTable(WebElement webTable) {
        super(webTable, rows, columns);
    }

    @Override
    public String getColumnText(WebElement row, int columnIndex) {
        By bycol = By.cssSelector(String.format("%s%d", CSSCOLS, columnIndex));
        return row.findElement(bycol).getText().trim();
    }
}
