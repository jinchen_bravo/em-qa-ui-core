package com.td.em.qa.utils;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.constants.Browsers;

/**
 * @author Jin Chen
 *
 */
public class BrowserFactory {
    private static final Logger logger = LoggerFactory.getLogger(BrowserFactory.class);
    private static final String BROWSER_PROP_KEY = "browser";
    private static final String DEFAULT_BROWSER = Browsers.CHROME.name();

    /**
     * creates the browser driver specified in the environment variable "browser" if
     * no property is set then default browser driver is created. The allow
     * properties are Firefox ,Chrome ,IE ,Safari e.g to run with chrome, pass in
     * the option -Dbrowser=chrome at runtime
     * 
     * @return WebDriver
     */
    public static WebDriver getBrowser() {
        Browsers browser;
        WebDriver driver;
        logger.info(String.format("Environment variable %s = %s", BROWSER_PROP_KEY, System.getenv(BROWSER_PROP_KEY)));
        if (System.getenv(BROWSER_PROP_KEY) == null) {
            logger.info(String.format("Using default browser %s", DEFAULT_BROWSER));
            browser = Browsers.browserForName(DEFAULT_BROWSER);
        } else {
            browser = Browsers.browserForName(System.getenv(BROWSER_PROP_KEY));
        }
        switch (browser) {
        case SAFARI:
            driver = createSafariDriver();
            break;
        case IE:
            driver = createIEDriver();
            break;
        case CHROME:
            driver = createChromeDriver();
            break;
        case FIREFOX:
            driver = createFirefoxDriver(getFirefoxProfile());
            break;
        default:
            driver = createChromeDriver();
            break;
        }
        addAllBrowserSetup(driver);
        return driver;
    }

    private static WebDriver createSafariDriver() {
        return new SafariDriver();
    }

    private static WebDriver createIEDriver() {
        if (null == BrowserFactory.class.getClassLoader().getResource("IEDriverServer.exe")) {
            try {
                throw new FileNotFoundException("File IEDriverServer.exe does NOT exist on the classpath");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        System.setProperty("webdriver.ie.driver",
                BrowserFactory.class.getClassLoader().getResource("IEDriverServer.exe").getPath());
        DesiredCapabilities cap = DesiredCapabilities.internetExplorer();
        cap.setCapability(InternetExplorerDriver.ELEMENT_SCROLL_BEHAVIOR, true);
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setJavascriptEnabled(true);
        cap.setCapability("requireWindowFocus", true);
        cap.setCapability("enablePersistentHover", false);
        cap.setCapability("nativeEvents", false);
        cap.setCapability("ignoreZoomSetting", true);
        //return new InternetExplorerDriver(cap);
        return new InternetExplorerDriver();
    }

    private static WebDriver createChromeDriver() {
        // if (null == BrowserFactory.class.getClassLoader()
        // .getResource("chromedriver")) {
        // try {
        // throw new FileNotFoundException(
        // "File chromedriver does NOT exist on the classpath");
        // } catch (FileNotFoundException e) {
        // e.printStackTrace();
        // }
        // }
        // System.setProperty("webdriver.chrome.driver", BrowserFactory.class
        // .getClassLoader().getResource("chromedriver").getPath());
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", System.getProperty("user.home"));
        chromePrefs.put("safebrowsing.enabled", "true");
        ChromeOptions options = new ChromeOptions();
        // HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("--test-type");
        ChromeOptions cap = new ChromeOptions();
        // cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
        // cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        // cap.setCapability(ChromeOptions.CAPABILITY, options);
        return new ChromeDriver(cap);
    }

    private static WebDriver createFirefoxDriver(FirefoxProfile firefoxProfile) {
        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
        FirefoxOptions options=new FirefoxOptions();
        options.setProfile(firefoxProfile);
        return new FirefoxDriver(options);
    }

    private static FirefoxProfile getFirefoxProfile() {
        /*
         *  the firefox profile was created beforehand using
         *  /Applications/Firefox.app/Contents/macOS/firefox-bin -p
         *  create a new profile and name it seleniumide (save to default folder)
         *  then start the firefox and install the selenium-ide addon
         */
//        ProfilesIni profile = new ProfilesIni();
//        FirefoxProfile firefoxProfile = profile.getProfile("seleniumide");
        FirefoxProfile firefoxProfile = new FirefoxProfile();
        firefoxProfile.setPreference("browser.download.folderList", 2);
        firefoxProfile.setPreference("browser.download.manager.showWhenStarting", false);
        firefoxProfile.setPreference("browser.download.dir", System.getProperty("user.home"));
        firefoxProfile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/csv,application/vnd.ms-excel");
        return firefoxProfile;
    }

    private static void addAllBrowserSetup(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().setPosition(new Point(0, 0));
        driver.manage().window().maximize();
    }

}
