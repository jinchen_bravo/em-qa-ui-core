/**
 * 
 */
package com.td.em.qa.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * @author Jin Chen
 *
 */
public class HtmlWebTable extends WebTable {

    private static final By rows = By.tagName("tr");
    private static final By columns = By.tagName("td");

    public HtmlWebTable(WebElement webTable) {
        super(webTable, rows, columns);
    }
}
