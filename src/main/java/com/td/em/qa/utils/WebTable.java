/**
 * 
 */
package com.td.em.qa.utils;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * @author Jin Chen
 *
 */
public class WebTable {
    protected WebElement webTable;
    protected By rows;
    protected By cols;

    public WebTable(WebElement webTable, By rows, By cols) {
        this.webTable = webTable;
        this.rows = rows;
        this.cols = cols;
    }

    public WebElement getWebTable() {
        return webTable;
    }

    public void setWebTable(WebElement webTable) {
        this.webTable = webTable;
    }

    public int getRowCount() {
        return getAllRows().size();
    }

    public int getColumnCount() {
        return getColumnsOfRow(getAllRows().get(0)).size();
    }

    public List<WebElement> getAllRows() {
        return webTable.findElements(rows);
    }

    public List<WebElement> getColumnsOfRow(WebElement row) {
        return row.findElements(cols);
    }

    public WebElement getCell(int rowIndex, int colIndex) {
        WebElement currentRow = getAllRows().get(rowIndex);
        return getColumnsOfRow(currentRow).get(colIndex);
    }

    public String getColumnText(WebElement row, int columnIndex) {
        return row.findElements(this.cols).get(columnIndex).getText().trim();
    }

    public int getIndexOfRowWithColumnText(int colIdx, String columnText) {
        int index = -1;
        List<WebElement> allrows = getAllRows();
        for (WebElement r : allrows) {
            if (getColumnText(r, colIdx).equalsIgnoreCase(columnText)) {
                index = allrows.indexOf(r);
                return index;
            } else {
                continue;
            }
        }
        throw new IllegalArgumentException(String.format("column %d with text %s does NOT exist", colIdx, columnText));
    }

    public int getIndexOfRowWithColumnText(int colIdx1, String columnText1, int colIdx2, String columnText2) {
        int index = -1;
        List<WebElement> allrows = getAllRows();
        for (WebElement r : allrows) {
            if (getColumnText(r, colIdx1).equalsIgnoreCase(columnText1)
                    && getColumnText(r, colIdx2).equalsIgnoreCase(columnText2)) {
                index = allrows.indexOf(r);
                return index;
            } else {
                continue;
            }
        }
        throw new IllegalArgumentException(
                String.format("column %d with text %s and column %d with text %s does NOT exist!", colIdx1, columnText1,
                        colIdx2, columnText2));
    }
}
