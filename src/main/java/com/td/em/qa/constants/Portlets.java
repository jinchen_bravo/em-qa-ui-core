package com.td.em.qa.constants;

public enum Portlets {
	DATALABS, QUERYGRID, NODEMONITOR, SQLSCRATCHPAD;

	public static Portlets portletForName(String portlet) throws IllegalArgumentException {
		for (Portlets b : values()) {
			if (b.toString().equalsIgnoreCase(portlet)) {
				return b;
			}
		}
		throw portletNotFound(portlet);
	}

	private static IllegalArgumentException portletNotFound(String outcome) {
		return new IllegalArgumentException(("Invalid portlet [" + outcome + "]"));
	}
}
