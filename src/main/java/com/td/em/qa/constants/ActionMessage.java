/**
 * 
 */
package com.td.em.qa.constants;

/**
 * @author Jin Chen
 *
 */
public enum ActionMessage {
    EM_ALERTCODE, EM_MESSAGE, EM_RESOURCEID, EM_APPLICATIONID, EM_RESOURCETYPE, EM_SEVERITY, EM_UOWID, EM_TDPID, EM_TABLENAME, EM_DATABASENAME, EM_STATECODE
}
