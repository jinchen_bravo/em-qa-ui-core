/**
 * 
 */
package com.td.em.qa.constants;

/**
 * @author Jin Chen
 *
 */
public enum DaemonState {
    Active, Standby, Passive, Interrupted, Readonly, Restore, Oos, Unrecoverable, Disconnected;
    public static DaemonState forName(String daemonState)
            throws IllegalArgumentException {
        for (DaemonState b : values()) {
            if (b.toString().equalsIgnoreCase(daemonState)) {
                return b;
            }
        }
        throw daemonStateNotFound(daemonState);
    }

    private static IllegalArgumentException daemonStateNotFound(
            String outcome) {
        return new IllegalArgumentException(
                ("Invalid Daemon State [" + outcome + "]"));
    }

}
