/**
 * 
 */
package com.td.em.qa.constants;

/**
 * @author Jin Chen
 *
 */
public enum IdentifyDaemonsBy {
    CMDARGS("Command Line Arguments"), 
    EXECNAME("Executable Name (Full Path)"), 
    JAVACLASS("Java Class (Full Package)");

    private String identifyDaemonsBy;

    private IdentifyDaemonsBy(String identifyDaemonsBy) {
        this.identifyDaemonsBy = identifyDaemonsBy;
    }

    @Override
    public String toString() {
        return identifyDaemonsBy;
    }

}
