package com.td.em.qa.constants;

public enum ServerType {
    APP, AST, BAR, BI, BUS, DMC, ETL, HDP, LAG, LS, RS, TD, USL, VP
}
