/**
 * 
 */
package com.td.em.qa.constants;

/**
 * @author Jin Chen
 *
 */
public enum ActionType {
    ES("Execute Script"), CM("Custom Message");
    private String actionType;

    private ActionType(String actionType) {
        this.actionType = actionType;
    }

    @Override
    public String toString() {
        return actionType;
    }

    public static ActionType forName(String actionType) throws IllegalArgumentException {
        for (ActionType b : values()) {
            if (b.toString().equalsIgnoreCase(actionType)) {
                return b;
            }
        }
        throw actionTypeNotFound(actionType);
    }

    private static IllegalArgumentException actionTypeNotFound(String outcome) {
        return new IllegalArgumentException(("Invalid Action Type [" + outcome + "]"));
    }

}
