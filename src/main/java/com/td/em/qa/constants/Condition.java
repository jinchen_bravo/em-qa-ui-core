package com.td.em.qa.constants;

public enum Condition {
    Normal, Critical, Down, Warning
}
