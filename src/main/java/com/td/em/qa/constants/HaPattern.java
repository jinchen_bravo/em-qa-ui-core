/**
 * 
 */
package com.td.em.qa.constants;

/**
 * @author Jin Chen
 *
 */
public enum HaPattern {
    AA("Active/Active"), AS("Active/Standby");
    private String haPattern;

    private HaPattern(String haPattern) {
        this.haPattern = haPattern;
    }

    @Override
    public String toString() {
        return haPattern;
    }

    public static HaPattern forName(String haPattern) throws IllegalArgumentException {
        for (HaPattern b : values()) {
            if (b.toString().equalsIgnoreCase(haPattern)) {
                return b;
            }
        }
        throw haPatternNotFound(haPattern);
    }

    private static IllegalArgumentException haPatternNotFound(String outcome) {
        return new IllegalArgumentException(("Invalid HA Pattern [" + outcome + "]"));
    }

}
