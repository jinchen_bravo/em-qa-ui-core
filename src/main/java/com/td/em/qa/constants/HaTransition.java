/**
 * 
 */
package com.td.em.qa.constants;

/**
 * @author Jin Chen
 *
 */
public enum HaTransition {
    A2P("Active to Passive"), 
    A2S("Active to Standby"), 
    OOS2A("Out of Service to Active"), 
    OOS2P("Out of Service to Passive"), 
    OOS2S("Out of Service to Standby"), 
    P2A("Passive to Active"), 
    P2S("Passive to Standby"), 
    S2A("Standby to Active"), 
    S2P("Standby to Passive");
    
    private String haTransition;

    private HaTransition(String haTransition) {
        this.haTransition = haTransition;
    }

    @Override
    public String toString() {
        return haTransition;
    }

    public static HaTransition forName(String haTransition) throws IllegalArgumentException {
        for (HaTransition b : values()) {
            if (b.toString().equalsIgnoreCase(haTransition)) {
                return b;
            }
        }
        throw haTransitionNotFound(haTransition);
    }

    private static IllegalArgumentException haTransitionNotFound(String outcome) {
        return new IllegalArgumentException(("Invalid HA Transition State [" + outcome + "]"));
    }
    
}
