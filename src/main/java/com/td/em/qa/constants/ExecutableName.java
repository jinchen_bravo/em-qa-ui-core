/**
 * 
 */
package com.td.em.qa.constants;

/**
 * @author Jin Chen
 *
 */
public enum ExecutableName {
JAVA("Java"),SH("Script (sh)"),BASH("Script (bash)"),OTHER("Other");
    private String executableName;

    private ExecutableName(String executableName) {
        this.executableName = executableName;
    }

    @Override
    public String toString() {
        return executableName;
    }
    
}
