package com.td.em.qa.pages.server;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.EcosystemConfigurationObject;
import com.td.em.qa.domain.Server;
import com.td.em.qa.pages.EcosystemConfiguration;
import com.td.em.qa.pages.EcosystemConfigurationObjectPage;
import com.td.em.qa.pages.HasFilter;
import com.td.em.qa.pages.ManageEcosystemConfigurationObjectPage;

public class ManageServersPage extends EcosystemConfiguration
        implements ManageEcosystemConfigurationObjectPage, HasFilter {
    private static final Logger logger = LoggerFactory
            .getLogger(ManageServersPage.class);
    
    @FindBy(how = How.CSS, using = "div[title=\"Add Servers\"]")
    private WebElement addServer;

    @FindBy(how = How.CSS, using = "div[title=\"Remove Servers\"]")
    private WebElement removeServer;

    @FindBy(how = How.CSS, using = ".viewport.TDPortalResizable div.body")
    private WebElement serverTable;
   
    public ManageServersPage(WebDriver d) {
        super(d);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Integer filter(EcosystemConfigurationObject objectToBeFilter) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer clearFilter() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ManageEcosystemConfigurationObjectPage add(
            EcosystemConfigurationObject ecoSystemConfObj) {
        logger.info("Click Add Server button");
        this.addServer.click();
        return new AddServerPage(webdriver)
                .addServer((Server) ecoSystemConfObj);
    }

    @Override
    public ManageEcosystemConfigurationObjectPage remove(
            EcosystemConfigurationObject ecoSystemConfObj) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ManageEcosystemConfigurationObjectPage removeFromCurrentPage(
            EcosystemConfigurationObject ecoSystemConfObj) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EcosystemConfigurationObjectPage update(
            EcosystemConfigurationObject ecoSystemConfObj) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Boolean isExist(EcosystemConfigurationObject ecoSystemConfObj) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Boolean isExistOnCurrentPage(
            EcosystemConfigurationObject ecoSystemConfObj) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ManageEcosystemConfigurationObjectPage gotoFirstPage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ManageEcosystemConfigurationObjectPage gotoLastPage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ManageEcosystemConfigurationObjectPage gotoNextPage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ManageEcosystemConfigurationObjectPage gotoPageNumber(
            String pageNum) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ManageEcosystemConfigurationObjectPage gotoPreviousPage() {
        // TODO Auto-generated method stub
        return null;
    }

}
