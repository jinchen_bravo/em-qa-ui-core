/**
 * 
 */
package com.td.em.qa.pages.actions;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.Action;
import com.td.em.qa.domain.EMActionCM;
import com.td.em.qa.utils.BrowserUtils;

/**
 * @author Jin Chen
 *
 */
public class AddCustomMessageActionPage
        extends AbstractCustomMessageActionPage {
    private static final Logger logger = LoggerFactory
            .getLogger(AddCustomMessageActionPage.class);
    private EMActionCM action;

    public AddCustomMessageActionPage(WebDriver d, Action action) {
        super(d,action);
    }

//    private void chosePreDefinedMessage(List<ActionMessage> list) {
//        List<WebElement> msgList = webdriver
//                .findElements(By.cssSelector("tokenContainer div"));
//        for (ActionMessage l : list) {
//            for (WebElement e : msgList) {
//                if (e.getText().equalsIgnoreCase(l.name())) {
//                    e.click();
//                    break;
//                }
//            }
//        }
//    }
//
//    public void addPreDefinedMesssageIntoSubject(List<ActionMessage> list) {
//        textareaSubject.click();
//        chosePreDefinedMessage(list);
//        for (ActionMessage l : list)
//            action.setTextSubject(action.getTextSubject() + l.name());
//    }
//
//    public void addPreDefinedMesssageIntoBody(List<ActionMessage> list) {
//        textareaBody.click();
//        chosePreDefinedMessage(list);
//        for (ActionMessage l : list)
//            action.setTextBody(action.getTextBody() + l.name());
//    }

    public ManageActionsPage addAction(EMActionCM action) {
        this.setAction(action);
        ifNull(action.getName());
        ifNull(action.getTextSubject());
        ifNull(action.getTextBody());
        actionName.clear();
        actionName.sendKeys(action.getName());
        enableAction(action);
        if (null != action.getTeradataAlertsAction()) {
            BrowserUtils.getDropDownOptionText(selectTeradataAlertsAction,
                    action.getTeradataAlertsAction().getName()).click();
        }        
        textareaSubject.clear();
        textareaSubject.sendKeys(action.getTextSubject());
        textareaBody.clear();        
        textareaBody.sendKeys(action.getTextBody());
        return clickApply();
    }

    @Override
    public ManageActionsPage clickApply() {
        logger.info("Click Apply button");
        applyButton.click();
        BrowserUtils.waitForElementsInvisible(webdriver, formThrobber);
        return new ManageActionsPage(webdriver);
    }

    /**
     * @return the action
     */
    public EMActionCM getAction() {
        return action;
    }

    /**
     * @param action the action to set
     */
    public void setAction(EMActionCM action) {
        this.action = action;
    }

}
