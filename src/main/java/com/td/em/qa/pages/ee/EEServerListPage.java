package com.td.em.qa.pages.ee;

import org.openqa.selenium.WebDriver;

import com.td.em.qa.domain.Application;
import com.td.em.qa.domain.Daemon;
import com.td.em.qa.domain.EcosystemConfigurationObject;
import com.td.em.qa.domain.Job;
import com.td.em.qa.domain.Table;
import com.td.em.qa.domain.Workflow;
import com.td.em.qa.pages.HasFilter;
import com.td.em.qa.pages.HasPagination;

public class EEServerListPage extends EcosystemExplorerPage
        implements HasPagination, HasFilter {

    public EEServerListPage(WebDriver d) {
        super(d);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Integer filter(EcosystemConfigurationObject objectToBeFilter) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer clearFilter() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object gotoFirstPage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object gotoLastPage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object gotoNextPage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object gotoPageNumber(String pageNum) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object gotoPreviousPage() {
        // TODO Auto-generated method stub
        return null;
    }

    public EcosystemExplorerPage shiftPerspective(
            Class<? extends EcosystemConfigurationObject> object) {
        if (object.equals(Workflow.class) || object.equals(Application.class)
                || object.equals(Job.class) || object.equals(Table.class)
                || object.equals(Daemon.class)) {
            return super.shiftPerspective("Server", object);
        }
        throw new IllegalArgumentException(
                "Not supported shift perspective " + object.toString());
    }

}
