package com.td.em.qa.pages;

import com.td.em.qa.domain.EcosystemConfigurationObject;

public interface HasFilter {
    /**
     * @param objectToBeFilter Object for applying the filter
     * @return Integer number of rows after apply the filter
     */
    public Integer filter(EcosystemConfigurationObject objectToBeFilter);
    
    /**
     * @return Integer number of rows after clear the filter
     */
    public Integer clearFilter();
}
