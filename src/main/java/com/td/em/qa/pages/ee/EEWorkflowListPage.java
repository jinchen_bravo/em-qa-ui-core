package com.td.em.qa.pages.ee;

import org.openqa.selenium.WebDriver;

import com.td.em.qa.domain.Application;
import com.td.em.qa.domain.EcosystemConfigurationObject;
import com.td.em.qa.domain.Server;
import com.td.em.qa.domain.Table;
import com.td.em.qa.pages.HasFilter;
import com.td.em.qa.pages.HasPagination;

public class EEWorkflowListPage extends EcosystemExplorerPage
        implements HasFilter, HasPagination {

    public EEWorkflowListPage(WebDriver d) {
        super(d);
        // TODO Auto-generated constructor stub
    }

    @Override
    public Object gotoFirstPage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object gotoLastPage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object gotoNextPage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object gotoPageNumber(String pageNum) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object gotoPreviousPage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer filter(EcosystemConfigurationObject objectToBeFilter) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Integer clearFilter() {
        // TODO Auto-generated method stub
        return null;
    }

    public EcosystemExplorerPage shiftPerspective(
            Class<? extends EcosystemConfigurationObject> object) {
        if (object.equals(Application.class) || object.equals(Server.class)
                || object.equals(Table.class)) {
            return super.shiftPerspective("Workflow", object);
        }
        throw new IllegalArgumentException(
                "Not supported shift perspective " + object.toString());
    }
}
