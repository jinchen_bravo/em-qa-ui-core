/**
 * 
 */
package com.td.em.qa.pages.actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.td.em.qa.domain.Action;
import com.td.em.qa.domain.EMActionES;

/**
 * @author Jin Chen
 *
 */
public abstract class AbstractExecuteScriptActionPage extends ActionPage {
    @FindBy(how = How.ID, using = "contactId")
    protected WebElement actionName;

    @FindBy(how = How.ID, using = "enabled1")
    protected WebElement actionEnabled;

    @FindBy(how = How.ID, using = "generateEventsFlag")
    protected WebElement executionVisibleInExplorerJobView;

    @FindBy(how = How.ID, using = "captureLogsFlag")
    protected WebElement outputDownloadable;

    @FindBy(how = How.ID, using = "hostFieldId")
    protected WebElement executeHost;

    @FindBy(how = How.ID, using = "userFieldId")
    protected WebElement executeUsername;

    @FindBy(how = How.ID, using = "passFieldId")
    protected WebElement executePassword;

    @FindBy(how = How.ID, using = "execScript2")
    protected WebElement executeScript;

    @FindBy(how = How.ID, using = "execParams")
    protected WebElement executeParameters;

    @FindBy(how = How.ID, using = "applyBttn")
    protected WebElement applyButton;

    public AbstractExecuteScriptActionPage(WebDriver d, Action action) {
        super(d, action);
    }

    public abstract Object clickApply();

    protected void fillForm(EMActionES action) {
        if (null != action.getEnabled()) {
            if (action.getEnabled()) {
                if (!actionEnabled.isSelected())
                    this.actionEnabled.click();
            } else if (actionEnabled.isSelected())
                this.actionEnabled.click();
        }
        if (null != action.getExecutionVisibleInExplorerJobView()) {
            if (!executionVisibleInExplorerJobView.isSelected())
                executionVisibleInExplorerJobView.click();
            if (null != action.getOutputDownloadable()) {
                if (!outputDownloadable.isSelected())
                    outputDownloadable.click();
            }
        }
        if (null != action.getExecuteHost()) {
            this.executeHost.clear();
            this.executeHost.sendKeys(action.getExecuteHost());
        }
        if (null != action.getExecuteUsername()) {
            this.executeUsername.clear();
            this.executeUsername.sendKeys(action.getExecuteUsername());
        }
        if (null != action.getExecutePassword()) {
            this.executePassword.clear();
            this.executePassword.sendKeys(action.getExecutePassword());
        }
        if (null != action.getExecuteScript()) {
            this.executeScript.clear();
            this.executeScript.sendKeys(action.getExecuteScript());
        }
        if (null != action.getExecuteParams()) {
            this.executeParameters.clear();
            this.executeParameters.sendKeys(action.getExecuteParams());
        }
    }
}