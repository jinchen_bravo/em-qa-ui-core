package com.td.em.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.constants.Portlets;
import com.td.em.qa.pages.ee.EcosystemExplorerPage;
import com.td.em.qa.utils.BrowserUtils;
import com.td.em.qa.utils.PageObjectManager;
import com.td.em.qa.utils.ViewPointTab;
import com.td.em.qa.utils.WebList;
import com.td.em.qa.utils.WebMenu;

/**
 * @author Jin Chen
 *
 */
public class ViewpointPage {

    private static final Logger logger = LoggerFactory.getLogger(ViewpointPage.class);

    @FindBy(how = How.ID, using = "portal-admin-button")
    private WebElement portalAdminButton;

    @FindBy(how = How.CSS, using = "button.tabButton.addTabButton")
    private WebElement addTabButton;

    @FindBy(how = How.CSS, using = "input.newTabInput")
    private WebElement newTabInput;

    @FindBy(how = How.ID, using = "portal-add-content-button")
    private WebElement buttonPortalAddContent;

    @FindBy(how = How.CSS, using = "button#portal-user-button")
    private WebElement userButton;

    @FindBy(how = How.CSS, using = "div#portal-user-menu-content li[data-value=\"LOG_OUT\"] a")
    private WebElement logOutButton;

    protected WebDriver webdriver;

    private ViewPointTab tab;

    @FindBy(how = How.CSS, using = "header#portal-header + div + div + div i")
    private WebElement hamburgerButton;

    @FindBy(how = How.CSS, using = "div#portal-navigation button.portal-add-page")
    private WebElement plusButton;

    @FindBy(how = How.CSS, using = "div.v-dialog form input[name=\"name\"]")
    private WebElement inputPageName;

    @FindBy(how = How.CSS, using = "div.v-dialog div.v-dialog-footer span.v-dialog-buttonset button")
    private WebElement buttonCreate;

    private PageObjectManager pageObjectManager;

    public ViewpointPage(WebDriver d) {
        BrowserUtils.waitUsingThreadSleep(500); // This is a hack to make Safari works
        BrowserUtils.waitJqueryCallDone(d);
        this.pageObjectManager = new PageObjectManager(d);
        this.webdriver = d;
        // initTab();
        PageFactory.initElements(d, this);
    }

    public LoginPage logOut() {
        this.userButton.click();
        this.logOutButton.click();
        logger.info("Log Out ... ");
        return pageObjectManager.getLoginPage();
    }

    /**
     * @return ViewpointPage
     */
    public ViewpointPage addTabExplorer() {
        logger.info("Add Tab Explorer");
        this.addTabButton.click();
        this.newTabInput.sendKeys("Explorer");
        this.newTabInput.sendKeys(Keys.ENTER);
        this.buttonPortalAddContent.click();
        return new PortalAddContentPage(webdriver).addEcosystemExplorer();
    }

    /**
     * @return ViewpointAdministration
     */
    public ViewpointAdminPage clickPortalAdminButton() {
        this.portalAdminButton.click();
        logger.info("click button Portal Admin");
        return pageObjectManager.getViewpointAdminPage();
    }

    /**
     * @return EEDaemonListPage
     */
    public EcosystemExplorerPage clickTabExplorer() {
        if (isTabExist("Explorer"))
            return new EcosystemExplorerPage(this.webdriver);
        else
            return addTabExplorer().clickTabExplorer();
    }

    /**
     * @return ViewPointTab
     */
    public ViewPointTab getTab() {
        return tab;
    }

    public WebDriver getWebdriver() {
        return webdriver;
    }

    // private void initTab() {
    // By b = By.cssSelector("div[id=portal-tabsContainer] .ui-tabs-nav");
    // WebElement x = BrowserUtils.waitForElementVisible(this.webdriver, b);
    // setTab(new ViewPointTab(x));
    // }

    public Boolean isTabExist(String tab) {
        try {
            this.tab.selectTab(tab);
        } catch (IllegalArgumentException e) {
            logger.warn(String.format("Tab %s does NOT exist", tab));
            return false;
        }
        logger.info(String.format("Select Tab %s", tab));
        return true;
    }

    /**
     * @param viewPointTab
     *            the tab to set
     */
    public void setTab(ViewPointTab viewPointTab) {
        this.tab = viewPointTab;
    }

    public void setWebdriver(WebDriver webdriver) {
        this.webdriver = webdriver;
    }

    private void clickHamburg() {
        this.hamburgerButton.click();
        logger.info("click hamburger button");
        BrowserUtils.waitUsingThreadSleep(500);
    }

    public void addPortletContainerPage(String name) {
        this.clickHamburg();
        this.plusButton.click();
        logger.info("click plus(+) button");
        // this is a workaround of corner case the input sometime will only has half the
        // value
        BrowserUtils.waitUsingThreadSleep(500);
        this.inputPageName.sendKeys(name);
        this.buttonCreate.click();
        logger.info("click button create");
    }

    @FindBy(how = How.CSS, using = "nav.mdl-navigation div.vp-ordered-pages")
    WebElement listRoot;

    public void selectPortletContainerPage(String name) {
        WebList myList = new WebList(listRoot);
        this.clickHamburg();
        int colIdx = 0;
        int rowIndex = myList.getIndexOfRowWithColumnText(colIdx, name);
        logger.info(String.format("Select page with the name \"%s\" and the index is %d", name, rowIndex));
        myList.getCell(rowIndex, colIdx).click();
    }

    @FindBy(how = How.CSS, using = "div.v-portal-dialog div.v-dialog div.v-dialog-footer span.v-dialog-buttonset button")
    WebElement confirmButton;

    public void deletePortletContainerPage(String name) {
        this.selectPortletContainerPage(name);
        this.clickHamburg();
        WebList myList = new WebList(listRoot);
        int colIdx = 0;
        int rowIndex = myList.getIndexOfRowWithColumnText(colIdx, name);
        WebElement enableButton = BrowserUtils.getParent(myList.getCell(rowIndex, colIdx))
                .findElement(By.cssSelector("span.portal-page-menu button"));
        WebElement menuRoot = BrowserUtils.getParent(myList.getCell(rowIndex, colIdx))
                .findElement(By.cssSelector("span.portal-page-menu ul"));
        logger.info(
                String.format("locate the 3 dot Menu with the page name \"%s\" and the index is %d", name, rowIndex));
        WebMenu menu = new WebMenu(enableButton, menuRoot);
        menu.clickEnableButton();
        menu.select("Delete");
        confirmButton.click();
        logger.info("click button confirm");
    }

    public Object addPortlet(Portlets portlet) {
        BrowserUtils.waitUsingThreadSleep(500);
        this.buttonPortalAddContent.click();
        PortalAddContentPage p = new PortalAddContentPage(this.webdriver);
        if (portlet.equals(Portlets.DATALABS)) {
            p.addDataLabs();
            // p.addSqlScratchpad();
            p.save();
            return pageObjectManager.getDatalabsPage();
        }
        if (portlet.equals(Portlets.SQLSCRATCHPAD)) {
            p.addSqlScratchpad();
            p.save();
            return pageObjectManager.getsqlScratchpadPage();
        }
        if (portlet.equals(Portlets.QUERYGRID)) {
            p.addQueryGrid();
            p.save();
            return pageObjectManager.getQueryGridPage();
        }
        return null;
    }
}
