/**
 * 
 */
package com.td.em.qa.pages.ee;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.Daemon;
import com.td.em.qa.domain.DaemonGroup;
import com.td.em.qa.utils.BrowserUtils;

import static org.junit.Assert.*;

/**
 * @author Jin Chen
 *
 */
public class EEDaemonMetricPage extends EcosystemExplorerPage {
    private static final Logger logger = LoggerFactory
            .getLogger(EEDaemonMetricPage.class);

    @FindBy(how = How.CSS,
            using = "div.detailSummary div.marginBottom5:nth-of-type(1) "
                    + "span.detailData")
    private WebElement daemonGroupName;

    @FindBy(how = How.CSS,
            using = "div.detailSummary div.marginBottom5:nth-of-type(3) "
                    + "span.detailData")
    private WebElement serverName;

    @FindBy(how = How.CSS, using = "div.detailSummary div.secondBlock "
            + "div.marginBottom5:nth-of-type(1) span.detailData")
    private WebElement daemonName;

    @FindBy(how = How.CSS, using = "div.detailSummary div.secondBlock "
            + "div.marginBottom5:nth-of-type(2) span.detailData")
    private WebElement haPattern;

    @FindBy(how = How.CSS,
            using = "div#metricsReport div[id^=chart0] span[id^=chart0]")
    private WebElement metricCpuUsageLastOneHour;

    @FindBy(how = How.CSS,
            using = "div#metricsReport div[id^=chart1] span[id^=chart1]")
    private WebElement metricMemUsageLastOneHour;

    @FindBy(how = How.CSS,
            using = "div#metricsReport div[id^=chart2] span[id^=chart2]")
    private WebElement metricHeartBeatLastOneHour;

    @FindBy(how = How.CSS,
            using = "div#metricsReport div[id^=chart0] div.TjsHealthlineName")
    private WebElement metricCpuUsageName;

    @FindBy(how = How.CSS,
            using = "div#metricsReport div[id^=chart1] div.TjsHealthlineName")
    private WebElement metricMemUsageName;

    @FindBy(how = How.CSS,
            using = "div#metricsReport div[id^=chart2] div.TjsHealthlineName")
    private WebElement metricHeartBeatName;

    @FindBy(how = How.CSS,
            using = "div[id*=chart0] div.healthline_not_inverted div + div + div + div + div + div + div+ div")
    private WebElement metricCpuUsageValue;

    @FindBy(how = How.CSS,
            using = "div[id*=chart1] div.healthline_not_inverted div + div + div + div + div + div + div+ div")
    private WebElement metricMemUsageValue;

    @FindBy(how = How.CSS,
            using = "div[id*=chart2] div.healthline_not_inverted div + div + div + div + div + div + div+ div")
    private WebElement metricHeartBeatValue;

    @FindBy(how = How.CSS,
            using = "button.portlet-back-arrow[aria-label=\'Go Back\']")
    private WebElement goBackButton;

    private Daemon daemon;
    private DaemonGroup daemonGroup;

    public EEDaemonMetricPage(WebDriver d) {
        super(d);
    }

    public EEDaemonMetricPage(WebDriver d, Daemon daemon) {
        this(d);
        setDaemon(daemon);
    }

    public EEDaemonMetricPage(WebDriver d, DaemonGroup dg, Daemon daemon) {
        this(d, daemon);
        setDaemonGroup(dg);
    }

    public Daemon getDaemon() {
        return daemon;
    }

    public DaemonGroup getDaemonGroup() {
        return daemonGroup;
    }

    public EEDaemonListPage goBack() {
        logger.info("Go Back to Daemon List Page");
        this.goBackButton.click();
        BrowserUtils.waitForElementInvisible(webdriver, portletThrobber);
        if (this.daemonGroup != null) {
            return new EEDaemonListPage(this.webdriver, this.daemonGroup);
        } else {
            return new EEDaemonListPage(this.webdriver);
        }
    }

    public void setDaemon(Daemon daemon) {
        this.daemon = daemon;
    }

    public void setDaemonGroup(DaemonGroup daemonGroup) {
        this.daemonGroup = daemonGroup;
    }

    public void verifyDaemon() {
        logger.info("Verify Daemon Name");
        assertTrue(this.daemon.getName()
                .equalsIgnoreCase(this.daemonName.getText()));
        logger.info("Verify Server Name");
        assertTrue(this.daemon.getServer()
                .equalsIgnoreCase(this.serverName.getText()));
        logger.info("Verify DaemonGroup Name");
        assertTrue(this.daemonGroup.getName()
                .equalsIgnoreCase(this.daemonGroupName.getText()));
        logger.info("Verify DaemonGroup HA Pattern");
        assertTrue(this.daemonGroup.getHaPattern().toString()
                .equalsIgnoreCase(this.haPattern.getText()));
        logger.info("Verify Metrics Name...");
        assertEquals(this.metricCpuUsageName.getText(), "CPUUSAGE");
        assertEquals(this.metricMemUsageName.getText(), "MEMUSAGE");
        assertEquals(this.metricHeartBeatName.getText(), "HEARTBEAT");
        logger.info("Verify Metrics Last One Hour...");
        assertFalse(this.metricCpuUsageLastOneHour.getText()
                .equalsIgnoreCase("No Data Available"));
        assertFalse(this.metricMemUsageLastOneHour.getText()
                .equalsIgnoreCase("No Data Available"));
        assertFalse(this.metricHeartBeatLastOneHour.getText()
                .equalsIgnoreCase("No Data Available"));
        logger.info("Verify Metrics Value...");
        logger.info(this.metricCpuUsageValue.getText());
        logger.info(this.metricMemUsageValue.getText());
        logger.info(this.metricHeartBeatValue.getText());
        assertNotNull(this.metricCpuUsageValue.getText());
        assertNotNull(this.metricMemUsageValue.getText());
        assertNotNull(this.metricHeartBeatValue.getText());
    }
}
