/**
 * 
 */
package com.td.em.qa.pages.ee;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.Application;
import com.td.em.qa.domain.Daemon;
import com.td.em.qa.domain.DaemonGroup;
import com.td.em.qa.domain.EcosystemConfigurationObject;
import com.td.em.qa.domain.Job;
import com.td.em.qa.domain.Server;
import com.td.em.qa.domain.Table;
import com.td.em.qa.domain.Workflow;
import com.td.em.qa.pages.ViewpointPage;
import com.td.em.qa.utils.BrowserUtils;
import com.td.em.qa.utils.EmExplorerTab;

/**
 * @author Jin Chen
 *
 */
public class EcosystemExplorerPage extends ViewpointPage {
    private static final Logger logger = LoggerFactory
            .getLogger(EcosystemExplorerPage.class);
    protected DaemonGroup daemonGroup;

    @FindBy(how = How.CSS, using = "div.portlet-throbber")
    protected List<WebElement> formThrobber;

    protected By portletThrobber = By
            .cssSelector("span.portlet-throbber.throbber-loading");

    protected By byFixedcolumnExplorerTableBody = By.cssSelector(
            "div[id*=portlet-container_EcosystemExplorer] div.fixedColumns div.body");
    protected By byResizableExplorerTableBody = By.cssSelector(
            "div[id*=portlet-container_EcosystemExplorer] div.viewport.TDPortalResizable div.body");

    protected By byDivHasData = By.cssSelector("div.hasData");

    @FindBy(how = How.CSS,
            using = "div.ecosystemManagerToolBar div[title=\'Alerts\']")
    protected WebElement alertDependency;

    @FindBy(how = How.CSS,
            using = "div.scrollContainer.scrollDependency div[id*=softwareAlertsDependencyBucket]")
    protected WebElement alertDependencyBucket;

    @FindBy(how = How.CSS,
            using = "div.ecosystemManagerToolBar div[title=\'Reports\']")
    protected WebElement reportDependency;

    @FindBy(how = How.CSS, using = "div[id*=reportsDependencyBucket]")
    protected WebElement reportDependencyBucket;

    private EmExplorerTab emExplorerTab;

    public EcosystemExplorerPage(WebDriver d) {
        super(d);
        initEmExplorerTab();
    }

    public EcosystemExplorerPage(WebDriver d, DaemonGroup dg) {
        this(d);
        daemonGroup = dg;
    }

    public EcosystemExplorerPage clickTab(
            Class<? extends EcosystemConfigurationObject> tabObject) {

        if (tabObject.equals(Daemon.class)) {
            logger.info("Click Tab Daemons on Ecosystem Explorer");
            this.emExplorerTab.selectTab("Daemons");
            BrowserUtils.waitForElementInvisible(webdriver,
                    this.portletThrobber);
            if (null != daemonGroup)
                return new EEDaemonListPage(webdriver, daemonGroup);
            else
                return new EEDaemonListPage(webdriver);
        } else if (tabObject.equals(Server.class)) {
            logger.info("Click Tab Servers on Ecosystem Explorer");
            this.emExplorerTab.selectTab("Servers");
            BrowserUtils.waitForElementInvisible(webdriver,
                    this.portletThrobber);
            return new EEServerListPage(webdriver);
        } else if (tabObject.equals(Table.class)) {
            logger.info("Click Tab Tables on Ecosystem Explorer");
            this.emExplorerTab.selectTab("Tables");
            BrowserUtils.waitForElementInvisible(webdriver,
                    this.portletThrobber);
            return new EETableListPage(webdriver);
        } else if (tabObject.equals(Job.class)) {
            logger.info("Click Tab Jobs on Ecosystem Explorer");
            this.emExplorerTab.selectTab("Jobs");
            BrowserUtils.waitForElementInvisible(webdriver,
                    this.portletThrobber);
            return new EEJobListPage(webdriver);
        } else if (tabObject.equals(Workflow.class)) {
            logger.info("Click Tab Workflows on Ecosystem Explorer");
            this.emExplorerTab.selectTab("Workflows");
            BrowserUtils.waitForElementInvisible(webdriver,
                    this.portletThrobber);
            return new EEWorkflowListPage(webdriver);
        } else if (tabObject.equals(Application.class)) {
            logger.info("Click Tab Applications on Ecosystem Explorer");
            this.emExplorerTab.selectTab("Applications");
            BrowserUtils.waitForElementInvisible(webdriver,
                    this.portletThrobber);
            return new EEApplicationListPage(webdriver);
        } else {
            throw new IllegalArgumentException(
                    "Tab " + tabObject.getName() + " does NOT exist ");
        }
    }

    private String convertString(String x) {
        String ret = null;
        if (x.equals("Server")) {
            ret = "hardware";
        } else if (x.equals("Daemon")) {
            ret = "software";
        } else {
            ret = x + "s";
        }
        return ret.toLowerCase();
    }

    public DaemonGroup getDaemonGroup() {
        return daemonGroup;
    }

    public EmExplorerTab getEmExplorerTab() {
        return emExplorerTab;
    }

    private void initEmExplorerTab() {
        setEmExplorerTab(new EmExplorerTab(webdriver.findElement(
                By.cssSelector("div[id*=portlet-container_EcosystemExplorer] "
                        + ".explorerTabsNavigationul.tabs-nav"))));
    }

    public void portletOption(String operation) {
        By root = By.cssSelector(
                "div[id*=portlet-container_EcosystemExplorer] span.portlet-rewind-options");
        By menuButton = By.cssSelector(
                "div[id*=portlet-container_EcosystemExplorer] span[id*=rewind_menu_button]");
        By sublocator = By.cssSelector(
                "div[id*=portlet-container_EcosystemExplorer] div[id*=rewind_menu] li");

        selectOptionUseCSS(webdriver, root, menuButton, sublocator, operation);
        BrowserUtils.waitForElementInvisible(webdriver, this.portletThrobber);
    }

    private void selectOptionUseCSS(WebDriver d, final By root, final By button,
            final By sub_locator, final String matchString) {
        WebDriverWait wait = new WebDriverWait(d, BrowserUtils.TIMEOUTVALUE);
        wait.ignoring(StaleElementReferenceException.class)
                .until(new ExpectedCondition<Boolean>() {
                    @Override
                    public Boolean apply(WebDriver webDriver) {
                        WebElement x = null;
                        logger.info("Click Menu Button");
                        webDriver.findElement(root).findElement(button).click();
                        List<WebElement> list = webDriver.findElement(root)
                                .findElements(sub_locator);
                        for (WebElement r : list) {
                            if (r.getAttribute("data-value").trim()
                                    .equalsIgnoreCase(matchString)) {
                                x = r;
                            } else {
                                continue;
                            }
                        }
                        if (null == x)
                            throw new IllegalArgumentException(matchString);
                        else
                            logger.info("Chose item " + matchString);
                        x.click();
                        return Boolean.TRUE;
                    }
                });
    }

    public void setDaemonGroup(DaemonGroup daemonGroup) {
        this.daemonGroup = daemonGroup;
    }

    public void setEmExplorerTab(EmExplorerTab emExplorerTab) {
        this.emExplorerTab = emExplorerTab;
    }

    private void shift(String from, String to) {
        String fromNew = convertString(from);
        String toNew = convertString(to);
        // div.ecosystemManagerContainerBody.tables div[id=hardwareDependency]
        String c1 = "div.ecosystemManagerContainerBody." + fromNew + " div[id="
                + toNew + "Dependency]";
        String c3 = "div.scrollContainer.scrollDependency div[id=" + toNew
                + "]";
        if (!BrowserUtils.isWebElementExist(webdriver, By.cssSelector(c3))) {
            logger.info(
                    to + " Dependency Bucket is NOT displayed under " + from);
            webdriver.findElement(By.cssSelector(c1)).click();
        }
        logger.info("Shift from " + from + " to " + to + " Perspective");
        webdriver.findElement(By.cssSelector(c3)).click();
        BrowserUtils.waitForElementInvisible(webdriver, portletThrobber);
    }

    /**
     * Shift Perspective from main bucket to other tabs will try to click the
     * dependency bucket button if the bucket is not visible
     * 
     * @param from
     *            From which main bucket to do the shift perspective
     * @param toObject
     *            Which perspective shift to
     * @return EcosystemExplorerPage
     * @throws IllegalArgumentException
     *             if the object is not Applicaiton, Server, Workflow, Job,
     *             Table, Daemon
     */
    protected EcosystemExplorerPage shiftPerspective(String from,
            Class<? extends EcosystemConfigurationObject> toObject) {
        if (toObject.equals(Application.class)) {
            shift(from, "Application");
            return new EEApplicationListPage(webdriver);
        } else if (toObject.equals(Server.class)) {
            shift(from, "Server");
            return new EEServerListPage(webdriver);
        } else if (toObject.equals(Workflow.class)) {
            shift(from, "Workflow");
            return new EEWorkflowListPage(webdriver);
        } else if (toObject.equals(Job.class)) {
            shift(from, "Job");
            return new EEJobListPage(webdriver);
        } else if (toObject.equals(Table.class)) {
            shift(from, "Table");
            return new EETableListPage(webdriver);
        } else if (toObject.equals(Daemon.class)) {
            shift(from, "Daemon");
            return new EEDaemonListPage(webdriver);
        }
        throw new IllegalArgumentException(
                "Not supported shift perspective " + toObject.toString());
    }
}
