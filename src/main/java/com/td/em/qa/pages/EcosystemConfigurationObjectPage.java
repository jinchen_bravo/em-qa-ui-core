/**
 * 
 */
package com.td.em.qa.pages;

/**
 * @author Jin Chen
 *
 */
public interface EcosystemConfigurationObjectPage {
    public Object clickApply();
}
