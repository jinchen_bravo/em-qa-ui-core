/**
 * 
 */
package com.td.em.qa.pages;

import com.td.em.qa.domain.EcosystemConfigurationObject;

/**
 * @author Jin Chen
 *
 */
public interface ManageEcosystemConfigurationObjectPage extends HasPagination {

    /**
     * @param EcosystemConfigurationObject
     * @return ManageEcosystemConfigurationObjectPage
     */
    public ManageEcosystemConfigurationObjectPage add(
            EcosystemConfigurationObject ecoSystemConfObj);

    public ManageEcosystemConfigurationObjectPage remove(
            EcosystemConfigurationObject ecoSystemConfObj);

    public ManageEcosystemConfigurationObjectPage removeFromCurrentPage(
            EcosystemConfigurationObject ecoSystemConfObj);

    public EcosystemConfigurationObjectPage update(
            EcosystemConfigurationObject ecoSystemConfObj);

    public Boolean isExist(EcosystemConfigurationObject ecoSystemConfObj);

    public Boolean isExistOnCurrentPage(
            EcosystemConfigurationObject ecoSystemConfObj);

    public ManageEcosystemConfigurationObjectPage gotoFirstPage();

    public ManageEcosystemConfigurationObjectPage gotoLastPage();

    public ManageEcosystemConfigurationObjectPage gotoNextPage();

    public ManageEcosystemConfigurationObjectPage gotoPageNumber(
            String pageNum);

    public ManageEcosystemConfigurationObjectPage gotoPreviousPage();
}
