/**
 * 
 */
package com.td.em.qa.pages.actions;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.Action;
import com.td.em.qa.domain.EMActionES;
import com.td.em.qa.utils.BrowserUtils;

/**
 * @author Jin Chen
 *
 */
public class EditExecuteScriptActionPage
        extends AbstractExecuteScriptActionPage {
    private static final Logger logger = LoggerFactory
            .getLogger(EditExecuteScriptActionPage.class);

    public EditExecuteScriptActionPage(WebDriver d,Action action) {
        super(d,action);
    }

    public EditExecuteScriptActionPage editAction(EMActionES action) {
        fillForm(action);
        return clickApply();
    }    
    
    @Override
    public EditExecuteScriptActionPage clickApply() {
        logger.info("Click Apply button");
        applyButton.click();
        BrowserUtils.waitForElementsInvisible(webdriver, formThrobber);
        return new EditExecuteScriptActionPage(webdriver,action);
    }

}
