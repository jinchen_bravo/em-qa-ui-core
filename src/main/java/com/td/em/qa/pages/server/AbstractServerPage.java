package com.td.em.qa.pages.server;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.td.em.qa.pages.EcosystemConfigurationObjectPage;

public class AbstractServerPage implements EcosystemConfigurationObjectPage {
    protected WebDriver webdriver;
    @FindBy(how = How.ID, using = "applyBttn")
    protected WebElement applyButton;

    public AbstractServerPage(WebDriver d) {
        this.webdriver = d;
        PageFactory.initElements(d, this);
    }
    
    @Override
    public ManageServersPage clickApply() {
        this.applyButton.click();
        return new ManageServersPage(webdriver);
    }

}
