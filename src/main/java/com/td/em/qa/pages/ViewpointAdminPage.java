package com.td.em.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.utils.BrowserUtils;

/**
 * @author Jin Chen
 *
 */
public class ViewpointAdminPage {
    private static final Logger logger = LoggerFactory.getLogger(ViewpointAdminPage.class);
    @FindBy(how = How.CSS, using = "button.v-button.add-content-cancel")
    protected WebElement closeButton;

    @FindBy(how = How.CSS, using = "div[title=\"Manage automation and configuration of the ecosystem\"]")
    protected WebElement emConfig;
    protected WebDriver webdriver;

    public ViewpointAdminPage(WebDriver d) {
        BrowserUtils.waitJqueryCallDone(d);
        this.webdriver = d;
        PageFactory.initElements(d, this);
    }

    public EcosystemConfiguration clickEcosystemConfiguration() {
        logger.info("Click Ecosystem Configuration");
        this.emConfig.click();
        return new EcosystemConfiguration(this.webdriver);
    }

    public ViewpointPage close() {
        logger.info("Click Close Button");
        this.closeButton.click();
        return new ViewpointPage(this.webdriver);
    }
}
