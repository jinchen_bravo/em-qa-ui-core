/**
 * 
 */
package com.td.em.qa.pages.actions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.td.em.qa.domain.Action;
import com.td.em.qa.domain.EMActionCM;

/**
 * @author Jin Chen
 *
 */
public abstract class AbstractCustomMessageActionPage extends ActionPage {
    @FindBy(how = How.ID, using = "id")
    protected WebElement actionName;

    @FindBy(how = How.ID, using = "enabled1")
    protected WebElement actionEnabled;

    @FindBy(how = How.ID, using = "camId")
    protected WebElement selectTeradataAlertsAction;

    @FindBy(how = How.ID, using = "subjectField")
    protected WebElement textareaSubject;

    @FindBy(how = How.ID, using = "bodyField")
    protected WebElement textareaBody;

    @FindBy(how = How.ID, using = "applyBttn")
    protected WebElement applyButton;

    @FindBy(how = How.CSS, using = "div.tokenContainer")
    protected WebElement messageTokenContainer;

    public AbstractCustomMessageActionPage(WebDriver d, Action action) {
        super(d, action);
    }

    public abstract Object clickApply();
    
    protected void enableAction(EMActionCM action){
        if (null != action.getEnabled()) {
            if (action.getEnabled()) {
                if (!actionEnabled.isSelected())
                    this.actionEnabled.click();
            } else if (actionEnabled.isSelected())
                this.actionEnabled.click();
        }        
    }
}
