/**
 * 
 */
package com.td.em.qa.pages.actions;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.Action;
import com.td.em.qa.pages.EcosystemConfigurationObjectPage;
import com.td.em.qa.utils.BrowserUtils;

/**
 * @author Jin Chen
 *
 */
public abstract class ActionPage implements EcosystemConfigurationObjectPage {
    private static final Logger logger = LoggerFactory
            .getLogger(ActionPage.class);

    @FindBy(how = How.CSS, using = "div.formThrobber")
    protected List<WebElement> formThrobber;

    @FindBy(how = How.ID, using = "contactTypeSelectId")
    protected WebElement selectActionType;

    protected WebDriver webdriver;

    protected Action action;

    /**
     * 
     */
    public ActionPage(WebDriver d, Action a) {
        webdriver = d;
        action = a;
        PageFactory.initElements(d, this);
    }

    public ActionPage selectActionType(Action action) {
        logger.info("Select Action Type " + action.getActionType().toString());
        BrowserUtils.getDropDownOptionText(selectActionType,
                action.getActionType().toString()).click();
        switch (action.getActionType()) {
        case CM:
            return new AddCustomMessageActionPage(webdriver, action);
        case ES:
            return new AddExecuteScriptActionPage(webdriver, action);
        default:
            return null;
        }
    }

    protected void ifNull(Object n) {
        if (null == n)
            throw new NullPointerException(n + " can NOT be Null!!!");
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.td.em.qa.pages.EcosystemConfigurationObjectPage#clickApply()
     */
    @Override
    public abstract Object clickApply();
}
