package com.td.em.qa.pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.DaemonGroup;
import com.td.em.qa.domain.EcosystemConfigurationObject;
import com.td.em.qa.utils.BrowserUtils;
import com.td.em.qa.utils.ViewpointWebTable;

/**
 * @author Jin Chen
 *
 */
public class ManageDaemonGroupsPage extends EcosystemConfiguration
        implements ManageEcosystemConfigurationObjectPage, HasFilter {
    private static final Logger logger = LoggerFactory.getLogger(ManageDaemonGroupsPage.class);

    @FindBy(how = How.CSS, using = "div[title=\"Add Daemon Group\"]")
    private WebElement addDaemonGroup;

    @FindBy(how = How.CSS, using = "div[title=\"Remove Daemon Groups\"]")
    private WebElement removeDaemonGroup;

    @FindBy(how = How.CSS, using = ".viewport.TDPortalResizable div.body")
    private WebElement daemonGroupTable;

    @FindBy(how = How.CSS, using = "div.v-dialog-footer button:nth-of-type(1)")
    private WebElement confirmYesBtn;

    @FindBy(how = How.CSS, using = "div.hdrText.unselectable label.hdrSpan")
    private WebElement dgHeader;

    @FindBy(how = How.CSS, using = "div.formThrobber")
    private List<WebElement> formThrobber;

    @FindBy(how = How.CSS, using = "button[id*=daemonGroupDataGridDiv_EMConfig][title=\"Previous page\"]")
    private WebElement previousPage;

    @FindBy(how = How.CSS, using = "button[id*=daemonGroupDataGridDiv_EMConfig][title=\"Next page\"]")
    private WebElement nextPage;

    @FindBy(how = How.CSS, using = "button[id*=daemonGroupDataGridDiv_EMConfig][title=\"First page\"]")
    private WebElement firstPage;

    @FindBy(how = How.CSS, using = "button[id*=daemonGroupDataGridDiv_EMConfig][title=\"Last page\"]")
    private WebElement lastPage;

    @FindBy(how = How.CSS, using = "div[id*=daemonGroupDataGridDiv_EMConfig] input.pageNum")
    private WebElement PageNumber;

    @FindBy(how = How.CSS, using = "input.filter")
    private WebElement daemonGroupFilter;

    @FindBy(how = How.CSS, using = "a[title=\'Clear all column filters\']")
    private WebElement clearFilter;
    private By byClearFilter = By.cssSelector("a[title=\'Clear all column filters\']");

    public ManageDaemonGroupsPage(WebDriver d) {
        super(d);
    }

    @Override
    public ManageDaemonGroupsPage add(EcosystemConfigurationObject ecoSystemConfObj) {
        logger.info("Click Add Daemon Group button");
        this.addDaemonGroup.click();
        return new AddDaemonGroupPage(this.webdriver).addDaemonGroup((DaemonGroup) ecoSystemConfObj);
    }

    @Override
    public Integer clearFilter() {
        if (this.clearFilter.isDisplayed() && this.clearFilter.isEnabled()) {
            logger.info("Clear the filter of Daemon Group");
            this.clearFilter.click();
            BrowserUtils.waitForElementInvisible(this.webdriver, byClearFilter);
            WebElement rowCount = this.webdriver.findElement(By.cssSelector("span[id*=RowCount]"));
            return Integer.valueOf(rowCount.getText());
        }
        logger.warn("clear filter link is not visible");
        return null;
    }

    @Override
    public Integer filter(EcosystemConfigurationObject objectTobeFilter) {
        logger.info(String.format("Apply filter of Daemon Group with %s", objectTobeFilter.getName()));
        this.daemonGroupFilter.sendKeys(objectTobeFilter.getName());
        BrowserUtils.waitForElementVisible(this.webdriver, byClearFilter);
        WebElement rowCount = this.webdriver.findElement(By.cssSelector("span[id*=RowCount]"));
        return Integer.valueOf(rowCount.getText());
    }

    @Override
    public ManageDaemonGroupsPage gotoFirstPage() {
        if (BrowserUtils.isWebElementExist(this.firstPage)) {
            logger.info("Go to First Page");
            this.firstPage.click();
            BrowserUtils.waitForElementsInvisible(this.webdriver, this.formThrobber);
            return new ManageDaemonGroupsPage(this.webdriver);
        } else {
            logger.warn("First Page does not Exist");
            return this;
        }
    }

    @Override
    public ManageDaemonGroupsPage gotoLastPage() {
        if (BrowserUtils.isWebElementExist(this.lastPage)) {
            logger.info("Go to Last Page");
            this.lastPage.click();
            BrowserUtils.waitForElementsInvisible(this.webdriver, this.formThrobber);
            return new ManageDaemonGroupsPage(this.webdriver);
        } else {
            logger.warn("Last Page does not Exist");
            return this;
        }
    }

    @Override
    public ManageDaemonGroupsPage gotoNextPage() {
        if (BrowserUtils.isWebElementExist(this.nextPage)) {
            logger.info("Go to Next Page");
            this.nextPage.click();
            BrowserUtils.waitForElementsInvisible(this.webdriver, this.formThrobber);
            return new ManageDaemonGroupsPage(this.webdriver);
        } else {
            logger.warn("Next Page does not Exist");
            return this;
        }
    }

    @Override
    public ManageDaemonGroupsPage gotoPageNumber(String pageNum) {
        if (BrowserUtils.isWebElementExist(this.PageNumber)) {
            logger.info("Go to Page Number " + pageNum);
            this.PageNumber.clear();
            this.PageNumber.sendKeys(pageNum);
            this.PageNumber.sendKeys(Keys.RETURN);
            BrowserUtils.waitForElementsInvisible(this.webdriver, this.formThrobber);
            return new ManageDaemonGroupsPage(this.webdriver);
        } else {
            logger.warn("Page Number field does not Exist");
            throw new IllegalArgumentException("Page Number field does not Exist");
        }
    }

    @Override
    public ManageDaemonGroupsPage gotoPreviousPage() {
        if (BrowserUtils.isWebElementExist(this.previousPage)) {
            logger.info("Go to Previous Page");
            this.previousPage.click();
            BrowserUtils.waitForElementsInvisible(this.webdriver, this.formThrobber);
            return new ManageDaemonGroupsPage(this.webdriver);
        } else {
            logger.warn("Previous Page does not Exist");
            throw new IllegalArgumentException("Previous Page does not Exist");
        }
    }

    @Override
    public Boolean isExist(EcosystemConfigurationObject ecoSystemConfObj) {
        if (BrowserUtils.isWebElementExist(this.firstPage)) {
            gotoFirstPage();
        }
        boolean pageVerified = false;
        while (!pageVerified) {
            if (isExistOnCurrentPage(ecoSystemConfObj))
                return true;
            else {
                if (BrowserUtils.isWebElementExist(this.nextPage))
                    gotoNextPage();
                else
                    pageVerified = true;
            }
        }
        return false;
    }

    @Override
    public Boolean isExistOnCurrentPage(EcosystemConfigurationObject ecoSystemConfObj) {
        int count = 0;
        int maxTries = 6;
        while (true) {
            ViewpointWebTable dgtable = new ViewpointWebTable(daemonGroupTable);
            try {
                logger.info(String.format("%s Rows in Table "), dgtable.getRowCount());
                dgtable.getIndexOfRowWithColumnText(2, ecoSystemConfObj.getName());
                return true;
            } catch (IllegalArgumentException e) {
                scrolldiv(count);
                if (++count == maxTries)
                    return false;
            }
        }
    }

    @Override
    public ManageDaemonGroupsPage remove(EcosystemConfigurationObject ecoSystemConfObj) {
        gotoFirstPage();
        boolean pageVerified = false;
        while (!pageVerified) {
            if (isExistOnCurrentPage(ecoSystemConfObj))
                return removeFromCurrentPage(ecoSystemConfObj);
            else {
                if (BrowserUtils.isWebElementExist(this.nextPage))
                    gotoNextPage();
                else
                    pageVerified = true;
            }
        }
        throw new IllegalArgumentException(String.format("Daemon Group %s does NOT exist", ecoSystemConfObj.getName()));

    }

    @Override
    public ManageDaemonGroupsPage removeFromCurrentPage(EcosystemConfigurationObject ecoSystemConfObj) {
        ViewpointWebTable dgtable = new ViewpointWebTable(daemonGroupTable);
        dgtable.getCell(dgtable.getIndexOfRowWithColumnText(2, ecoSystemConfObj.getName()), 0)
                .findElement(By.cssSelector("span div")).click();
        logger.info(String.format("Remove Daemon Group %s", ecoSystemConfObj.getName()));
        removeDaemonGroup.click();
        confirmYesBtn.click();
        BrowserUtils.waitForElementsInvisible(this.webdriver, formThrobber);
        return new ManageDaemonGroupsPage(this.webdriver);
    }

    private void scrolldiv(int offset) {
        logger.info("scroll start...");
        JavascriptExecutor js;
        js = (JavascriptExecutor) this.webdriver;
        logger.info("Scroll the div");
        String s = Integer.toString(offset * 100);
        js.executeScript("jQuery(\".scroll.TDPortalResizable\").scrollTop(" + s + ");");
        logger.info("scroll end...");
    }

    public EcosystemConfigurationObjectPage update(EcosystemConfigurationObject ecoSystemConfObj) {
        if (isExist(ecoSystemConfObj)) {
            ViewpointWebTable dgtable = new ViewpointWebTable(daemonGroupTable);
            logger.info("Click Daemon Group " + ecoSystemConfObj.getName());
            dgtable.getCell(dgtable.getIndexOfRowWithColumnText(2, ecoSystemConfObj.getName()), 2)
                    .findElement(By.cssSelector("a")).click();
            return new EditDaemonGroupPage(this.webdriver, (DaemonGroup) ecoSystemConfObj).updateDaemonGroup();
        }
        throw new IllegalArgumentException(String.format("Daemon Group %s does NOT exist", ecoSystemConfObj.getName()));
    }

    public ActionRulesPage updateActionRules(EcosystemConfigurationObject ecoSystemConfObj) {
        if (isExist(ecoSystemConfObj)) {
            ViewpointWebTable dgtable = new ViewpointWebTable(daemonGroupTable);
            logger.info("Click Daemon Group " + ecoSystemConfObj.getName());
            dgtable.getCell(dgtable.getIndexOfRowWithColumnText(2, ecoSystemConfObj.getName()), 2)
                    .findElement(By.cssSelector("a")).click();
            return new EditDaemonGroupPage(this.webdriver, (DaemonGroup) ecoSystemConfObj).createActionRulesPage();
        }
        throw new IllegalArgumentException(String.format("Daemon Group %s does NOT exist", ecoSystemConfObj.getName()));
    }
}
