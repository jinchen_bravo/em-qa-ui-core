/**
 * 
 */
package com.td.em.qa.pages.actions;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.Action;
import com.td.em.qa.domain.EMActionES;
import com.td.em.qa.utils.BrowserUtils;

/**
 * @author Jin Chen
 *
 */
public class AddExecuteScriptActionPage
        extends AbstractExecuteScriptActionPage {
    private static final Logger logger = LoggerFactory
            .getLogger(AddExecuteScriptActionPage.class);

    public AddExecuteScriptActionPage(WebDriver d, Action action) {
        super(d,action);
    }

    public ManageActionsPage addAction(EMActionES action) {
        logger.info("Add Action " + action.getName());
        ifNull(action.getName());
        actionName.clear();
        actionName.sendKeys(action.getName());
        fillForm(action);
        return clickApply();
    }

    @Override
    public ManageActionsPage clickApply() {
        logger.info("Click Apply button");
        applyButton.click();
        BrowserUtils.waitForElementsInvisible(webdriver, formThrobber);
        return new ManageActionsPage(webdriver);
    }

}
