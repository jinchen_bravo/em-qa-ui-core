package com.td.em.qa.pages.server;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.pages.EcosystemConfiguration;
import com.td.em.qa.utils.BrowserUtils;

public class EditServerPage extends EcosystemConfiguration {

    private static final Logger logger = LoggerFactory
            .getLogger(EditServerPage.class);

    public EditServerPage(WebDriver d) {
        super(d);

    }

    @FindBy(how = How.CSS,
            using = "button[id*=serversDataGridDiv_EMConfig][title=\"Previous page\"]")
    private WebElement previousPage;

    @FindBy(how = How.CSS,
            using = "button[id*=serversDataGridDiv_EMConfig][title=\"Next page\"]")
    private WebElement nextPage;

    @FindBy(how = How.CSS,
            using = "button[id*=serversDataGridDiv_EMConfig][title=\"First page\"]")
    private WebElement firstPage;

    @FindBy(how = How.CSS,
            using = "button[id*=serversDataGridDiv_EMConfig][title=\"Last page\"]")
    private WebElement lastPage;

    @FindBy(how = How.CSS,
            using = "div[id*=serversDataGridDiv_EMConfig] input.pageNum")
    private WebElement PageNumber;

    public void gotoNextPage() {
        logger.info("Go to Next Page");
        this.nextPage.click();
        BrowserUtils.waitForElementsInvisible(webdriver, this.formThrobber);
    }

    public void gotoPreviousPage() {
        logger.info("Go to Previous Page");
        this.previousPage.click();
        BrowserUtils.waitForElementsInvisible(webdriver, this.formThrobber);
    }

    public void gotoFirstPage() {
        logger.info("Go to First Page");
        this.firstPage.click();
        BrowserUtils.waitForElementsInvisible(webdriver, this.formThrobber);
    }

    public void gotoLastPage() {
        logger.info("Go to Last Page");
        this.lastPage.click();
        BrowserUtils.waitForElementsInvisible(webdriver, this.formThrobber);
    }

    public void gotoPageNumber(String pageNum) {
        logger.info("Go to Page Number " + pageNum);
        this.PageNumber.clear();
        this.PageNumber.sendKeys(pageNum);
        this.PageNumber.sendKeys(Keys.RETURN);
        BrowserUtils.waitForElementsInvisible(webdriver, this.formThrobber);
    }

}
