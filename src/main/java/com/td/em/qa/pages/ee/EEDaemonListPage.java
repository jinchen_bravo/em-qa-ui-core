/**
 * 
 */
package com.td.em.qa.pages.ee;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.constants.Condition;
import com.td.em.qa.constants.DaemonState;
import com.td.em.qa.constants.HaPattern;
import com.td.em.qa.domain.Application;
import com.td.em.qa.domain.Daemon;
import com.td.em.qa.domain.DaemonGroup;
import com.td.em.qa.domain.EcosystemConfigurationObject;
import com.td.em.qa.domain.Server;
import com.td.em.qa.pages.HasFilter;
import com.td.em.qa.pages.HasPagination;
import com.td.em.qa.utils.*;

/**
 * @author Jin Chen
 *
 */
public class EEDaemonListPage extends EcosystemExplorerPage
        implements HasPagination, HasFilter {
    private static final Logger logger = LoggerFactory
            .getLogger(EEDaemonListPage.class);

    @FindBy(how = How.CSS,
            using = "button[id*=softwareDataGridDiv_EcosystemExplorer][title=\"Previous page\"]")
    private WebElement previousPage;

    @FindBy(how = How.CSS,
            using = "button[id*=softwareDataGridDiv_EcosystemExplorer][aria-label=\"Next page\"]")
    private WebElement nextPage;

    @FindBy(how = How.CSS,
            using = "button[id*=softwareDataGridDiv_EcosystemExplorer][title=\"First page\"]")
    private WebElement firstPage;

    @FindBy(how = How.CSS,
            using = "button[id*=softwareDataGridDiv_EcosystemExplorer][title=\"Last page\"]")
    private WebElement lastPage;

    @FindBy(how = How.CSS,
            using = "div[id*=softwareDataGridDiv_EcosystemExplorer] input.pageNum")
    private WebElement PageNumber;

    @FindBy(how = How.CSS,
            using = "div.fixedColumns.TDPortalResizable div[aria-label=\'DAEMON GROUP\'] input.filter")
    private WebElement filterDaemonGroup;

    @FindBy(how = How.CSS,
            using = "div[id*=softwareDataGridDiv_EcosystemExplorer] div.viewport.TDPortalResizable div[aria-label=\'SERVER\'] input.filter")
    private WebElement filterServer;

    @FindBy(how = How.CSS,
            using = "div[id*=softwareDataGridDiv_EcosystemExplorer] div.viewport.TDPortalResizable div[aria-label=\'DAEMON\'] input.filter")
    private WebElement filterDaemon;

    @FindBy(how = How.CSS,
            using = "div[id*=softwareDataGridDiv_EcosystemExplorer] div.viewport.TDPortalResizable div[aria-label=\'STATE\'] input.filter")
    private WebElement filterDaemonState;

    @FindBy(how = How.CSS, using = "a[id*=ClearFilters]")
    private WebElement clearFilters;

    private By byClearFilters = By.cssSelector("a[id*=ClearFilters]");

    @FindBy(how = How.CSS, using = "span[id*=RowCount]")
    private WebElement rowCount;

    @FindBy(how = How.CSS, using = "div[id=applicationsDependency]")
    private WebElement applicationDependency;

    @FindBy(how = How.CSS, using = "div#applicationsDependency+div.toolBarIcon")
    private WebElement serverDependency;

    @FindBy(how = How.CSS, using = "div[id=reportsDependency]")
    private WebElement reportDependency;

    @FindBy(how = How.CSS, using = "div[id=softwareAlertsDependency]")
    private WebElement alertDependency;

    @FindBy(how = How.CSS,
            using = "div[id*=software].tabs-container div.show-collapse")
    private WebElement collapseButon;

    @FindBy(how = How.CSS,
            using = "div.scrollContainer.scrollDependency div[id*=applicationsDependencyBucket]")
    private WebElement applicationDependencyBucket;

    @FindBy(how = How.CSS,
            using = "div.scrollContainer.scrollDependency div[id*=applicationsDependencyBucket] div[id=applications]")
    private WebElement applicationShiftPerspective;

    @FindBy(how = How.CSS,
            using = "div.scrollContainer.scrollDependency div[id*=hardwareDependencyBucket]")
    private WebElement serverDependencyBucket;

    @FindBy(how = How.CSS,
            using = "div.scrollContainer.scrollDependency div[id*=hardwareDependencyBucket] div[id=hardware]")
    private WebElement serverShiftPerspective;

    @FindBy(how = How.CSS,
            using = "div.scrollContainer.scrollDependency div[id*=softwareAlertsDependencyBucket]")
    private WebElement alertDependencyBucket;

    @FindBy(how = How.CSS, using = "div[id*=reportsDependencyBucket]")
    private WebElement reportDependencyBucket;

    public EEDaemonListPage(WebDriver d) {
        super(d);
    }

    public EEDaemonListPage(WebDriver d, DaemonGroup dg) {
        super(d, dg);
    }

    /**
     * Change the DaemonState for Daemon , will try to auto navigate to
     * different pages to locate the Daemon, Column 2 must be Server ,Column 3
     * must be Daemon and Column 4 is the DaemonState
     * 
     * @param dm
     *            Daemon Object
     * @param daemonState
     *            DaemonState Object
     * @throws IllegalArgumentException
     *             If the combination of column 2 Server + Column 3 Daemon is
     *             not found on any page
     */
    public void changeDaemonState(Daemon dm, DaemonState daemonState) {
        if (isExist(dm)) {
            String server = dm.getServer();
            String daemon = dm.getName();
            logger.info("Server " + server + " Daemon " + daemon);
            WebElement root = webdriver
                    .findElement(byResizableExplorerTableBody);
            ViewpointWebTable rtable = new ViewpointWebTable(root);
            logger.info("Pop up the state menu");
            rtable.getCell(
                    rtable.getIndexOfRowWithColumnText(2, server, 3, daemon), 4)
                    .findElement(By.cssSelector("div.state-menu-small-overlay"))
                    .click();
            // css=a[id*="as-sh-cmdargs|sdl01187"][state=passive]
            String cssString = "a[id*=" + daemon + "\\|" + server + "][state="
                    + daemonState.name().toLowerCase() + "]";
            By state = By.cssSelector(cssString);
            logger.info("Chose state " + daemonState.name().toLowerCase());
            webdriver.findElement(state).click();

            By submit = By.cssSelector("div.v-dialog-footer button#submitBtn");
            logger.info("Click the Submit Button");
            webdriver.findElement(submit).click();
        } else {
            throw new IllegalArgumentException("Daemon " + dm.getName()
                    + " on Server " + dm.getServer() + " does NOT exist");
        }
    }

    @Override
    public Integer clearFilter() {
        if (this.clearFilters.isDisplayed() && this.clearFilters.isEnabled()) {
            logger.info("clear the filter");
            this.clearFilters.click();
            BrowserUtils.waitForElementInvisible(webdriver, byClearFilters);
            return Integer.valueOf(this.rowCount.getText());
        }
        logger.warn("clear filter link is not visible");
        return null;
    }

    /**
     * Click Daemon and Return EEDaemonMetricPage , will try to auto navigate to
     * different pages to locate the Daemon, Column 2 must be Server ,Column 3
     * must be Daemon and Column 3 is click-able
     * 
     * @param dm
     *            Daemon Object
     * @return EEDaemonMetricPage
     * @throws IllegalArgumentException
     *             If the combination of column 2 Server + Column 3 Daemon is
     *             not found on any page
     */
    public EEDaemonMetricPage clickDaemon(Daemon dm) {
        if (isExist(dm)) {
            String server = dm.getServer();
            String daemon = dm.getName();
            logger.info("Server name is " + server + " And Daemon name is "
                    + daemon);
            WebElement root = webdriver
                    .findElement(byResizableExplorerTableBody);
            ViewpointWebTable rtable = new ViewpointWebTable(root);
            rtable.getCell(
                    rtable.getIndexOfRowWithColumnText(2, server, 3, daemon), 3)
                    .findElement(By.cssSelector("a")).click();
            BrowserUtils.waitForElementsInvisible(webdriver, this.formThrobber);
            return new EEDaemonMetricPage(webdriver, daemonGroup, dm);
        }
        throw new IllegalArgumentException("Daemon " + dm.getName()
                + " on Server " + dm.getServer() + " does NOT exist");
    }

    /**
     * @throws IllegalArgumentException
     *             if the objectTobeFilter is not DaemonGroup, Daemon or Server
     */
    @Override
    public Integer filter(EcosystemConfigurationObject objectToBeFilter) {
        String f = objectToBeFilter.getName();
        if (objectToBeFilter instanceof DaemonGroup) {
            logger.info("Apply filter for Daemon Group use " + f);
            this.filterDaemonGroup.sendKeys(f);
            BrowserUtils.waitForElementVisible(webdriver, byClearFilters);
            return Integer.valueOf(this.rowCount.getText());
        } else if (objectToBeFilter instanceof Daemon) {
            logger.info("Apply filter for Daemon use " + f);
            this.filterDaemon.sendKeys(f);
            BrowserUtils.waitForElementVisible(webdriver, byClearFilters);
            return Integer.valueOf(this.rowCount.getText());
        } else if (objectToBeFilter instanceof Server) {
            logger.info("Apply filter for Server use " + f);
            this.filterServer.sendKeys(f);
            BrowserUtils.waitForElementVisible(webdriver, byClearFilters);
            return Integer.valueOf(this.rowCount.getText());
        }
        throw new IllegalArgumentException(
                "Not supported filter " + objectToBeFilter.toString());
    }

    /**
     * @param obj
     *            The Enum Object to be used as the filter Example:
     *            Condition.Down
     * @return Integer Total number of rows after apply the filter
     * @throws IllegalArgumentException
     *             if the filter Enum object is not DaemonState, Condition or
     *             HaPattern
     */
    public Integer filter(Enum<?> obj) {
        if (obj instanceof DaemonState) {
            logger.info("Apply filter for State use " + obj.name());
            String c = "div.viewport.TDPortalResizable div[aria-label=\'STATE\'] input.filter";
            webdriver.findElement(By.cssSelector(c)).click();
            String d = "div.viewport.TDPortalResizable div[aria-label=\'STATE\'] div[state=\'"
                    + obj.name().toLowerCase() + "\']";
            webdriver.findElement(By.cssSelector(d)).click();
            BrowserUtils.waitForElementVisible(webdriver, byClearFilters);
            webdriver.findElement(By.cssSelector(c)).click();

        } else if (obj instanceof Condition) {
            logger.info("Apply filter for Condition use " + obj.name());
            String c = "div.viewport.TDPortalResizable div[aria-label=\'CONDITION\'] input.filter";
            webdriver.findElement(By.cssSelector(c)).click();
            String d = "div.viewport.TDPortalResizable div[aria-label=\'CONDITION\'] span[title=\'"
                    + obj.name() + "\']";
            webdriver.findElement(By.cssSelector(d)).click();
            BrowserUtils.waitForElementVisible(webdriver, byClearFilters);
            webdriver.findElement(By.cssSelector(c)).click();
        } else if (obj instanceof HaPattern) {
            logger.info("Apply filter for HA Pattern use " + obj.name());
            String c = "div.viewport.TDPortalResizable div[aria-label=\'HA PATTERN\'] input.filter";
            webdriver.findElement(By.cssSelector(c)).click();
            int x = obj.ordinal();
            x++;
            String d = "div.viewport.TDPortalResizable div[aria-label=\'HA PATTERN\'] div.filterOption:nth-of-type("
                    + x + ")";
            webdriver.findElement(By.cssSelector(d)).click();
            BrowserUtils.waitForElementVisible(webdriver, byClearFilters);
            webdriver.findElement(By.cssSelector(c)).click();
        } else {
            throw new IllegalArgumentException(
                    "Not Supported filter type " + obj.toString());
        }
        return Integer.valueOf(this.rowCount.getText());
    }

    @Override
    public EEDaemonListPage gotoFirstPage() {
        if (BrowserUtils.isWebElementExist(this.firstPage)) {
            logger.info("Go to First Page");
            firstPage.click();
            BrowserUtils.waitForElementInvisible(webdriver,
                    this.portletThrobber);
            if (null != daemonGroup)
                return new EEDaemonListPage(webdriver, daemonGroup);
            else
                return new EEDaemonListPage(webdriver);
        } else {
            logger.warn("First Page is NOT enabled");
            return this;
        }
    }

    @Override
    public EEDaemonListPage gotoLastPage() {
        if (BrowserUtils.isWebElementExist(this.lastPage)) {
            logger.info("Go to Last Page");
            lastPage.click();
            BrowserUtils.waitForElementInvisible(webdriver,
                    this.portletThrobber);
            if (null != daemonGroup)
                return new EEDaemonListPage(webdriver, daemonGroup);
            else
                return new EEDaemonListPage(webdriver);
        } else {
            logger.warn("Last Page is NOT enabled");
            return this;
        }
    }

    @Override
    public EEDaemonListPage gotoNextPage() {
        if (BrowserUtils.isWebElementExist(this.nextPage)) {
            logger.info("Go to Next Page");
            nextPage.click();
            BrowserUtils.waitForElementInvisible(webdriver,
                    this.portletThrobber);
            if (null != daemonGroup)
                return new EEDaemonListPage(webdriver, daemonGroup);
            else
                return new EEDaemonListPage(webdriver);
        } else {
            logger.warn("Next Page is NOT enabled");
            return this;
        }
    }

    @Override
    public EEDaemonListPage gotoPageNumber(String pageNum) {
        if (BrowserUtils.isWebElementExist(this.PageNumber)) {
            logger.info("Go to Page Number " + pageNum);
            PageNumber.clear();
            PageNumber.sendKeys(pageNum);
            PageNumber.sendKeys(Keys.RETURN);
            BrowserUtils.waitForElementInvisible(webdriver,
                    this.portletThrobber);
            if (null != daemonGroup)
                return new EEDaemonListPage(webdriver, daemonGroup);
            else
                return new EEDaemonListPage(webdriver);
        } else {
            logger.warn("PageNumber text is NOT enabled");
            return this;
        }
    }

    @Override
    public EEDaemonListPage gotoPreviousPage() {
        if (BrowserUtils.isWebElementExist(this.previousPage)) {
            logger.info("Go to Previous Page");
            previousPage.click();
            BrowserUtils.waitForElementInvisible(webdriver,
                    this.portletThrobber);
            if (null != daemonGroup)
                return new EEDaemonListPage(webdriver, daemonGroup);
            else
                return new EEDaemonListPage(webdriver);
        } else {
            logger.warn("Previous Page is NOT enabled");
            return this;
        }
    }

    /**
     * will try to auto navigate to different pages to locate the Daemon,
     * 
     * @param daemon
     * @return true or false
     */
    public boolean isExist(Daemon daemon) {
        if (BrowserUtils.isWebElementExist(firstPage)) {
            gotoFirstPage();
        }
        boolean pageVerified = false;
        while (!pageVerified) {
            if (isExistOnCurrentPage(daemon))
                return true;
            else {
                if (BrowserUtils.isWebElementExist(nextPage))
                    gotoNextPage();
                else
                    pageVerified = true;
            }
        }
        return false;
    }

    public boolean isExistOnCurrentPage(Daemon daemon) {
        String servername = daemon.getServer();
        String daemonname = daemon.getName();
        WebElement root = webdriver.findElement(byResizableExplorerTableBody);
        ViewpointWebTable rtable = new ViewpointWebTable(root);
        try {
            rtable.getCell(rtable.getIndexOfRowWithColumnText(2, servername, 3,
                    daemonname), 3);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

    public EcosystemExplorerPage shiftPerspective(
            Class<? extends EcosystemConfigurationObject> object) {
        if (object.equals(Application.class) || object.equals(Server.class)) {
            return super.shiftPerspective("Daemon", object);
        }
        throw new IllegalArgumentException(
                "Not supported shift perspective " + object.toString());
    }
}
