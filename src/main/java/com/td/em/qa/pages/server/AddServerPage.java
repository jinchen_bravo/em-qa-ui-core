package com.td.em.qa.pages.server;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.Server;
import com.td.em.qa.pages.ManageEcosystemConfigurationObjectPage;
import com.td.em.qa.utils.BrowserUtils;

public class AddServerPage extends AbstractServerPage {

    private static final Logger logger = LoggerFactory
            .getLogger(AddServerPage.class);

    public AddServerPage(WebDriver dr) {
        super(dr);
    }

    @FindBy(how = How.CSS, using = "#navi-s>strong")
    WebElement servertab;

    @FindBy(how = How.XPATH,
            using = ".//*[@id='portletContent']/div[1]/div/div/div[2]")
    WebElement Addservertab;

    @FindBy(how = How.ID, using = "name")
    WebElement servername;

    // Selecting Datacenter from Drop Down list Box
    @FindBy(how = How.ID, using = "ecosystem")
    WebElement Datacenter;

    // Selecting servertype from Drop Down list Box
    @FindBy(how = How.ID, using = "serverType")
    WebElement serverType;

    @FindBy(how = How.ID, using = "execScriptUser")
    WebElement execScriptUser;

    @FindBy(how = How.ID, using = "execScriptPass")
    WebElement execScriptPass;

    @FindBy(how = How.CSS, using = "div[title=\"Remove Servers\"]")
    private WebElement removeServers;

    @FindBy(how = How.CSS, using = "div.v-dialog-footer button:nth-of-type(1)")
    private WebElement confirmYesBtn;

    @FindBy(how = How.CSS, using = ".viewport.TDPortalResizable div.body")
    private WebElement serverGroupTable;

    public ManageEcosystemConfigurationObjectPage addServer(Server server) {
        logger.info("Add Server " + server.getName());
        this.servername.sendKeys(server.getName());
        BrowserUtils.getDropDownOption(this.Datacenter,
                server.getDataCenter().toString()).click();
        BrowserUtils.getDropDownOption(this.serverType,
                server.getServerType().name()).click();
        return this.clickApply();
    }

}
