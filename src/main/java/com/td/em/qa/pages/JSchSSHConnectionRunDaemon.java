package com.td.em.qa.pages;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;


public class JSchSSHConnectionRunDaemon {

	/**
	 * Java SSH Connection Program
	 * we need jsch-0.1.53.jar
	 * To run this it should contain dummydaemon.sh at /var/lib/dummydaemon.sh and 
	 * em-qa-sdaemon-0.0.1-SNAPSHOT.jar at /opt/teradata/em-qa-sdaemon/lib on the machine where we create and verify Daemons
	 */
	public void RunDaemons(String hst ,String usr,String pwd) {
	//public void RunDaemons() {
	          /* String host="xorn.td.teradata.com";
	           String user="root";
	           String password="emdev2015";*/
		
			   String host=hst;
			   String user=usr;
			   String password=pwd;

	           String command1="/bin/sh /var/lib/dummydaemon.sh &";
	           String command2="/opt/teradata/jvm64/jdk7/bin/java -classpath /opt/teradata/em-qa-sdaemon/lib/em-qa-sdaemon-0.0.1-SNAPSHOT.jar com.td.em.qa.sdaemon.Main &";
	           String command3="/opt/teradata/jvm64/jdk7/bin/java -classpath /opt/teradata/em-qa-sdaemon Add 10 20 30 &";
	           String command4="/bin/sh /opt/teradata/em-qa-sdaemon/test.sh Hello &";
	           
	           try{
	              
	              java.util.Properties config = new java.util.Properties(); 
	              config.put("StrictHostKeyChecking", "no");
	              JSch jsch = new JSch();
	              Session session=jsch.getSession(user, host, 22);
	              session.setPassword(password);
	              session.setConfig(config);
	              session.connect();
	              System.out.println("Connected to " + host);
	              
	              Channel channel1=session.openChannel("exec");
	              Channel channel2=session.openChannel("exec");
	              Channel channel3=session.openChannel("exec");
	              Channel channel4=session.openChannel("exec");
	              
	              
	              ((ChannelExec)channel1).setCommand(command1);
	              ((ChannelExec)channel2).setCommand(command2);
	              ((ChannelExec)channel3).setCommand(command3);
	              ((ChannelExec)channel4).setCommand(command4);
	             
	              channel1.connect();
	              Thread.sleep(10);
	              channel2.connect();
	              channel3.connect();
	              channel4.connect();
	              System.out.println("Connected to channel");
	                             
	              Thread.sleep(10);
	              channel1.disconnect();
	              channel2.disconnect();
	              channel3.disconnect();
	              channel4.disconnect();
	              session.disconnect();
	              System.out.println("Started Daemons on " + host);
	           }catch(Exception e){
	              e.printStackTrace();
	           }

	       }

	}

