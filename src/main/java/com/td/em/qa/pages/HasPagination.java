/**
 * 
 */
package com.td.em.qa.pages;

/**
 * @author Jin Chen
 *
 */
public interface HasPagination {
    public Object gotoFirstPage();

    public Object gotoLastPage();

    public Object gotoNextPage();

    public Object gotoPageNumber(String pageNum);

    public Object gotoPreviousPage();

}
