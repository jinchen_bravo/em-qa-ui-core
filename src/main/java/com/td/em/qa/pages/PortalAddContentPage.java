/**
 * 
 */
package com.td.em.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Jin Chen
 *
 */
public class PortalAddContentPage {

    private static final Logger logger = LoggerFactory.getLogger(PortalAddContentPage.class);
    @FindBy(how = How.CSS, using = "button.add-content-save")
    private WebElement addContentSave;

    @FindBy(how = How.CSS, using = "div[title=\"Ecosystem Explorer\"]")
    private WebElement divEcosystemExplorer;

    @FindBy(how = How.ID, using = "DataLabs-add-content-portlet-desc")
    private WebElement divDataLabs;

    private WebDriver webdriver;

    public PortalAddContentPage(WebDriver d) {
        webdriver = d;
        PageFactory.initElements(webdriver, this);
    }

    public ViewpointPage addEcosystemExplorer() {
        logger.info("Add Ecosystem Explorer");
        this.divEcosystemExplorer.click();
        this.addContentSave.click();
        return new ViewpointPage(webdriver);
    }

    public void save() {
        this.addContentSave.click();
        logger.info("click button Save");
    }

    public void addDataLabs() {
        logger.info("Click DataLabs portlet");
        this.divDataLabs.click();
    }

    @FindBy(how = How.ID, using = "QueryGridConfigurator-add-content-portlet-desc")
    private WebElement divQueryGrid;

    public void addQueryGrid() {
        logger.info("click QueryGrid portlet");
        divQueryGrid.click();
    }

    @FindBy(how = How.ID, using = "SqlScratchpad-add-content-portlet-title")
    private WebElement divSqlScrachpad;

    public void addSqlScratchpad() {
        logger.info("click SqlScratchPad portlet");
        divSqlScrachpad.click();
    }

}
