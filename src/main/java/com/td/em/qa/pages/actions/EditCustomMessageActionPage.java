/**
 * 
 */
package com.td.em.qa.pages.actions;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.Action;
import com.td.em.qa.domain.EMActionCM;
import com.td.em.qa.utils.BrowserUtils;

/**
 * @author Jin Chen
 *
 */
public class EditCustomMessageActionPage
        extends AbstractCustomMessageActionPage {
    private static final Logger logger = LoggerFactory
            .getLogger(EditCustomMessageActionPage.class);    

    public EditCustomMessageActionPage(WebDriver d, Action action) {
        super(d, action);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.td.em.qa.pages.actions.AbstractCustomMessageActionPage#clickApply()
     */
    @Override
    public EditCustomMessageActionPage clickApply() {
        logger.info("Click Apply button");
        applyButton.click();
        BrowserUtils.waitForElementsInvisible(webdriver, formThrobber);
        return new EditCustomMessageActionPage(webdriver, action);
    }

    /**
     * @param ecoSystemConfObj
     * @return
     */
    public EditCustomMessageActionPage editAction(EMActionCM action) {
        enableAction(action);
        if (null != action.getTeradataAlertsAction()) {
            BrowserUtils.getDropDownOptionText(selectTeradataAlertsAction,
                    action.getTeradataAlertsAction().getName()).click();
        }
        if (null != action.getTextSubject()) {
            textareaSubject.clear();
            textareaSubject.sendKeys(action.getTextSubject());
        }
        if (null != action.getTextBody()) {
            textareaBody.clear();
            textareaBody.sendKeys(action.getTextBody());
        }
        return clickApply();
    }

}
