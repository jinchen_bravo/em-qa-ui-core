/**
 * 
 */
package com.td.em.qa.pages.actions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.domain.Action;
import com.td.em.qa.domain.EMActionCM;
import com.td.em.qa.domain.EMActionES;
import com.td.em.qa.domain.EcosystemConfigurationObject;
import com.td.em.qa.pages.EcosystemConfiguration;
import com.td.em.qa.pages.EcosystemConfigurationObjectPage;
import com.td.em.qa.utils.BrowserUtils;
import com.td.em.qa.utils.ViewpointWebTable;
import com.td.em.qa.pages.ManageEcosystemConfigurationObjectPage;

/**
 * @author Jin Chen
 *
 */
public class ManageActionsPage extends EcosystemConfiguration
        implements ManageEcosystemConfigurationObjectPage {
    private static final Logger logger = LoggerFactory
            .getLogger(ManageActionsPage.class);
    @FindBy(how = How.CSS, using = "div[title=\"Add Actions\"]")
    private WebElement addActions;

    @FindBy(how = How.CSS, using = "div[title=\"Remove Actions\"]")
    private WebElement removeActions;

    @FindBy(how = How.CSS, using = ".viewport.TDPortalResizable div.body")
    private WebElement actionsTable;

    @FindBy(how = How.CSS, using = "button[id^=yes]")
    private WebElement confirmYesBtn;

    @FindBy(how = How.CSS,
            using = "button[id*=actionsDataGridDiv_EMConfig][title=\"Previous page\"]")
    private WebElement previousPage;

    @FindBy(how = How.CSS,
            using = "button[id*=actionsDataGridDiv_EMConfig][title=\"Next page\"]")
    private WebElement nextPage;

    @FindBy(how = How.CSS,
            using = "button[id*=actionsDataGridDiv_EMConfig][title=\"First page\"]")
    private WebElement firstPage;

    @FindBy(how = How.CSS,
            using = "button[id*=actionsDataGridDiv_EMConfig][title=\"Last page\"]")
    private WebElement lastPage;

    @FindBy(how = How.CSS,
            using = "div[id*=actioinsDataGridDiv_EMConfig] input.pageNum")
    private WebElement PageNumber;

    /**
     * @param webdriver
     */
    public ManageActionsPage(WebDriver d) {
        super(d);
    }

    @Override
    public ManageEcosystemConfigurationObjectPage add(
            EcosystemConfigurationObject ecoSystemConfObj) {
        logger.info("Click Add Button");
        addActions.click();
        switch (((Action) ecoSystemConfObj).getActionType()) {
        case CM:
            return ((AddCustomMessageActionPage) new AddCustomMessageActionPage(
                    webdriver, (Action) ecoSystemConfObj)
                            .selectActionType((Action) ecoSystemConfObj))
                                    .addAction((EMActionCM) ecoSystemConfObj);

        case ES:
            return ((AddExecuteScriptActionPage) new AddExecuteScriptActionPage(
                    webdriver, (Action) ecoSystemConfObj)
                            .selectActionType((Action) ecoSystemConfObj))
                                    .addAction((EMActionES) ecoSystemConfObj);
        default:
            return null;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.td.em.qa.pages.AbstractManageDomainObjectPage#gotoFirstPage()
     */
    @Override
    public ManageActionsPage gotoFirstPage() {
        if (BrowserUtils.isWebElementExist(this.firstPage)) {
            logger.info("Go to First Page");
            this.firstPage.click();
            BrowserUtils.waitForElementsInvisible(webdriver, this.formThrobber);
            return new ManageActionsPage(webdriver);
        } else {
            logger.warn("First Page does not Exist");
            return this;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.td.em.qa.pages.AbstractManageDomainObjectPage#gotoLastPage()
     */
    @Override
    public ManageActionsPage gotoLastPage() {
        if (BrowserUtils.isWebElementExist(this.lastPage)) {
            logger.info("Go to Last Page");
            lastPage.click();
            BrowserUtils.waitForElementsInvisible(webdriver, this.formThrobber);
            return new ManageActionsPage(webdriver);
        } else {
            logger.warn("Last Page does not Exist");
            return this;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.td.em.qa.pages.AbstractManageDomainObjectPage#gotoNextPage()
     */
    @Override
    public ManageActionsPage gotoNextPage() {
        if (BrowserUtils.isWebElementExist(this.nextPage)) {
            logger.info("Go to Next Page");
            nextPage.click();
            BrowserUtils.waitForElementsInvisible(webdriver, this.formThrobber);
            return new ManageActionsPage(webdriver);
        } else {
            logger.warn("Next Page does not Exist");
            return this;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.td.em.qa.pages.AbstractManageDomainObjectPage#gotoPageNumber(java.
     * lang.String)
     */
    @Override
    public ManageActionsPage gotoPageNumber(String pageNum) {
        if (BrowserUtils.isWebElementExist(PageNumber)) {
            logger.info("Go to Page Number " + pageNum);
            this.PageNumber.clear();
            this.PageNumber.sendKeys(pageNum);
            this.PageNumber.sendKeys(Keys.RETURN);
            BrowserUtils.waitForElementsInvisible(webdriver, this.formThrobber);
            return new ManageActionsPage(webdriver);
        } else {
            logger.warn("Page Number field does not Exist");
            return this;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.td.em.qa.pages.AbstractManageDomainObjectPage#gotoPreviousPage()
     */
    @Override
    public ManageActionsPage gotoPreviousPage() {
        if (BrowserUtils.isWebElementExist(previousPage)) {
            logger.info("Go to Previous Page");
            previousPage.click();
            BrowserUtils.waitForElementsInvisible(webdriver, this.formThrobber);
            return new ManageActionsPage(webdriver);
        } else {
            logger.warn("Previous Page does not Exist");
            return this;
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.td.em.qa.pages.ManageEcosystemConfigurationObjectPage#isExist(com.td.
     * em.qa.domain.EcosystemConfigurationObject)
     */
    @Override
    public Boolean isExist(EcosystemConfigurationObject ecoSystemConfObj) {
        gotoFirstPage();
        boolean pageVerified = false;
        while (!pageVerified) {
            if (isExistOnCurrentPage(ecoSystemConfObj))
                return true;
            else {
                if (BrowserUtils.isWebElementExist(nextPage))
                    gotoNextPage();
                else
                    pageVerified = true;
            }
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.td.em.qa.pages.ManageEcosystemConfigurationObjectPage#
     * isExistOnCurrentPage(com.td.em.qa.domain.EcosystemConfigurationObject)
     */
    @Override
    public Boolean isExistOnCurrentPage(
            EcosystemConfigurationObject ecoSystemConfObj) {
        int count = 0;
        int maxTries = 6;
        while (true) {
            ViewpointWebTable table = new ViewpointWebTable(actionsTable);
            try {
                logger.info(table.getRowCount() + " Rows in Table ");
                table.getIndexOfRowWithColumnText(3,
                        ecoSystemConfObj.getName());
                return true;
            } catch (IllegalArgumentException e) {
                scrolldiv(count);
                if (++count == maxTries)
                    return false;
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.td.em.qa.pages.ManageEcosystemConfigurationObjectPage#remove(com.td.
     * em.qa.domain.EcosystemConfigurationObject)
     */
    @Override
    public ManageEcosystemConfigurationObjectPage remove(
            EcosystemConfigurationObject ecoSystemConfObj) {
        gotoFirstPage();
        boolean pageVerified = false;
        while (!pageVerified) {
            if (isExistOnCurrentPage(ecoSystemConfObj))
                return removeFromCurrentPage(ecoSystemConfObj);
            else {
                if (BrowserUtils.isWebElementExist(nextPage))
                    gotoNextPage();
                else
                    pageVerified = true;
            }
        }
        throw new IllegalArgumentException("Daemon Group "
                + ecoSystemConfObj.getName() + " does NOT exist");

    }

    /*
     * (non-Javadoc)
     * 
     * @see com.td.em.qa.pages.ManageEcosystemConfigurationObjectPage#
     * removeFromCurrentPage(com.td.em.qa.domain.EcosystemConfigurationObject)
     */
    @Override
    public ManageEcosystemConfigurationObjectPage removeFromCurrentPage(
            EcosystemConfigurationObject ecoSystemConfObj) {
        ViewpointWebTable table = new ViewpointWebTable(actionsTable);
        table.getCell(table.getIndexOfRowWithColumnText(3,
                ecoSystemConfObj.getName()), 0)
                .findElement(By.cssSelector("span div")).click();
        logger.info("Remove Action " + ecoSystemConfObj.getName());
        removeActions.click();
        confirmYesBtn.click();
        BrowserUtils.waitForElementsInvisible(webdriver, formThrobber);
        return new ManageActionsPage(webdriver);
    }

    private void scrolldiv(int offset) {
        logger.info("scroll start...");
        JavascriptExecutor js;
        js = (JavascriptExecutor) webdriver;
        logger.info("Scroll the div");
        String s = Integer.toString(offset * 100);
        js.executeScript(
                "jQuery(\".scroll.TDPortalResizable\").scrollTop(" + s + ");");
        logger.info("scroll end...");
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.td.em.qa.pages.ManageEcosystemConfigurationObjectPage#update(com.td.
     * em.qa.domain.EcosystemConfigurationObject)
     */
    @Override
    public EcosystemConfigurationObjectPage update(
            EcosystemConfigurationObject ecoSystemConfObj) {
        if (isExist(ecoSystemConfObj)) {
            ViewpointWebTable table = new ViewpointWebTable(actionsTable);
            logger.info("Click Action " + ecoSystemConfObj.getName());
            table.getCell(table.getIndexOfRowWithColumnText(3,
                    ecoSystemConfObj.getName()), 3)
                    .findElement(By.cssSelector("a")).click();
            switch (((Action) ecoSystemConfObj).getActionType()) {
            case CM:
                return new EditCustomMessageActionPage(webdriver,
                        (Action) ecoSystemConfObj)
                                .editAction((EMActionCM) ecoSystemConfObj);
            case ES:
                return new EditExecuteScriptActionPage(webdriver,
                        (Action) ecoSystemConfObj)
                                .editAction((EMActionES) ecoSystemConfObj);
            default:
                return null;
            }
        }
        throw new IllegalArgumentException(
                "Action " + ecoSystemConfObj.getName() + " does NOT exist");

    }
}
