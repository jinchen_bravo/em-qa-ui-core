package com.td.em.qa.pages;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.pages.actions.ManageActionsPage;
import com.td.em.qa.pages.server.ManageServersPage;
import com.td.em.qa.utils.BrowserUtils;

/**
 * @author Jin Chen
 *
 */
public class EcosystemConfiguration extends ViewpointAdminPage {

    private static final Logger logger = LoggerFactory.getLogger(EcosystemConfiguration.class);

    @FindBy(how = How.CSS, using = "div#Daemons div.preferenceName.mouseHand")
    protected WebElement Daemons;

    @FindBy(how = How.CSS, using = "div.formThrobber")
    protected List<WebElement> formThrobber;

    @FindBy(how = How.CSS, using = "div#AlertsAndContacts div.preferenceName.mouseHand")
    protected WebElement Actions;

    @FindBy(how = How.CSS, using = "div#Servers div.preferenceName.mouseHand")
    protected WebElement Servers;

    public EcosystemConfiguration(WebDriver d) {
        super(d);
        BrowserUtils.waitUsingThreadSleep(500);
    }

    public ManageDaemonGroupsPage clickDaemons() {
        logger.info("Click Daemons...");
        this.Daemons.click();
        BrowserUtils.waitForElementsInvisible(this.webdriver, formThrobber, 20);
        return new ManageDaemonGroupsPage(this.webdriver);
    }

    public ManageActionsPage clickActions() {
        logger.info("Click Actions...");
        this.Actions.click();
        BrowserUtils.waitForElementsInvisible(this.webdriver, formThrobber, 20);
        return new ManageActionsPage(this.webdriver);
    }

    public ManageServersPage clickServers() {
        logger.info("Click Servers...");
        this.Servers.click();
        BrowserUtils.waitForElementsInvisible(this.webdriver, formThrobber, 20);
        return new ManageServersPage(this.webdriver);
    }
}
