package com.td.vp.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.utils.BrowserUtils;
import com.td.em.qa.utils.WebMenu;
import com.td.vp.qa.model.MonitoredSystem;

public class SqlScratchpadPage {
    private static final Logger logger = LoggerFactory.getLogger(QueryGridPage.class);
    // private WebDriver webdriver;

    public SqlScratchpadPage(WebDriver d) {
        // this.webdriver = d;
        PageFactory.initElements(d, this);
    }

    @FindBy(how = How.CSS, using = "div[id^=portlet-container_SqlScratchpad] div.portlet-system-select button")
    private WebElement buttonSelectSystem;
    @FindBy(how = How.CSS, using = "div[id^=portlet-container_SqlScratchpad] div.portlet-system-select ul")
    private WebElement menuSelectSystem;
    @FindBy(how = How.CSS, using = "div[id^=portlet-container_SqlScratchpad] div.SqlScratchpad-dialog-content input[name=username]")
    private WebElement inputSystemUsername;
    @FindBy(how = How.CSS, using = "div[id^=portlet-container_SqlScratchpad] div.SqlScratchpad-dialog-content input[name=password]")
    private WebElement inputSystemPassword;
    @FindBy(how = How.CSS, using = "div[id^=portlet-container_SqlScratchpad] div.SqlScratchpad-dialog-content+div button")
    private WebElement buttonSystemConnect;
    public void Connect(MonitoredSystem system) {
        WebMenu menu = new WebMenu(buttonSelectSystem, menuSelectSystem);
        menu.clickEnableButton();
        menu.select(system.getName());
        inputSystemUsername.sendKeys(system.getCredential().getUsername());
        inputSystemPassword.sendKeys(system.getCredential().getPassword());
        buttonSystemConnect.click();
        logger.info(String.format("click button Connect to System %s",system.getName()));
    }
    
    @FindBy(how = How.CSS, using = "div[id^=portlet-container_SqlScratchpad] textarea")
    private WebElement textareaSqlScratch;
    private static final String divButtonSql = "div.sqlScratchpadPortlet div.sqlScratchpadSubmitButtons button[title=\"Execute queries\"]";
    @FindBy(how = How.CSS, using = divButtonSql)
    private WebElement buttonRunSql;

    public void runSqlInScratchpad(String sql) {
        BrowserUtils.waitUsingThreadSleep(1000);
        textareaSqlScratch.sendKeys(sql);
        logger.info(String.format("input sql %s into SQL ScratchPad", sql));
        buttonRunSql.click();
        logger.info("click button Run");
        BrowserUtils.waitUsingThreadSleep(10000);
    }
}
