package com.td.vp.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.utils.ViewpointWebTable;
import com.td.em.qa.utils.WebMenu;
import com.td.em.qa.utils.WebTable;
import com.td.vp.qa.model.Connector;
import com.td.vp.qa.model.Fabric;
import com.td.vp.qa.model.Link;
import com.td.vp.qa.model.QueryGridSystem;

public class QueryGridPage {

    private static final Logger logger = LoggerFactory.getLogger(QueryGridPage.class);
    //private WebDriver webdriver;

    public QueryGridPage(WebDriver d) {
      //  this.webdriver = d;
        PageFactory.initElements(d, this);
    }

    @FindBy(how = How.CSS, using = "nav.nav ul")
    private WebElement tableFabricOverview;
    private By byTableFabricRows = By.cssSelector("li");
    private By byTableFabricColumns = By.cssSelector("span");

    private void choseFromOverviewList(String name) {
        WebTable mylist = new WebTable(tableFabricOverview, byTableFabricRows, byTableFabricColumns);
        int rowIndex = mylist.getIndexOfRowWithColumnText(0, name);
        mylist.getCell(rowIndex, 0).click();
        logger.info(String.format("click %s from Fabric overview list", name));
        // BrowserUtils.waitUsingThreadSleep(5000);
    }

    @FindBy(how = How.CSS, using = "button[title=\"Add Fabric\"]")
    private WebElement buttonAddFabric;
    @FindBy(how = How.CSS, using = "form.v-form input[name=\"name\"]")
    private WebElement fabricName;
    @FindBy(how = How.CSS, using = "form.v-form input[name=\"port\"]")
    private WebElement fabricPort;
    @FindBy(how = How.CSS, using = "div.v-dialog-footer button")
    private WebElement buttonSave;

    public void add(Fabric fabric) {
        choseFromOverviewList("Fabrics");
        buttonAddFabric.click();
        logger.info("click button add Fabric");
        fabricName.sendKeys(fabric.getName());
        fabricPort.sendKeys(Integer.toString(fabric.getPort()));
        buttonSave.click();
        logger.info("click button Save");
    }

    @FindBy(how = How.CSS, using = "div.fabricsTabView div.TDPortalResizable div.fixedColumns div.body")
    private WebElement tableFabricsAction;

    @FindBy(how = How.CSS, using = "div.fabricsTabView div.TDPortalResizable div.viewport div.body")
    private WebElement tableFabrics;

    @FindBy(how = How.CSS, using = "div.components.for_fabrics div.summaryDataGrid div.menu-overlay ul")
    private WebElement menuFabricsAction;

    @FindBy(how = How.CSS, using = "div.v-portlet-modal-container div.v-dialog-footer button")
    private WebElement buttonDelete;

    public void delete(Fabric fabric) {
        choseFromOverviewList("Fabrics");
        ViewpointWebTable fabricsTable = new ViewpointWebTable(tableFabrics);
        int rowIndex = fabricsTable.getIndexOfRowWithColumnText(5, fabric.getName());
        ViewpointWebTable fabricsActionTable = new ViewpointWebTable(tableFabricsAction);
        WebElement enableButton = fabricsActionTable.getCell(rowIndex, 0).findElement(By.cssSelector("button"));
        WebMenu menu = new WebMenu(enableButton, menuFabricsAction);
        menu.clickEnableButton();
        menu.select("Delete");
        buttonDelete.click();
        logger.info("click button Delete");
    }

    @FindBy(how = How.CSS, using = "button[title=\"Add Connector\"]")
    private WebElement buttonAddConnector;

    public void add(Connector connector) {
        choseFromOverviewList("Fabrics");
        ViewpointWebTable fabricsTable = new ViewpointWebTable(tableFabrics);
        int rowIndex = fabricsTable.getIndexOfRowWithColumnText(5, connector.getFabric().getName());
        fabricsTable.getCell(rowIndex, 5).click();
        logger.info(String.format("click fabric with name %s", connector.getFabric().getName()));
        choseFromOverviewList("Connectors");
    }
    @FindBy(how = How.CSS, using = "button[title=\"Add Link\"]")
    private WebElement buttonAddLink;

    public void add(Link link) {
        choseFromOverviewList("Fabrics");
        ViewpointWebTable fabricsTable = new ViewpointWebTable(tableFabrics);
        int rowIndex = fabricsTable.getIndexOfRowWithColumnText(5, link.getFabric().getName());
        fabricsTable.getCell(rowIndex, 5).click();
        logger.info(String.format("click fabric with name %s", link.getFabric().getName()));
        choseFromOverviewList("Links");
    }

    public void add(QueryGridSystem system) {
        choseFromOverviewList("Systems");
    }
}
