package com.td.vp.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.utils.BrowserUtils;
import com.td.em.qa.utils.ViewpointWebTable;
import com.td.em.qa.utils.WebMenu;
import com.td.vp.qa.model.Datalab;
import com.td.vp.qa.model.DatalabGroup;
import com.td.vp.qa.model.MonitoredSystem;

/**
 * @author Jin Chen
 *
 */
public class DatalabsPage {

    private WebDriver webdriver;

    public DatalabsPage(WebDriver d) {
        this.webdriver = d;
        PageFactory.initElements(d, this);
    }

    private static final Logger logger = LoggerFactory.getLogger(DatalabsPage.class);
    @FindBy(how = How.CSS, using = "div.v-portlet-login-modal-container form select")
    private WebElement selectSystem;
    @FindBy(how = How.CSS, using = "div.v-portlet-login-modal-container form input[name=\"username\"]")
    private WebElement inputUsernameSystem;
    @FindBy(how = How.CSS, using = "div.v-portlet-login-modal-container form input[name=\"password\"]")
    private WebElement inputPasswordSystem;
    @FindBy(how = How.CSS, using = "div.v-portlet-login-modal-container div.v-dialog-modal-footer button")
    private WebElement buttonConnectSystem;

    private void connect(MonitoredSystem system) {
        BrowserUtils.getDropDownOptionText(this.selectSystem, system.getName()).click();
        logger.info(String.format("Select system with name \"%s\"", system.getName()));
        BrowserUtils.waitUsingThreadSleep(500);
        this.inputUsernameSystem.clear();
        this.inputUsernameSystem.sendKeys(system.getCredential().getUsername());
        this.inputPasswordSystem.clear();
        this.inputPasswordSystem.sendKeys(system.getCredential().getPassword());
        this.buttonConnectSystem.click();
        logger.info(String.format("click button %s", buttonConnectSystem.getText()));
    }

    @FindBy(how = How.CSS, using = "div.tabs div.v-tab-content div.groups-section button")
    private WebElement buttonAddDatalabGroup;

    @FindBy(how = How.CSS, using = "form input[name=\"groupName\"]")
    private WebElement inputLabGroupName;

    @FindBy(how = How.CSS, using = "div.v-portlet-modal-container div.v-stepper-content form div.v-form-row+div+div a")
    private WebElement linkParentDB;

    @FindBy(how = How.CSS, using = "div#select2-drop div.select2-search input")
    private WebElement inputParentDB;

    @FindBy(how = How.CSS, using = "form input[name=\"permSpace\"]")
    private WebElement inputlabGroupSize;

    @FindBy(how = How.CSS, using = "div.datalabs-wizard div.v-dialog-footer button[id^=next]")
    private WebElement buttonNext;

    @FindBy(how = How.CSS, using = "div.v-portlet-modal-container div.v-dialog-footer button+button+button")
    private WebElement buttonCreate;

    @FindBy(how = How.CSS, using = "form input[name=\"defaultLabPermspace\"]")
    private WebElement inputDefaultLabSize;

    @FindBy(how = How.CSS, using = "div.datalabs-portlet div.create-group button")
    private WebElement buttonCreateDatalabGroup;

    public void add(DatalabGroup datalabGroup) {
        connect(datalabGroup.getSystem());
        BrowserUtils.waitUsingThreadSleep(2000);// TODO change to wait for visible of WebElement
        if (BrowserUtils.isWebElementExist(buttonCreateDatalabGroup)) {
            buttonCreateDatalabGroup.click();
            logger.info("click button Create-datalab-group");
        } else {
            buttonAddDatalabGroup.click();
            logger.info("click button add-datalab-group");
        }
        this.inputLabGroupName.sendKeys(datalabGroup.getName());
        logger.info(String.format("input data-lab-group name : %s", datalabGroup.getName()));
        this.linkParentDB.click();
        logger.info("click parent database link");
        this.inputParentDB.sendKeys(datalabGroup.getParentDatabase());
        BrowserUtils.waitUsingThreadSleep(1000);
        this.inputParentDB.sendKeys(Keys.ENTER);
        logger.info(String.format("input parent database : %s", datalabGroup.getParentDatabase()));
        this.inputlabGroupSize.sendKeys(Integer.toString(datalabGroup.getLabGroupSize()));
        logger.info(String.format("input data-lab-group size : %d", datalabGroup.getLabGroupSize()));
        BrowserUtils.waitForElementClickble(webdriver, buttonNext);
        this.buttonNext.click();
        logger.info("click button Next");
        BrowserUtils.waitUsingThreadSleep(500);
        this.inputDefaultLabSize.sendKeys(Integer.toString(datalabGroup.getDefaultLabSize()));
        logger.info(String.format("input default-lab-size : %d", datalabGroup.getDefaultLabSize()));
        BrowserUtils.waitUsingThreadSleep(500);

        for (int i = 0; i < 5; i++) {
            this.buttonNext.click();
            logger.info("click button Next");
        }

        this.buttonCreate.click();
        logger.info("click button Create");
        BrowserUtils.waitUsingThreadSleep(5000);
    }

    @FindBy(how = How.CSS, using = "div.tabs div.v-tab-content div.labs-section button")
    private WebElement buttonAddDatalab;

    @FindBy(how = How.CSS, using = "form input[name=\"labName\"]")
    private WebElement inputLabName;

    @FindBy(how = How.CSS, using = "form input[name=\"labSize\"]")
    private WebElement inputLabSize;

    @FindBy(how = How.CSS, using = "div.v-portlet-modal-container div.v-dialog-footer button")
    private WebElement buttonSubmit;

    public void add(Datalab datalab) {
        connect(datalab.getDatalabGroup().getSystem());
        if (isExist(datalab.getDatalabGroup())) {
            BrowserUtils.waitUsingThreadSleep(500);
            this.buttonAddDatalab.click();
            logger.info("click button add-datalab");
            this.inputLabName.sendKeys(datalab.getLabName());
            logger.info(String.format("input data-lab-group name : %s", datalab.getLabName()));
            this.inputLabSize.sendKeys(Integer.toString(datalab.getLabSize()));
            logger.info(String.format("input data-lab-size : %d", datalab.getLabSize()));
            this.buttonSubmit.click();
            logger.info("click button Submit");
        }
    }

    @FindBy(how = How.CSS, using = "div.groups-section div.TDPortalResizable div.viewport div.body")
    private WebElement tableDatalabGroup;

    @FindBy(how = How.CSS, using = "div.groups-section div.fixedColumns div.body")
    private WebElement tableDatalabGroupAction;

    @FindBy(how = How.CSS, using = "div.groups-section div.TDPortalResizable div.userOverlay ul")
    private WebElement menuRoot;

    @FindBy(how = How.CSS, using = "div.v-dialog-container div.v-dialog-footer div.v-dialog-buttons button")
    private WebElement buttonOK;

    public void delete(DatalabGroup datalabGroup) {
        connect(datalabGroup.getSystem());
        if (isExist(datalabGroup)) {
            ViewpointWebTable table = new ViewpointWebTable(this.tableDatalabGroup);
            int colIndex = 2;
            int rowIndex = table.getIndexOfRowWithColumnText(colIndex, datalabGroup.getName());
            ViewpointWebTable tableAction = new ViewpointWebTable(this.tableDatalabGroupAction);
            WebElement menuEnableButton = tableAction.getCell(rowIndex, 0).findElement(By.cssSelector("button"));
            WebMenu menu = new WebMenu(menuEnableButton, menuRoot);
            menu.clickEnableButton();
            menu.select("Delete Lab Group");
            BrowserUtils.waitUsingThreadSleep(1000);
            this.buttonOK.click();
            logger.info(String.format("click button %s", buttonOK.getText()));
            BrowserUtils.waitUsingThreadSleep(1000);
        }
    }

    @FindBy(how = How.CSS, using = "div.labs-section div.TDPortalResizable div.viewport div.body")
    private WebElement tableDatalab;

    @FindBy(how = How.CSS, using = "div.labs-section div.fixedColumns div.body")
    private WebElement tableDatalabAction;

    @FindBy(how = How.CSS, using = "div.labs-section div.labs-overlay ul")
    private WebElement menuRootDatalab;
    @FindBy(how = How.CSS, using = "div.labs-section div.labs-overlay ul span.vpmenu-btn-down")
    private WebElement menuDown;

    @FindBy(how = How.CSS, using = "div.v-dialog-container div.v-dialog-footer div.v-dialog-buttons button")
    private WebElement buttonSubmitDatalab;

    public void delete(Datalab datalab) {
        connect(datalab.getDatalabGroup().getSystem());
        if (isExist(datalab.getDatalabGroup())) {
            ViewpointWebTable table = new ViewpointWebTable(tableDatalab);
            int colIndex = 2;
            int rowIndex = table.getIndexOfRowWithColumnText(colIndex, datalab.getLabName());
            ViewpointWebTable tableAction = new ViewpointWebTable(tableDatalabAction);
            WebElement menuEnableButton = tableAction.getCell(rowIndex, 0).findElement(By.cssSelector("button"));
            WebMenu menu = new WebMenu(menuEnableButton, menuRootDatalab);
            menu.clickEnableButton();
            menuDown.click();
            menuDown.click();
            logger.info("click button scroll down menu twice");
            WebMenu menu1 = new WebMenu(menuEnableButton, menuRootDatalab, By.cssSelector("li:not(.vpmenu-disabled)"));
            menu1.select("Delete Lab");
            buttonSubmitDatalab.click();
            BrowserUtils.waitUsingThreadSleep(3000);
        }
    }

    public boolean isExist(Datalab datalab) {
        // connect(datalab.getDatalabGroup().getSystem());
        if (isExist(datalab.getDatalabGroup())) {
            ViewpointWebTable table = new ViewpointWebTable(this.tableDatalab);
            int colIndex = 2;
            int rowIndex = table.getIndexOfRowWithColumnText(colIndex, datalab.getLabName());
            table.getCell(rowIndex, colIndex).click();
            BrowserUtils.waitUsingThreadSleep(1000);
            return true;
        } else
            return false;
    }

    public boolean isExist(DatalabGroup datalabGroup) {
        // connect(datalabGroup.getSystem());
        ViewpointWebTable table = new ViewpointWebTable(this.tableDatalabGroup);
        int colIndex = 2;
        int rowIndex = table.getIndexOfRowWithColumnText(colIndex, datalabGroup.getName());
        table.getCell(rowIndex, colIndex).click();
        BrowserUtils.waitUsingThreadSleep(1000);
        return true;
    }
}
