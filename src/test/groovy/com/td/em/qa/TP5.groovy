/**
 * 
 */
package com.td.em.qa

import static org.junit.Assert.*
import java.util.List
import com.td.em.qa.domain.*
import com.td.em.qa.pages.*
import com.td.em.qa.utils.*

import org.junit.Test
import org.openqa.selenium.*
import com.td.em.qa.constants.*

/**
 * @author Jin Chen
 *
 */
class TP5 extends AbstractTestCaseBase {

    private ManageDaemonGroupsPage navigateToDaemons(){
        ManageDaemonGroupsPage p5 = new LoginPage(d, u)
                .signIn()
                .clickPortalAdminButton()
                .clickEcosystemConfiguration()
                .clickDaemons()
        return p5
    }
    private void addDaemonGroup(DaemonGroup dg){
        ManageDaemonGroupsPage p5 = navigateToDaemons()
        ManageDaemonGroupsPage p6=null
        if (p5.isDaemonGroupExist(dg.name)) {
            p6 = p5.removeDaemonGroup(dg.name)
        }else{
            p6=p5
        }
        assertFalse(p6.isDaemonGroupExist(dg.name))
        def p8 = p6
                .clickAddDaemonGroup()
                .createDaemonGroup(dg)
        assertTrue(p8.isDaemonGroupExist(dg.name))
        def p9 = p8.close()
        p9.verifyEMExplorerTable()
    }

    @Test
    void test_edit_daemon_group(){
        List<HaTransition> haTransList = new ArrayList<HaTransition>()
        haTransList.add(HaTransition.A2P)
        haTransList.add(HaTransition.OOS2P)
        haTransList.add(HaTransition.OOS2S)
        haTransList.add(HaTransition.P2S)
        haTransList.add(HaTransition.S2P)
        def dm1 = new Daemon(server:"xorn",
        name:"as-sh-cmdargs",
        currentState:DaemonState.Active)
        List<Daemon> dmlist = new ArrayList<Daemon>()
        dmlist.add(dm1)
        def dg = new DaemonGroup()
        dg.with {
            name="DG-AS-SH-CMDARGS"
            haPattern=HaPattern.AS
            executableName=ExecutableName.OTHER
            otherInput="ThisIsTest"
            validHaTransitions = haTransList
            daemons = dmlist
        }
        def p5 = navigateToDaemons()
        EditDaemonGroupPage p6 = p5.clickDaemonGroup(dg)
        def p8 = p6.updateDaemonGroup()
    }
    @Test
    public void test_Action_Rules(){
        def dg = new DaemonGroup(name:"DG-AS-SH-CMDARGS")
        Action a1 = new Action(name:"email_me")
        List<Action> al = new ArrayList<Action>()
        al.add(a1)
        Rule r1 = new Rule(severity:Severity.Warning
        ,actions:al)
        Map<String,Rule> rules =new LinkedHashMap<String,Rule>()
        rules.put("Rules for Daemon Group",r1)
        rules.put("Rules for daemon on sdld0345",r1)
        rules.put("Rules for daemon on sdl01187",r1)
        def p5 = navigateToDaemons()
        def p6 = p5.clickDaemonGroup(dg)
        ActionRulesPage p7 = p6.createActionRulesPage()
        p7.setRules(rules)
        p7.clickAddRules()
    }

    @Test
    public void test_AS_SH_CMDARGS(){
        List<HaTransition> h = new ArrayList<HaTransition>()
        h.add(HaTransition.OOS2P)
        h.add(HaTransition.OOS2S)
        h.add(HaTransition.P2S)
        h.add(HaTransition.S2P)
        h.add(HaTransition.A2P)
        def dm1 = new Daemon(server:"xorn",
        name:"as-sh-cmdargs",
        currentState:DaemonState.Active,
        customSearchPatternValue: "ThisIsCustomeValue")
        def dm2 = new Daemon(
                server:"angel",
                name:"as-sh-cmdargs",
                currentState:DaemonState.Standby
                )
        List<Daemon> dmlist = new ArrayList<Daemon>()
        dmlist.add(dm1)
        dmlist.add(dm2)

        def dg = new DaemonGroup(
                name:"DG-AS-SH-CMDARGS",
                executeUser:"root",
                executePassword:"emdev2015",
                globalSearchPatternValue:"/var/lib/dummydaemon.sh",
                executableName:ExecutableName.SH,
                haPattern:HaPattern.AS,
                identifyDaemonsBy:IdentifyDaemonsBy.CMDARGS,
                daemons:dmlist,
                validHaTransitions: h,
                stateChangeControlScript: "/usr/local/bin/abc.sh",
                )
        addDaemonGroup(dg)
    }
}