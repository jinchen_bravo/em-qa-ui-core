/**
 * 
 */
package com.td.em.qa

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.td.em.qa.constants.*
import com.td.em.qa.domain.DaemonGroup

/**
 * @author Jin Chen
 *
 */
class TP8 {

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test_enum(){
        def x = HaPattern.AA
        println HaPattern.forName("Active/Standby").name()
        println x.toString()
        
    }

    @Test
    void test_executable_other(){
        def dg = new DaemonGroup(executableName:ExecutableName.OTHER,
        otherInput: "test123")
        assertEquals(dg.executableName,ExecutableName.OTHER)
    }
}
