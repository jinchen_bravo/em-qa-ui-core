/**
 * 
 */
package com.td.em.qa

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.hidetake.groovy.ssh.Ssh

/**
 * @author Jin Chen
 *
 */
class TestXmlFilesandSsh {

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void test_xml_file() {
        URL url = this.getClass().getResource("/emmonitorconfig.xml");
        File emmonitorFile = new File(url.getFile())
        def emmonitorXmlString = emmonitorFile.text
        def root = new XmlSlurper().parseText(emmonitorXmlString)

        def listDisplayName = root.metricCollector.metric["@displayName"].toList().unique()

        println listDisplayName.size()

        listDisplayName.each { x ->
            def node = root.metricCollector.metric.findAll{ it.@displayName == x}.each {
                println it.@displayName
                println it.@resourceType
                println it.arg[0]
                println it.arg[1]
            }
        }
    }

    @Test
    public void test_ssh(){
        def ssh = Ssh.newService()
        ssh.settings { knownHosts = allowAnyHosts }
        ssh.remotes {
            xorn {
                host = 'xorn'
                user = 'root'
                password = 'emdev2015'
            }
            amanda {
                host = 'amanda'
                user = 'root'
                password = 'emdev2015'
            }
        }
        ssh.run {
            session(ssh.remotes.xorn){
                execute ('tail -n 50 /var/log/messages', logging: 'none')
                {result -> println result }
            }
        }
    }
}
