/**
 * 
 */
package com.td.em.qa

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.td.em.qa.domain.*
import com.td.em.qa.pages.*
import com.td.em.qa.utils.*
import com.td.em.qa.constants.*

/**
 * @author Jin Chen
 *
 */
class TP7 extends AbstractTestCaseBase {

    /* (non-Javadoc)
     * @see com.td.em.qa.TestCaseBase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    /* (non-Javadoc)
     * @see com.td.em.qa.TestCaseBase#tearDown()
     */
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void test() {
        def p1 = new LoginPage(d, u)
        ViewpointPage p2 = p1.signIn()
        BrowserUtils.waitForElementVisible(d,p2.linkHome)
        p2.verifyEMExplorerTable()
    }
}
