/**
 * 
 */
package com.td.em.qa

import java.util.Arrays;
import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test
import org.junit.runners.Parameterized.Parameters;

import com.td.em.qa.constants.ActionMessage
import com.td.em.qa.domain.EMActionCM
import com.td.em.qa.domain.EMActionES
import com.td.em.qa.domain.TDAlertActionSet
import com.td.em.qa.pages.LoginPage;
import com.td.em.qa.pages.ManageDaemonGroupsPage;;
import com.td.em.qa.pages.actions.AddCustomMessageActionPage
import com.td.em.qa.pages.actions.AddExecuteScriptActionPage

/**
 * @author Jin Chen
 *
 */
class TestActions extends AbstractTestCaseBase {

    /* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    /* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#tearDown()
     */
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }


    @Test
    public void test_Execute_Script_Action() {
        def p5 = new LoginPage(d, u)
                .signIn()
                .clickPortalAdminButton()
                .clickEcosystemConfiguration()
                .clickActions()

        def a1 = new EMActionES(
                name:"Action111",
                enabled: Boolean.TRUE
                )
        if(p5.isExist(a1)){
            p5.remove(a1)
            p5.add(a1)
        }
        else
            p5.add(a1)
    }

    @Test
    void test_update_Execute_script_Action(){
        def p5 = new LoginPage(d, u)
                .signIn()
                .clickPortalAdminButton()
                .clickEcosystemConfiguration()
                .clickActions()

        def a1 = new EMActionES(
                name:"Action111",
                enabled: Boolean.FALSE,
                executionVisibleInExplorerJobView: Boolean.TRUE
                )
        p5.update(a1)
    }

    @Test
    void test_custom_message_action(){
        def p5 = new LoginPage(d, u)
                .signIn()
                .clickPortalAdminButton()
                .clickEcosystemConfiguration()
                .clickActions()

        def a1 = new EMActionCM(
                name:"Action222",
                enabled: Boolean.TRUE,
                textSubject: "abc"+"["+ActionMessage.EM_ALERTCODE.name()+"]",
                textBody: "bbc"+"["+ActionMessage.EM_APPLICATIONID.name()+"]"
                )
        if(p5.isExist(a1)){
            p5.remove(a1)
            p5.add(a1)
        }
        else
            p5.add(a1)
    }

    @Test
    void test_update_custome_message_action(){
        def p5 = new LoginPage(d, u)
                .signIn()
                .clickPortalAdminButton()
                .clickEcosystemConfiguration()
                .clickActions()
               
        def a1 = new EMActionCM(
                name:"Action222",
                enabled: Boolean.FALSE,
                teradataAlertsAction: new TDAlertActionSet(name: "email_me"),
                textSubject: "abc"+"["+ActionMessage.EM_MESSAGE.name()+"]",
                textBody: "bbc"+"["+ActionMessage.EM_APPLICATIONID.name()+"]"
                )
        p5.update(a1)
    }
}
