/**
 * 
 */
package com.td.em.qa

import static org.junit.Assert.*;
import com.td.em.qa.constants.*

import com.td.em.qa.domain.Daemon
import com.td.em.qa.domain.DaemonGroup
import com.td.em.qa.pages.EditDaemonGroupPage
import com.td.em.qa.pages.LoginPage
import com.td.em.qa.pages.ManageDaemonGroupsPage
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Jin Chen
 *
 */
class TP3 extends AbstractTestCaseBase {

    /* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    /* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#tearDown()
     */
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void test_scroll() {
        ManageDaemonGroupsPage p5 = new LoginPage(d, u)
                .signIn()
                .clickPortalAdminButton()
                .clickEcosystemConfiguration()
                .clickDaemons()
        if (p5.isDaemonGroupExist("daemonGroup16")){
            println "Exist"
        }else{
            println "NOT Exist"
        }
    }

    @Test
    void test_daemon_group_pagination(){
        ManageDaemonGroupsPage p5 = new LoginPage(d, u)
                .signIn()
                .clickPortalAdminButton()
                .clickEcosystemConfiguration()
                .clickDaemons()
        p5.gotoPageNumber("2")
                .gotoPreviousPage()
                .gotoLastPage()
                .gotoFirstPage()
                .gotoNextPage()
    }

    @Test
    void test_daemon_group_multiple_pages(){
        ManageDaemonGroupsPage p5 = new LoginPage(d, u)
                .signIn()
                .clickPortalAdminButton()
                .clickEcosystemConfiguration()
                .clickDaemons()
        assertTrue(p5.isDaemonGroupExistOnAnyPageOfMultiplePages("DemodaemonGroup1"))
        p5.gotoFirstPage()
        assertFalse(p5.isDaemonGroupExistOnAnyPageOfMultiplePages("DemodaemonGroup2"))
    }

    @Test
    void test_edit_daemongroup_with_scroll(){
        def dm1 = new Daemon(server:"angel",
        name:"as-sh-cmdargs",
        currentState:DaemonState.Active)
        List<Daemon> dmlist = new ArrayList<Daemon>()
        dmlist.add(dm1)
        def dg = new DaemonGroup()
        dg.with {
            name="daemonGroup15"
            haPattern=HaPattern.AS
            executableName=ExecutableName.OTHER
            otherInput="ThisIsTest"
            daemons = dmlist
        }
        ManageDaemonGroupsPage p5 = new LoginPage(d, u)
                .signIn()
                .clickPortalAdminButton()
                .clickEcosystemConfiguration()
                .clickDaemons()

        EditDaemonGroupPage p6 = p5.clickDaemonGroup(dg)
        def p8 = p6.updateDaemonGroup()
    }
}
