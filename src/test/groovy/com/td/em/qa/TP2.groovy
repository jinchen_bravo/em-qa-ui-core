/**
 * 
 */
package com.td.em.qa

import static org.junit.Assert.*;

import com.td.em.qa.domain.Daemon
import com.td.em.qa.domain.DaemonGroup
import com.td.em.qa.pages.LoginPage
import com.td.em.qa.pages.ManageDaemonGroupsPage;
import com.td.em.qa.constants.*

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Jin Chen
 *
 */
class TP2 extends AbstractTestCaseBase {

    /* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    /* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#tearDown()
     */
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
    private ManageDaemonGroupsPage navigateToDaemons(){
        ManageDaemonGroupsPage p5 = new LoginPage(d, u)
                .signIn()
                .clickPortalAdminButton()
                .clickEcosystemConfiguration()
                .clickDaemons()
        return p5
    }
    private void addDaemonGroup(DaemonGroup dg){
        ManageDaemonGroupsPage p5 = navigateToDaemons()
        ManageDaemonGroupsPage p6=null
        if (p5.isDaemonGroupExist(dg.name)) {
            p6 = p5.removeDaemonGroup(dg.name)
        }else{
            p6=p5
        }
        assertFalse(p6.isDaemonGroupExist(dg.name))
        def p8 = p6
                .clickAddDaemonGroup()
                .createDaemonGroup(dg)
        assertTrue(p8.isDaemonGroupExist(dg.name))
        def p9 = p8.close()
        //p9.verifyEMExplorerTable()
    }

    @Test
    public void test_AA_SH_CMDARGS(){

        def dm = new Daemon()
        dm.with {
            server="angel"
            name="aa-sh-cmdargs"
            currentState=DaemonState.Active
        }
        List<Daemon> dmlist = new ArrayList<Daemon>()
        dmlist.add(dm)
        def dg = new DaemonGroup()
        dg.with {
            name="DG-AA-SH-CMDARGS"
            executeUser="root"
            executePassword="emdev2015"
            globalSearchPatternValue="dummydaemon.sh"
            executableName=ExecutableName.SH
            haPattern=HaPattern.AA
            identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
            daemons=dmlist
        }

        addDaemonGroup(dg)
    }
    
        @Test
        public void test_AA_SH_CMDARGS_WILDCARDBOTH(){
    
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-sh-cmdargs-wildcardboth"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-SH-CMDARGS-WILDCARDBOTH"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="*dummydaemon*.sh"
                executableName=ExecutableName.SH
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
            }
    
            addDaemonGroup(dg)
        }
    
        @Test
        public void test_AA_SH_CMDARGS_WILDCARDPREFIX(){
    
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-sh-cmdargs-wildcardprefix"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-SH-CMDARGS-WILDCARDPREFIX"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="*dummydaemon.sh"
                executableName=ExecutableName.SH
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
            }
    
            addDaemonGroup(dg)
        }
    
        @Test
        public void test_AA_SH_CMDARGS_WILDCARDSUFFIX(){
    
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-sh-cmdargs-wildcardsuffix"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-SH-CMDARGS-WILDCARDSUFFIX"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="dummydaemon*.sh"
                executableName=ExecutableName.SH
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
            }
    
            addDaemonGroup(dg)
        }
    
    
        @Test
        public void test_AA_SH_FULLPATH() {
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-sh-fullpath"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-SH-FULLPATH"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="/var/lib/dummydaemon.sh"
                executableName=ExecutableName.SH
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
            }
            addDaemonGroup(dg)
        }
    
        @Test
        public void test_AA_BASH_CMDARGS(){
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-bash-cmdargs"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-BASH-CMDARGS"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="dummydaemon.sh"
                executableName=ExecutableName.BASH
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
            }
    
            addDaemonGroup(dg)
        }
    
        @Test
        public void test_AA_BASH_CMDARGS_WILDCARDBOTH(){
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-bash-cmdargs-wildcardboth"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-BASH-CMDARGS-WILDCARDBOTH"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="*dummydaemon*.sh"
                executableName=ExecutableName.BASH
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
            }
    
            addDaemonGroup(dg)
        }
    
        @Test
        public void test_AA_BASH_CMDARGS_WILDCARDPREFIX(){
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-bash-cmdargs-wildcardprefix"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-BASH-CMDARGS-WILDCARDPREFIX"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="*dummydaemon.sh"
                executableName=ExecutableName.BASH
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
            }
    
            addDaemonGroup(dg)
        }
    
        @Test
        public void test_AA_BASH_CMDARGS_WILDCARDSUFFIX(){
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-bash-cmdargs-wildcardsuffix"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-BASH-CMDARGS-WILDCARDSUFFIX"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="dummydaemon*.sh"
                executableName=ExecutableName.BASH
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
            }
    
            addDaemonGroup(dg)
        }
    
    
        @Test
        public void test_AA_BASH_FULLPATH() {
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-bash-fullpath"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-BASH-FULLPATH"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="/var/lib/dummydaemon.sh"
                executableName=ExecutableName.BASH
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
            }
            addDaemonGroup(dg)
        }
    
    
        @Test
        public void test_AA_JAVA_FULLCLASSNAME() {
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-java-fullclassname"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-JAVA-FULLCLASSNAME"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="com.td.em.qa.sdaemon.Main"
                executableName=ExecutableName.JAVA
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.JAVACLASS
                daemons=dmlist
            }
    
            addDaemonGroup(dg)
        }
        @Test
        public void test_AA_JAVA_CMDARGS() {
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-java-cmdargs"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-JAVA-CMDARGS"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="-classpath lib/em-qa-sdaemon-1.0.0.jar"
                executableName=ExecutableName.JAVA
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
            }
            addDaemonGroup(dg)
        }
    
        @Test
        public void test_AA_OTHER_CMDARGS(){
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-other-cmdargs"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-OTHER-CMDARGS"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="dummydaemon.sh"
                executableName=ExecutableName.OTHER
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.EXECNAME
                daemons=dmlist
                otherInput="ThisIsTest"
            }
    
            addDaemonGroup(dg)
        }
        @Test
        public void test_AA_OTHER_FULLPATH() {
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa-other-fullpath"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA-OTHER-FULLPATH"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="/var/lib/dummydaemon.sh"
                executableName=ExecutableName.OTHER
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
                otherInput="ThisIsTest"
            }
            addDaemonGroup(dg)
        }

        @Test
        public void test_AA1_OTHER_FULLPATH() {
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa1-other-fullpath"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA1-OTHER-FULLPATH"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="/var/lib/dummydaemon.sh"
                executableName=ExecutableName.OTHER
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
                otherInput="ThisIsTest"
            }
            addDaemonGroup(dg)
        }
        @Test
        public void test_AA2_OTHER_FULLPATH() {
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa2-other-fullpath"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA2-OTHER-FULLPATH"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="/var/lib/dummydaemon.sh"
                executableName=ExecutableName.OTHER
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
                otherInput="ThisIsTest"
            }
            addDaemonGroup(dg)
        }
        @Test
        public void test_AA3_OTHER_FULLPATH() {
            def dm = new Daemon()
            dm.with {
                server="angel"
                name="aa3-other-fullpath"
                currentState=DaemonState.Active
            }
            List<Daemon> dmlist = new ArrayList<Daemon>()
            dmlist.add(dm)
            def dg = new DaemonGroup()
            dg.with {
                name="DG-AA3-OTHER-FULLPATH"
                executeUser="root"
                executePassword="emdev2015"
                globalSearchPatternValue="/var/lib/dummydaemon.sh"
                executableName=ExecutableName.OTHER
                haPattern=HaPattern.AA
                identifyDaemonsBy=IdentifyDaemonsBy.CMDARGS
                daemons=dmlist
                otherInput="ThisIsTest"
            }
            addDaemonGroup(dg)
        }

}

