/**
 * 
 */
package com.td.em.qa

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver
import com.td.em.qa.domain.Credential
import com.td.em.qa.utils.BrowserDriver

/**
 * @author Jin Chen
 *
 */
abstract class AbstractTestCaseBase {
    protected WebDriver d= BrowserDriver.getCurrentDriver()
    protected Credential u = new Credential()
    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception {
        u.with {
            username="swaggertester1"
            password="Qwert!2345"
        }
        BrowserDriver.loadPage("http://sdl27488.labs.teradata.com")
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception {
        BrowserDriver.close()
    }
}
