/**
 * 
 */
package com.td.em.qa

import static org.junit.Assert.*
import com.td.em.qa.constants.*

import com.td.em.qa.domain.Daemon
import com.td.em.qa.domain.DaemonGroup
import com.td.em.qa.pages.LoginPage
import com.td.em.qa.pages.ee.EEDaemonListPage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Jin Chen
 *
 */
class TP1 extends AbstractTestCaseBase {

    /* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    /* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#tearDown()
     */
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    void test_EME_tab(){
        def p2 = new LoginPage(d, u)
                        .signIn()
                        .clickTabExplorer()
        def p3 = p2.clickTabDaemons()
        assertEquals(6, p3.getEmExplorerTab().getTabCount())
        assertEquals("Daemons", p3.getEmExplorerTab().getSelectedTab())
    }

    @Test
    void test_Ecosystem_Explorer_pagination(){
        def p2 = new LoginPage(d, u).signIn()
        def p3 = new EEDaemonListPage(d).clickTabDaemons()
        p3.portletOption("REFRESH")
        p3.portletOption("PAUSE")
        p3.gotoPageNumber("2")
                .gotoPreviousPage()
                .gotoLastPage()
                .gotoFirstPage()
                .gotoNextPage()
    }

    @Test
    public void test_Ecosystem_Explorer() {
        def p2 = new LoginPage(d, u).signIn()
        List<HaTransition> h = new ArrayList<HaTransition>()
        h.add(HaTransition.OOS2P)
        h.add(HaTransition.OOS2S)
        h.add(HaTransition.P2S)
        h.add(HaTransition.S2P)
        h.add(HaTransition.A2P)
        def dm1 = new Daemon(server:"sdl01187",
        name:"as-sh-cmdargs",
        currentState:DaemonState.Active)
        def dm2 = new Daemon(
                server:"sdld0345",
                name:"as-sh-cmdargs",
                currentState:DaemonState.Standby)
        List<Daemon> dmlist = new ArrayList<Daemon>()
        dmlist.add(dm1)
        dmlist.add(dm2)
        def dg = new DaemonGroup(
                name:"DG-AS-SH-CMDARGS",
                executeUser:"root",
                executePassword:"TCAMPass123",
                globalSearchPatternValue:"/var/lib/dummydaemon.sh",
                executableName:ExecutableName.SH,
                haPattern:HaPattern.AS,
                identifyDaemonsBy:IdentifyDaemonsBy.CMDARGS,
                daemons:dmlist,
                validHaTransitions: h
                )
        def p3 = new EEDaemonListPage(d)
        p3.clickTabDaemons()
        p3.portletOption("REFRESH")
        p3.portletOption("PAUSE")
        p3.setDaemonGroup(dg)
        def p4 = p3.clickDaemon(dm2)
        p4.verifyDaemon()
    }

    @Test
    void test_daemon_state_change(){
        def p2 = new LoginPage(d, u).signIn()
        List<HaTransition> h = new ArrayList<HaTransition>()
        h.add(HaTransition.OOS2P)
        h.add(HaTransition.OOS2S)
        h.add(HaTransition.P2S)
        h.add(HaTransition.S2P)
        h.add(HaTransition.A2P)
        def dm1 = new Daemon(server:"sdl01187",
        name:"as-sh-cmdargs",
        currentState:DaemonState.Active)
        def dm2 = new Daemon(
                server:"sdld0345",
                name:"as-sh-cmdargs",
                currentState:DaemonState.Standby)
        List<Daemon> dmlist = new ArrayList<Daemon>()
        dmlist.add(dm1)
        dmlist.add(dm2)
        def dg = new DaemonGroup(
                name:"DG-AS-SH-CMDARGS",
                executeUser:"root",
                executePassword:"TCAMPass123",
                globalSearchPatternValue:"/var/lib/dummydaemon.sh",
                executableName:ExecutableName.SH,
                haPattern:HaPattern.AS,
                identifyDaemonsBy:IdentifyDaemonsBy.CMDARGS,
                daemons:dmlist,
                validHaTransitions: h
                )
        def p3 = new EEDaemonListPage(d)
        p3.clickTabDaemons()
        p3.portletOption("REFRESH")
        p3.portletOption("PAUSE")
        p3.setDaemonGroup(dg)
        p3.changeDaemonState(dm1,DaemonState.Passive)
    }
    
    @Test
    void test_ViewPointPage_with_Tab(){
        def p2 = new LoginPage(d, u).signIn()
        assertEquals(3, p2.getTab().getTabCount())
        def p3=p2.clickTabExplorer()
        assertEquals("Explorer", p3.getTab().getSelectedTab())
    }
}
