package com.td.em.qa;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.td.em.qa.constants.Condition;
import com.td.em.qa.constants.DaemonState;
import com.td.em.qa.constants.ExecutableName;
import com.td.em.qa.constants.HaPattern;
import com.td.em.qa.constants.HaTransition;
import com.td.em.qa.constants.IdentifyDaemonsBy;
import com.td.em.qa.domain.Application;
import com.td.em.qa.domain.Daemon;
import com.td.em.qa.domain.DaemonGroup;
import com.td.em.qa.domain.Job;
import com.td.em.qa.domain.Server;
import com.td.em.qa.domain.Table;
import com.td.em.qa.domain.Workflow;
import com.td.em.qa.pages.LoginPage;
import com.td.em.qa.pages.ManageDaemonGroupsPage;
import com.td.em.qa.pages.ee.EEApplicationListPage;
import com.td.em.qa.pages.ee.EEDaemonListPage;
import com.td.em.qa.pages.ee.EEDaemonMetricPage;
import com.td.em.qa.pages.ee.EEJobListPage;
import com.td.em.qa.pages.ee.EEServerListPage;
import com.td.em.qa.pages.ee.EETableListPage;
import com.td.em.qa.pages.ee.EEWorkflowListPage;

public class TestFilters extends AbstractTestCaseBase {
    private static final Logger logger = LoggerFactory
            .getLogger(TestFilters.class);

    @Test
    public void test() {
        List<HaTransition> h = new ArrayList<HaTransition>();
        h.add(HaTransition.OOS2P);
        h.add(HaTransition.OOS2S);
        h.add(HaTransition.P2S);
        h.add(HaTransition.S2P);
        h.add(HaTransition.A2P);
        Daemon dm1 = new Daemon();
        dm1.setServer("xorn");
        dm1.setName("daemon12345");
        dm1.setCurrentState(DaemonState.Active);
        Daemon dm2 = new Daemon();
        dm2.setServer("angel");
        dm2.setName("daemon12345");
        dm2.setCurrentState(DaemonState.Standby);

        List<Daemon> dmlist = new ArrayList<Daemon>();
        dmlist.add(dm1);
        dmlist.add(dm2);
        DaemonGroup dg = new DaemonGroup();
        dg.setName("DGdaemon12345");
        dg.setExecuteUser("root");
        dg.setExecutePassword("TCAMPass123");
        dg.setGlobalSearchPatternValue("/var/lib/dummydaemon.sh");
        dg.setExecutableName(ExecutableName.SH);
        dg.setHaPattern(HaPattern.AS);
        dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
        dg.setDaemons(dmlist);
        dg.setValidHaTransitions(h);

        EEDaemonListPage p5 = (EEDaemonListPage) new LoginPage(d, u).signIn()
                .clickTabExplorer().clickTab(Daemon.class);
        // p5.changeDaemonState(d1, DaemonState.Passive);
        // p5.clickDaemon(d1);
        p5.setDaemonGroup(dg);
        p5.portletOption("PAUSE");
        EEDaemonMetricPage p6 = p5.clickDaemon(dm2);
        p6.verifyDaemon();
        p6.goBack();
    }

    @Test
    public void testDaemonGroupFilter() {
        ManageDaemonGroupsPage p1 = new LoginPage(d, u).signIn()
                .clickPortalAdminButton().clickEcosystemConfiguration()
                .clickDaemons();
        logger.info(p1.filter(new DaemonGroup().withName("as")).toString());
        logger.info(p1.clearFilter().toString());
    }

    @Test
    public void testEEFilter() {
        EEDaemonListPage p1 = (EEDaemonListPage) new LoginPage(d, u).signIn()
                .clickTabExplorer().clickTab(Daemon.class);
        p1.portletOption("REFRESH");
        p1.portletOption("PAUSE");
        p1.clearFilter();
        logger.info(p1.filter(new DaemonGroup().withName("as")).toString());
        p1.clearFilter();
        logger.info(p1.filter(new Daemon().withName("as")).toString());
        p1.clearFilter();
        logger.info(p1.filter(new Server().withName("as")).toString());
        p1.clearFilter();
        logger.info(p1.filter(DaemonState.Active).toString());
        p1.clearFilter();
        logger.info(p1.filter(DaemonState.Passive).toString());
        p1.clearFilter();
        logger.info(p1.filter(DaemonState.Active).toString());
        p1.clearFilter();
        logger.info(p1.filter(DaemonState.Restore).toString());
        p1.clearFilter();
        logger.info(p1.filter(Condition.Down).toString());
        p1.clearFilter();
        logger.info(p1.filter(HaPattern.AS).toString());
        p1.clearFilter();

    }

    @Test
    public void testShiftPerspective() {
        EEDaemonListPage p1 = (EEDaemonListPage) new LoginPage(d, u).signIn()
                .clickTabExplorer().clickTab(Daemon.class);
        p1.portletOption("REFRESH");
        p1.portletOption("PAUSE");
        EEServerListPage p2 = (EEServerListPage) p1
                .shiftPerspective(Server.class);
        EETableListPage p3 = (EETableListPage) p2.shiftPerspective(Table.class);
        EEJobListPage p4 = (EEJobListPage) p3.shiftPerspective(Job.class);
        EEApplicationListPage p5 = (EEApplicationListPage) p4.shiftPerspective(Application.class);
        EEWorkflowListPage p6 =(EEWorkflowListPage) p5.shiftPerspective(Workflow.class);
        p6.clickTab(Daemon.class);
    }
}
