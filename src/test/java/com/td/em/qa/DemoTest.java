/**
 * 
 */
package com.td.em.qa;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.td.em.qa.constants.DaemonState;
import com.td.em.qa.constants.ExecutableName;
import com.td.em.qa.constants.HaPattern;
import com.td.em.qa.constants.IdentifyDaemonsBy;
import com.td.em.qa.constants.Severity;
import com.td.em.qa.domain.Action;
import com.td.em.qa.domain.Daemon;
import com.td.em.qa.domain.DaemonGroup;
import com.td.em.qa.domain.Rule;
import com.td.em.qa.pages.ActionRulesPage;
import com.td.em.qa.pages.LoginPage;
import com.td.em.qa.pages.ManageDaemonGroupsPage;

/**
 * @author Jin Chen
 *
 */
public class DemoTest extends AbstractTestCaseBase {

  /*
   * (non-Javadoc)
   * 
   * @see com.td.em.qa.AbstractTestCaseBase#setUp()
   */
  @Before
  public void setUp() throws Exception {
    super.setUp();
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.td.em.qa.AbstractTestCaseBase#tearDown()
   */
  @After
  public void tearDown() throws Exception {
    super.tearDown();
  }

  @Test
  public void test_add_daemonGroup_1() {
    Daemon dm = new Daemon();
    dm.setName("demoTest1");
    dm.setServer("angel");
    dm.setCurrentState(DaemonState.Active);

    List<Daemon> dmlist = new ArrayList<Daemon>();
    dmlist.add(dm);

    DaemonGroup dg = new DaemonGroup();
    dg.setName("DemodaemonGroup1");
    dg.setExecuteUser("root");
    dg.setExecutePassword("emdev2015");
    dg.setGlobalSearchPatternValue("/bin/dash/myscript.sh");
    dg.setExecutableName(ExecutableName.SH);
    dg.setHaPattern(HaPattern.AA);
    dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
    dg.setDaemons(dmlist);

    ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
        .clickPortalAdminButton().clickEcosystemConfiguration().clickDaemons();
    ManageDaemonGroupsPage p6 = null;
    if (p5.isExist(dg)) {
      p6 = p5.remove(dg);
    } else {
      p6 = p5;
    }
    ManageDaemonGroupsPage p8 = p6.add(dg);
    p8.close();
  }

  @Test
  public void test_add_action_rules_1() {
    Action a1 = new Action();
    a1.setName("email_me");
    List<Action> alist = new ArrayList<Action>();
    alist.add(a1);
    Rule r1 = new Rule();
    r1.setActions(alist);
    r1.setSeverity(Severity.Warning);

    Map<String, Rule> rules = new LinkedHashMap<String, Rule>();
    rules.put("Rules for Daemon Group", r1);
    rules.put("Rules for daemon on angel", r1);

    DaemonGroup dg = new DaemonGroup();
    dg.setName("DemodaemonGroup1");

    ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
        .clickPortalAdminButton().clickEcosystemConfiguration().clickDaemons();
    ActionRulesPage p6 = p5.updateActionRules(dg);
    p6.setRules(rules);
    p6.clickAddRules();
  }
}
