package com.td.em.qa;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.td.em.qa.constants.Portlets;
import com.td.em.qa.constants.ServerType;
import com.td.em.qa.domain.Credential;
import com.td.em.qa.domain.Server;
import com.td.em.qa.pages.LoginPage;
import com.td.em.qa.pages.ViewpointPage;
import com.td.em.qa.pages.server.ManageServersPage;
import com.td.em.qa.utils.PageObjectManager;
import com.td.vp.qa.model.MonitoredSystem;
import com.td.vp.qa.model.Connector;
import com.td.vp.qa.model.Datalab;
import com.td.vp.qa.model.DatalabGroup;
import com.td.vp.qa.model.Fabric;
import com.td.vp.qa.pages.DatalabsPage;
import com.td.vp.qa.pages.QueryGridPage;
import com.td.vp.qa.pages.SqlScratchpadPage;

public class TestAddServer extends AbstractTestCaseBase {
    PageObjectManager pageObjectManager;
    ViewpointPage viewpointPage;

    @Before
    public void setUp() throws Exception {
        super.setUp();
        pageObjectManager = new PageObjectManager(d);
        viewpointPage = new LoginPage(d, u).signIn();
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void test() {
        Server s1 = new Server();
        s1.setName("abc");
        s1.setServerType(ServerType.HDP);
        s1.setDataCenter("UNKNOWN");
        ManageServersPage p5 = new LoginPage(d, u).signIn().clickPortalAdminButton().clickEcosystemConfiguration()
                .clickServers();
        p5.add(s1);
    }

    @Test
    public void test1() {
        ViewpointPage p1 = new LoginPage(d, u).signIn();
        p1.logOut();
    }

    @Test
    public void testnew() {
        viewpointPage.addPortletContainerPage("abcde");
        viewpointPage.deletePortletContainerPage("abcde");
        // p1.selectPage("svsharedpage");
    }

    @Test
    public void testAddPortlets() {
        String portletsPageName = "my-portlets-page";
        viewpointPage.addPortletContainerPage(portletsPageName);
        viewpointPage.addPortlet(Portlets.DATALABS);
        viewpointPage.addPortlet(Portlets.SQLSCRATCHPAD);
        viewpointPage.addPortlet(Portlets.QUERYGRID);
        viewpointPage.deletePortletContainerPage(portletsPageName);
    }

    @Test
    public void deleteDatalabsPage() {
        // viewpointPage.selectPortletContainerPage("my-datalabs");
        viewpointPage.deletePortletContainerPage("my-datalabs");
    }

    String tdName = "1620TDLa";

    @Test
    public void testCreateDatalabGroup() {
        viewpointPage.addPortletContainerPage("my-datalabs");
        DatalabsPage p2 = (DatalabsPage) viewpointPage.addPortlet(Portlets.DATALABS);

        Credential credential = new Credential("dbc", "dbc");
        MonitoredSystem system = new MonitoredSystem(tdName);
        system.setCredential(credential);
        DatalabGroup datalabGroup = new DatalabGroup("my-lab-group");
        datalabGroup.setSystem(system);
        datalabGroup.setParentDatabase("DBC");
        datalabGroup.setLabGroupSize(12);
        datalabGroup.setDefaultLabSize(1);

        p2.add(datalabGroup);
        // p1.deletePortletContainerPage("my-datalabs");

    }

    @Test
    public void testIsExistDatalabGroup() {

        viewpointPage.selectPortletContainerPage("my-datalabs");

        Credential credential = new Credential("dbc", "dbc");
        MonitoredSystem system = new MonitoredSystem("16.20TDLatest");
        system.setCredential(credential);
        DatalabGroup datalabGroup = new DatalabGroup("my-lab-group");
        datalabGroup.setSystem(system);
        datalabGroup.setParentDatabase("DBC");
        datalabGroup.setLabGroupSize(12);
        datalabGroup.setDefaultLabSize(1);

        DatalabsPage p2 = pageObjectManager.getDatalabsPage();
        assert (p2.isExist(datalabGroup) == true);

    }

    @Test
    public void testDeleteDatalabGroup() {
        viewpointPage.selectPortletContainerPage("my-datalabs");

        Credential credential = new Credential("dbc", "dbc");
        MonitoredSystem system = new MonitoredSystem(tdName);
        system.setCredential(credential);
        DatalabGroup datalabGroup = new DatalabGroup("my-lab-group");
        datalabGroup.setSystem(system);
        datalabGroup.setParentDatabase("DBC");
        datalabGroup.setLabGroupSize(12);
        datalabGroup.setDefaultLabSize(1);

        DatalabsPage p2 = pageObjectManager.getDatalabsPage();
        p2.delete(datalabGroup);

        viewpointPage.deletePortletContainerPage("my-datalabs");
    }

    String datalabName = "swaggerlab1";

    @Test
    public void testAddDatalab() {
        viewpointPage.selectPortletContainerPage("my-datalabs");
        Credential credential = new Credential("dbc", "dbc");
        MonitoredSystem system = new MonitoredSystem(tdName);
        system.setCredential(credential);
        DatalabGroup datalabGroup = new DatalabGroup("my-lab-group");
        datalabGroup.setSystem(system);

        Datalab datalab = new Datalab(datalabName);
        datalab.setDatalabGroup(datalabGroup);
        datalab.setLabSize(1);
        datalab.setDescription("description of swaggerlab1");
        DatalabsPage p2 = pageObjectManager.getDatalabsPage();
        p2.add(datalab);
    }

    @Test
    public void testAddSQLScratchPad() {
        viewpointPage.addPortletContainerPage("my-sqlscratchpad");
        viewpointPage.addPortlet(Portlets.SQLSCRATCHPAD);
        SqlScratchpadPage p3 = pageObjectManager.getsqlScratchpadPage();
        Credential credential = new Credential("dbc", "dbc");
        MonitoredSystem system = new MonitoredSystem(tdName);
        system.setCredential(credential);
        p3.Connect(system);
        p3.runSqlInScratchpad(String.format("CREATE PROCEDURE %s.PROC() BEGIN END;", datalabName));
        // p2.runSqlInScratchpad(datalab, String.format("CREATE PROCEDURE %s.PROC()
        // BEGIN END;", datalabName));
    }

    @Test
    public void testIsExistDatalab() {
        viewpointPage.selectPortletContainerPage("my-datalabs");

        Credential credential = new Credential("dbc", "dbc");
        MonitoredSystem system = new MonitoredSystem(tdName);
        system.setCredential(credential);
        DatalabGroup datalabGroup = new DatalabGroup("my-lab-group");
        datalabGroup.setSystem(system);

        Datalab datalab = new Datalab("swaggerlab1");
        datalab.setDatalabGroup(datalabGroup);
        DatalabsPage p2 = pageObjectManager.getDatalabsPage();
        assert (p2.isExist(datalab) == true);
    }

    @Test
    public void testDeleteDatalab() {
        viewpointPage.selectPortletContainerPage("my-datalabs");

        Credential credential = new Credential("dbc", "dbc");
        MonitoredSystem system = new MonitoredSystem(tdName);
        system.setCredential(credential);
        DatalabGroup datalabGroup = new DatalabGroup("my-lab-group");
        datalabGroup.setSystem(system);

        Datalab datalab = new Datalab(datalabName);
        datalab.setDatalabGroup(datalabGroup);
        DatalabsPage p2 = pageObjectManager.getDatalabsPage();
        p2.delete(datalab);
    }

    @Test
    public void testCreateFabric() {
        viewpointPage.selectPortletContainerPage("my-querygrid");
        QueryGridPage p2 = pageObjectManager.getQueryGridPage();
        Fabric fabric = new Fabric();
        fabric.setName("swagger-fabric1");
        fabric.setPort(1111);
        p2.add(fabric);
    }

    @Test
    public void testDeleteFabric() {
        viewpointPage.selectPortletContainerPage("my-querygrid");
        QueryGridPage p2 = pageObjectManager.getQueryGridPage();
        Fabric fabric = new Fabric("swagger-fabric1");
        fabric.setPort(1111);
        p2.delete(fabric);
    }

    @Test
    public void testCreateConnector() {
        viewpointPage.selectPortletContainerPage("my-querygrid");
        QueryGridPage p2 = pageObjectManager.getQueryGridPage();
        Fabric fabric = new Fabric("swagger-fabric1");
        fabric.setPort(1111);
        Connector connector = new Connector("swagger-connector1");
        connector.setFabric(fabric);
        p2.add(connector);
    }
}
