package com.td.em.qa;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.td.em.qa.constants.DaemonState;
import com.td.em.qa.constants.ExecutableName;
import com.td.em.qa.constants.HaPattern;
import com.td.em.qa.constants.HaTransition;
import com.td.em.qa.constants.IdentifyDaemonsBy;
import com.td.em.qa.constants.JiraUpdate;
import com.td.em.qa.constants.Severity;
import com.td.em.qa.domain.Action;
import com.td.em.qa.domain.Daemon;
import com.td.em.qa.domain.DaemonGroup;
import com.td.em.qa.domain.Rule;
import com.td.em.qa.pages.ActionRulesPage;
import com.td.em.qa.pages.JSchSSHConnectionRunDaemon;
import com.td.em.qa.pages.LoginPage;
import com.td.em.qa.pages.ManageDaemonGroupsPage;

public class DaemonTC1 extends AbstractTestCaseBase{
	
	/* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
        //ManageDaemonGroupsPage p5 = navigateToDaemons();
    }

    /* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#tearDown()
     */
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }
    //Start Daemons
    @Test
    public void test() {
        
        JSchSSHConnectionRunDaemon rdmn = new JSchSSHConnectionRunDaemon();
        rdmn.RunDaemons("xorn.td.teradata.com","root","emdev2015");
        rdmn.RunDaemons("sdld0867.td.teradata.com","root","TCAMPass123");
     
    }

    
    //ESM-44187
    @Test
    public void test_add_daemon_javafullclassname() throws InterruptedException {
    	
    	String jirano = "ESM-44187";
        Daemon dm = new Daemon();
        dm.setName("AA-JAVA-FULLCLASSNAME");
        dm.setServer("angel");
        dm.setCurrentState(DaemonState.Active);

        List<Daemon> dmlist = new ArrayList<Daemon>();
        dmlist.add(dm);

        DaemonGroup dg = new DaemonGroup();
        dg.setName("DG-AA-JAVA-FULLCLASSNAME");
        dg.setExecuteUser("root");
        dg.setExecutePassword("emdev2015");
        dg.setGlobalSearchPatternValue("com.td.em.qa.sdaemon.Main");
        dg.setExecutableName(ExecutableName.JAVA);
        dg.setHaPattern(HaPattern.AA);
        dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
        dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.JAVACLASS);
        dg.setDaemons(dmlist);
        
        JiraUpdate jrup = new JiraUpdate();
        
        try{
        ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                .clickPortalAdminButton().clickEcosystemConfiguration()
                .clickDaemons();
        ManageDaemonGroupsPage p6=null;        
        if (p5.isExist(dg)) {
            p6 = p5.remove(dg);
        }else{
            p6=p5;
        }        
        ManageDaemonGroupsPage p8 = p6.add(dg);
//        p5.gotoPageNumber("2")
//        .gotoPreviousPage()
//        .gotoLastPage()
//        .gotoFirstPage()
//        .gotoNextPage();
        String result = "pass";
        jrup.jirastatus(jirano,result);
        System.out.println("Test Passed "+ jirano );
        p8.close();
        
        }
        catch(Exception e){
            e.printStackTrace();
            String result = "fail";
            jrup.jirastatus(jirano,result);
            System.out.println("Test Failed "+ jirano );
         }

    }
    
   //ESM-44188 
   @Test
   public void test_add_daemon_CMDLineArg() throws InterruptedException {
       Daemon dm = new Daemon();
       dm.setName("AA-SH-CMDARGS");
       dm.setServer("angel");
       dm.setCurrentState(DaemonState.Active);

       List<Daemon> dmlist = new ArrayList<Daemon>();
       dmlist.add(dm);
       
       DaemonGroup dg = new DaemonGroup();
       dg.setName("DG-AA-SH-CMDARGS");
       dg.setExecuteUser("root");
       dg.setExecutePassword("emdev2015");
       dg.setGlobalSearchPatternValue("dummydaemon.sh");
       dg.setExecutableName(ExecutableName.SH);
       dg.setHaPattern(HaPattern.AA);
       dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
       dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
       dg.setDaemons(dmlist);

       ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
               .clickPortalAdminButton().clickEcosystemConfiguration()
               .clickDaemons();
       ManageDaemonGroupsPage p6=null;        
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }else{
           p6=p5;
       }        
       ManageDaemonGroupsPage p8 = p6.add(dg);
//       p5.gotoPageNumber("2")
//       .gotoPreviousPage()
//       .gotoLastPage()
//       .gotoFirstPage()
//       .gotoNextPage();
       p8.close();
   }
   
   //ESM-44189
   @Test
   public void test_add_daemon_FullPathToProcExec() throws InterruptedException {
       Daemon dm = new Daemon();
       dm.setName("AA-SH-FULLPATH");
       dm.setServer("angel");
       dm.setCurrentState(DaemonState.Active);

       List<Daemon> dmlist = new ArrayList<Daemon>();
       dmlist.add(dm);
      
       DaemonGroup dg = new DaemonGroup();
       dg.setName("DG-AA-SH-FULLPATH");
       dg.setExecuteUser("root");
       dg.setExecutePassword("emdev2015");
       dg.setGlobalSearchPatternValue("/var/lib/dummydaemon.sh");
       dg.setExecutableName(ExecutableName.SH);
       dg.setHaPattern(HaPattern.AA);
       dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
       dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
       dg.setDaemons(dmlist);

       ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
               .clickPortalAdminButton().clickEcosystemConfiguration()
               .clickDaemons();
       ManageDaemonGroupsPage p6=null;        
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }else{
           p6=p5;
       }        
       ManageDaemonGroupsPage p8 = p6.add(dg);
//       p5.gotoPageNumber("2")
//       .gotoPreviousPage()
//       .gotoLastPage()
//       .gotoFirstPage()
//       .gotoNextPage();
       p8.close();
   }
   
   //ESM-44191
   @Test
   public void test_add_daemon_JAVA_CMDARGS_VALUE() throws InterruptedException {
             
           Daemon dm = new Daemon();
           dm.setName("AA-JAVA-CMDARGS-VALUE");
           dm.setServer("angel");
           dm.setCurrentState(DaemonState.Active);

           List<Daemon> dmlist = new ArrayList<Daemon>();
           dmlist.add(dm);
          
           DaemonGroup dg = new DaemonGroup();
           dg.setName("DG-AA-JAVA-CMDARGS-VALUE");
           dg.setExecuteUser("root");
           dg.setExecutePassword("emdev2015");
           dg.setGlobalSearchPatternValue("/opt/teradata/jvm64/jdk7/bin/java -classpath /opt/teradata/em-qa-sdaemon/lib/em-qa-sdaemon-0.0.1-SNAPSHOT.jar com.td.em.qa.sdaemon.Main");
           dg.setExecutableName(ExecutableName.JAVA);
           dg.setHaPattern(HaPattern.AA);
           dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
           dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
           dg.setDaemons(dmlist);

           ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                   .clickPortalAdminButton().clickEcosystemConfiguration()
                   .clickDaemons();
           ManageDaemonGroupsPage p6=null;        
           if (p5.isExist(dg)) {
               p6 = p5.remove(dg);
           }else{
               p6=p5;
           }        
           ManageDaemonGroupsPage p8 = p6.add(dg);
//           p5.gotoPageNumber("2")
//           .gotoPreviousPage()
//           .gotoLastPage()
//           .gotoFirstPage()
//           .gotoNextPage();
           p8.close();
      
   }
   
 //ESM-44192
   @Test
   public void test_add_daemon_CMDARGS_VALUE() throws InterruptedException {
             
           Daemon dm = new Daemon();
           dm.setName("AA-CMDARGS-VALUE");
           dm.setServer("angel");
           dm.setCurrentState(DaemonState.Active);

           List<Daemon> dmlist = new ArrayList<Daemon>();
           dmlist.add(dm);
          
           DaemonGroup dg = new DaemonGroup();
           dg.setName("DG-AA-CMDARGS-VALUE");
           dg.setExecuteUser("root");
           dg.setExecutePassword("emdev2015");
           dg.setGlobalSearchPatternValue("/bin/sh /opt/teradata/em-qa-sdaemon/test.sh Hello");
           dg.setExecutableName(ExecutableName.SH);
           dg.setHaPattern(HaPattern.AA);
           dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
           dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
           dg.setDaemons(dmlist);

           ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                   .clickPortalAdminButton().clickEcosystemConfiguration()
                   .clickDaemons();
           ManageDaemonGroupsPage p6=null;        
           if (p5.isExist(dg)) {
               p6 = p5.remove(dg);
           }else{
               p6=p5;
           }        
           ManageDaemonGroupsPage p8 = p6.add(dg);
//           p5.gotoPageNumber("2")
//           .gotoPreviousPage()
//           .gotoLastPage()
//           .gotoFirstPage()
//           .gotoNextPage();
           p8.close();
      
   }
   
 //ESM-44193
   @Test
   public void test_add_daemon_FullPathToProcExec_Value() throws InterruptedException {
       Daemon dm = new Daemon();
       dm.setName("AA-SH-FULLPATH_VALUE");
       dm.setServer("angel");
       dm.setCurrentState(DaemonState.Active);

       List<Daemon> dmlist = new ArrayList<Daemon>();
       dmlist.add(dm);
      
       DaemonGroup dg = new DaemonGroup();
       dg.setName("DG-AA-SH-FULLPATH_VALUE");
       dg.setExecuteUser("root");
       dg.setExecutePassword("emdev2015");
       dg.setGlobalSearchPatternValue("/opt/teradata/em-qa-sdaemon/test.sh Hello");
       dg.setExecutableName(ExecutableName.SH);
       dg.setHaPattern(HaPattern.AA);
       dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
       dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
       dg.setDaemons(dmlist);

       ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
               .clickPortalAdminButton().clickEcosystemConfiguration()
               .clickDaemons();
       ManageDaemonGroupsPage p6=null;        
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }else{
           p6=p5;
       }        
       ManageDaemonGroupsPage p8 = p6.add(dg);
//       p5.gotoPageNumber("2")
//       .gotoPreviousPage()
//       .gotoLastPage()
//       .gotoFirstPage()
//       .gotoNextPage();
       p8.close();
   }
   
   //ESM-44187
   @Test
   public void test_add_daemon_javafullclassname_Value() throws InterruptedException {
       Daemon dm = new Daemon();
       dm.setName("AA-JAVA-FULLCLASSNAME_VALUE");
       dm.setServer("angel");
       dm.setCurrentState(DaemonState.Active);

       List<Daemon> dmlist = new ArrayList<Daemon>();
       dmlist.add(dm);

       DaemonGroup dg = new DaemonGroup();
       dg.setName("DG-AA-JAVA-FULLCLASSNAME_VALUE");
       dg.setExecuteUser("root");
       dg.setExecutePassword("emdev2015");
       dg.setGlobalSearchPatternValue("/opt/teradata/jvm64/jdk7/bin/java -classpath /opt/teradata/em-qa-sdaemon Add 10 20 30");
       dg.setExecutableName(ExecutableName.JAVA);
       dg.setHaPattern(HaPattern.AA);
       dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
       dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.JAVACLASS);
       dg.setDaemons(dmlist);
       
       ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
               .clickPortalAdminButton().clickEcosystemConfiguration()
               .clickDaemons();
       ManageDaemonGroupsPage p6=null;        
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }else{
           p6=p5;
       }        
       ManageDaemonGroupsPage p8 = p6.add(dg);
//       p5.gotoPageNumber("2")
//       .gotoPreviousPage()
//       .gotoLastPage()
//       .gotoFirstPage()
//       .gotoNextPage();
       p8.close();

   }
   
 //ESM-44199
   @Test
   public void test_add_daemon_as_javafullclassname() throws InterruptedException {
	   
	   List<HaTransition> h = new ArrayList<HaTransition>();
	   h.add(HaTransition.OOS2P);
	   h.add(HaTransition.OOS2S);
	   h.add(HaTransition.P2S);
	   h.add(HaTransition.S2P);
	   h.add(HaTransition.A2P);
	      
       Daemon dm = new Daemon();
       dm.setName("AS-JAVA-FULLCLASSNAME");
       dm.setServer("angel");
       dm.setCurrentState(DaemonState.Active);
       
       Daemon dm1 = new Daemon();
       dm1.setName("AS-JAVA-FULLCLASSNAME");
       dm1.setServer("xorn");
       dm1.setCurrentState(DaemonState.Standby);

       List<Daemon> dmlist = new ArrayList<Daemon>();
       dmlist.add(dm);
       dmlist.add(dm1);

       DaemonGroup dg = new DaemonGroup();
       dg.setName("DG-AS-JAVA-FULLCLASSNAME");
       dg.setExecuteUser("root");
       dg.setExecutePassword("emdev2015");
       dg.setGlobalSearchPatternValue("com.td.em.qa.sdaemon.Main");
       dg.setExecutableName(ExecutableName.JAVA);
       dg.setHaPattern(HaPattern.AS);
       dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
       dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.JAVACLASS);
       dg.setDaemons(dmlist);
       dg.setValidHaTransitions(h);
       
       ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
               .clickPortalAdminButton().clickEcosystemConfiguration()
               .clickDaemons();
       ManageDaemonGroupsPage p6=null;        
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }else{
           p6=p5;
       }        
       ManageDaemonGroupsPage p8 = p6.add(dg);
//       p5.gotoPageNumber("2")
//       .gotoPreviousPage()
//       .gotoLastPage()
//       .gotoFirstPage()
//       .gotoNextPage();
       p8.close();

   }
   
  //ESM-44200
  @Test
  public void test_add_daemon_as_CMDLineArg() throws InterruptedException {
      
	  List<HaTransition> h = new ArrayList<HaTransition>();
      h.add(HaTransition.OOS2P);
      h.add(HaTransition.OOS2S);
      h.add(HaTransition.P2S);
      h.add(HaTransition.S2P);
      h.add(HaTransition.A2P);
	  
	  Daemon dm = new Daemon();
      dm.setName("AS-SH-CMDARGS");
      dm.setServer("angel");
      dm.setCurrentState(DaemonState.Active);
      
      Daemon dm1 = new Daemon();
      dm1.setServer("xorn");
      dm1.setName("AS-SH-CMDARGS");
      dm1.setCurrentState(DaemonState.Standby);

      List<Daemon> dmlist = new ArrayList<Daemon>();
      dmlist.add(dm);
      dmlist.add(dm1);
      
      DaemonGroup dg = new DaemonGroup();
      dg.setName("DG-AS-SH-CMDARGS");
      dg.setExecuteUser("root");
      dg.setExecutePassword("emdev2015");
      dg.setGlobalSearchPatternValue("dummydaemon.sh");
      dg.setExecutableName(ExecutableName.SH);
      dg.setHaPattern(HaPattern.AS);
      dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
      dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
      dg.setDaemons(dmlist);
      dg.setValidHaTransitions(h);

      ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
              .clickPortalAdminButton().clickEcosystemConfiguration()
              .clickDaemons();
      ManageDaemonGroupsPage p6=null;        
      if (p5.isExist(dg)) {
          p6 = p5.remove(dg);
      }else{
          p6=p5;
      }        
      ManageDaemonGroupsPage p8 = p6.add(dg);
//      p5.gotoPageNumber("2")
//      .gotoPreviousPage()
//      .gotoLastPage()
//      .gotoFirstPage()
//      .gotoNextPage();
      p8.close();
  }
  
  //ESM-44201
  @Test
  public void test_add_daemon_as_FullPathToProcExec() throws InterruptedException {
	  
	  List<HaTransition> h = new ArrayList<HaTransition>();
      h.add(HaTransition.OOS2P);
      h.add(HaTransition.OOS2S);
      h.add(HaTransition.P2S);
      h.add(HaTransition.S2P);
      h.add(HaTransition.A2P);
      
      Daemon dm = new Daemon();
      dm.setName("AS-SH-FULLPATH");
      dm.setServer("angel");
      dm.setCurrentState(DaemonState.Standby);
      
      Daemon dm1 = new Daemon();
      dm1.setServer("xorn");
      dm1.setName("AS-SH-FULLPATH");
      dm1.setCurrentState(DaemonState.Active);

      List<Daemon> dmlist = new ArrayList<Daemon>();
      dmlist.add(dm);
      dmlist.add(dm1);
     
      DaemonGroup dg = new DaemonGroup();
      dg.setName("DG-AS-SH-FULLPATH");
      dg.setExecuteUser("root");
      dg.setExecutePassword("emdev2015");
      dg.setGlobalSearchPatternValue("/var/lib/dummydaemon.sh");
      dg.setExecutableName(ExecutableName.SH);
      dg.setHaPattern(HaPattern.AS);
      dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
      dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
      dg.setDaemons(dmlist);
      dg.setValidHaTransitions(h);

      ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
              .clickPortalAdminButton().clickEcosystemConfiguration()
              .clickDaemons();
      ManageDaemonGroupsPage p6=null;        
      if (p5.isExist(dg)) {
          p6 = p5.remove(dg);
      }else{
          p6=p5;
      }        
      ManageDaemonGroupsPage p8 = p6.add(dg);
//      p5.gotoPageNumber("2")
//      .gotoPreviousPage()
//      .gotoLastPage()
//      .gotoFirstPage()
//      .gotoNextPage();
      p8.close();
  }
  //ESM-44202
  @Test
  public void test_add_daemon_as_JAVA_CMDARGS() throws InterruptedException {
          
	  	  List<HaTransition> h = new ArrayList<HaTransition>();
	  	  h.add(HaTransition.OOS2P);
	  	  h.add(HaTransition.OOS2S);
	  	  h.add(HaTransition.P2S);
	  	  h.add(HaTransition.S2P);
	  	  h.add(HaTransition.A2P);
	  
          Daemon dm = new Daemon();
          dm.setName("AS-JAVA-CMDARGS");
          dm.setServer("angel");
          dm.setCurrentState(DaemonState.Active);
          
          Daemon dm1 = new Daemon();
          dm1.setServer("xorn");
          dm1.setName("AS-JAVA-CMDARGS");
          dm1.setCurrentState(DaemonState.Standby);

          List<Daemon> dmlist = new ArrayList<Daemon>();
          dmlist.add(dm);
          dmlist.add(dm1);
         
          DaemonGroup dg = new DaemonGroup();
          dg.setName("DG-AS-JAVA-CMDARGS");
          dg.setExecuteUser("root");
          dg.setExecutePassword("emdev2015");
          dg.setGlobalSearchPatternValue("/opt/teradata/jvm64/jdk7/bin/java -classpath /opt/teradata/em-qa-sdaemon/lib/em-qa-sdaemon-0.0.1-SNAPSHOT.jar com.td.em.qa.sdaemon.Main");
          dg.setExecutableName(ExecutableName.JAVA);
          dg.setHaPattern(HaPattern.AS);
          dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
          dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
          dg.setDaemons(dmlist);
          dg.setValidHaTransitions(h);

          ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                  .clickPortalAdminButton().clickEcosystemConfiguration()
                  .clickDaemons();
          ManageDaemonGroupsPage p6=null;        
          if (p5.isExist(dg)) {
              p6 = p5.remove(dg);
          }else{
              p6=p5;
          }        
          ManageDaemonGroupsPage p8 = p6.add(dg);
//          p5.gotoPageNumber("2")
//          .gotoPreviousPage()
//          .gotoLastPage()
//          .gotoFirstPage()
//          .gotoNextPage();
          p8.close();
     
  }
   //ESM-44226
   @Test
   public void test_add_daemon_CMDARGS_WILDCARDBOTH() throws InterruptedException{
   
           Daemon dm = new Daemon();
           dm.setName("AA-SH-CMDARGS-WILDCARDBOTH");
           dm.setServer("angel");
           dm.setCurrentState(DaemonState.Active);

           List<Daemon> dmlist = new ArrayList<Daemon>();
           dmlist.add(dm);
          
           DaemonGroup dg = new DaemonGroup();
           dg.setName("DG-AA-SH-CMDARGS-WILDCARDBOTH");
           dg.setExecuteUser("root");
           dg.setExecutePassword("emdev2015");
           dg.setGlobalSearchPatternValue("*dummydaemon*.sh");
           dg.setExecutableName(ExecutableName.SH);
           dg.setHaPattern(HaPattern.AA);
           dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
           dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
           dg.setDaemons(dmlist);

           ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                   .clickPortalAdminButton().clickEcosystemConfiguration()
                   .clickDaemons();
           ManageDaemonGroupsPage p6=null;        
           if (p5.isExist(dg)) {
               p6 = p5.remove(dg);
           }else{
               p6=p5;
           }        
           ManageDaemonGroupsPage p8 = p6.add(dg);
//           p5.gotoPageNumber("2")
//           .gotoPreviousPage()
//           .gotoLastPage()
//           .gotoFirstPage()
//           .gotoNextPage();
           p8.close();
       
   }

   //ESM-44224
   @Test
   public void test_add_daemon_CMDARGS_WILDCARDPREFIX()throws InterruptedException{
          
	   		Daemon dm = new Daemon();
	   		dm.setName("AA-SH-CMDARGS-WILDCARDPREFIX");
	   		dm.setServer("angel");
	   		dm.setCurrentState(DaemonState.Active);

	   		List<Daemon> dmlist = new ArrayList<Daemon>();
	   		dmlist.add(dm);
       
           DaemonGroup dg = new DaemonGroup();
           dg.setName("DG-AA-SH-CMDARGS-WILDCARDPREFIX");
           dg.setExecuteUser("root");
           dg.setExecutePassword("emdev2015");
           dg.setGlobalSearchPatternValue("*dummydaemon.sh");
           dg.setExecutableName(ExecutableName.SH);
           dg.setHaPattern(HaPattern.AA);
           dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
           dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
           dg.setDaemons(dmlist);

           ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                   .clickPortalAdminButton().clickEcosystemConfiguration()
                   .clickDaemons();
           ManageDaemonGroupsPage p6=null;        
           if (p5.isExist(dg)) {
               p6 = p5.remove(dg);
           }else{
               p6=p5;
           }        
           ManageDaemonGroupsPage p8 = p6.add(dg);
//           p5.gotoPageNumber("2")
//           .gotoPreviousPage()
//           .gotoLastPage()
//           .gotoFirstPage()
//           .gotoNextPage();
           p8.close();
      
   }
   
   @Test
   public void test_add_solsparc_daemon_CMDLineArg() throws InterruptedException {
       Daemon dm = new Daemon();
       dm.setName("SOLSPARC-AA-SH-CMDARGS");
       dm.setServer("sdsl0006");
       dm.setCurrentState(DaemonState.Active);

       List<Daemon> dmlist = new ArrayList<Daemon>();
       dmlist.add(dm);
       
       DaemonGroup dg = new DaemonGroup();
       dg.setName("SOLSPARC-DG-AA-SH-CMDARGS");
       dg.setExecuteUser("root");
       dg.setExecutePassword("emdev2015");
       dg.setGlobalSearchPatternValue("dummydaemon.sh");
       dg.setExecutableName(ExecutableName.SH);
       dg.setHaPattern(HaPattern.AA);
       dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
       dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
       dg.setDaemons(dmlist);

       ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
               .clickPortalAdminButton().clickEcosystemConfiguration()
               .clickDaemons();
       ManageDaemonGroupsPage p6=null;        
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }else{
           p6=p5;
       }        
       ManageDaemonGroupsPage p8 = p6.add(dg);
//       p5.gotoPageNumber("2")
//       .gotoPreviousPage()
//       .gotoLastPage()
//       .gotoFirstPage()
//       .gotoNextPage();
       p8.close();
   }
   
   @Test
   public void test_add_solopteron_daemon_CMDLineArg() throws InterruptedException {
       Daemon dm = new Daemon();
       dm.setName("SOLOPTERON-AA-SH-CMDARGS");
       dm.setServer("sdsl1017");
       dm.setCurrentState(DaemonState.Active);

       List<Daemon> dmlist = new ArrayList<Daemon>();
       dmlist.add(dm);
       
       DaemonGroup dg = new DaemonGroup();
       dg.setName("SOLOPTERON-DG-AA-SH-CMDARGS");
       dg.setExecuteUser("root");
       dg.setExecutePassword("emdev2015");
       dg.setGlobalSearchPatternValue("dummydaemon.sh");
       dg.setExecutableName(ExecutableName.SH);
       dg.setHaPattern(HaPattern.AA);
       dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
       dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
       dg.setDaemons(dmlist);

       ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
               .clickPortalAdminButton().clickEcosystemConfiguration()
               .clickDaemons();
       ManageDaemonGroupsPage p6=null;        
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }else{
           p6=p5;
       }        
       ManageDaemonGroupsPage p8 = p6.add(dg);
//       p5.gotoPageNumber("2")
//       .gotoPreviousPage()
//       .gotoLastPage()
//       .gotoFirstPage()
//       .gotoNextPage();
       p8.close();
   }
   
   @Test
   public void test_add_hpux_daemon_CMDLineArg() throws InterruptedException {
       Daemon dm = new Daemon();
       dm.setName("HPUX-AA-SH-CMDARGS");
       dm.setServer("sdhl0014");
       dm.setCurrentState(DaemonState.Active);

       List<Daemon> dmlist = new ArrayList<Daemon>();
       dmlist.add(dm);
       
       DaemonGroup dg = new DaemonGroup();
       dg.setName("HPUX-DG-AA-SH-CMDARGS");
       dg.setExecuteUser("root");
       dg.setExecutePassword("emdev2015");
       dg.setGlobalSearchPatternValue("dummydaemon.sh");
       dg.setExecutableName(ExecutableName.SH);
       dg.setHaPattern(HaPattern.AA);
       dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
       dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
       dg.setDaemons(dmlist);

       ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
               .clickPortalAdminButton().clickEcosystemConfiguration()
               .clickDaemons();
       ManageDaemonGroupsPage p6=null;        
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }else{
           p6=p5;
       }        
       ManageDaemonGroupsPage p8 = p6.add(dg);
//       p5.gotoPageNumber("2")
//       .gotoPreviousPage()
//       .gotoLastPage()
//       .gotoFirstPage()
//       .gotoNextPage();
       p8.close();
   }
   
   @Test
   public void test_add_aix_daemon_CMDLineArg() throws InterruptedException {
       Daemon dm = new Daemon();
       dm.setName("AIX-AA-SH-CMDARGS");
       dm.setServer("aix71ralt");
       dm.setCurrentState(DaemonState.Active);

       List<Daemon> dmlist = new ArrayList<Daemon>();
       dmlist.add(dm);
       
       DaemonGroup dg = new DaemonGroup();
       dg.setName("AIX-DG-AA-SH-CMDARGS");
       dg.setExecuteUser("root");
       dg.setExecutePassword("emdev2015");
       dg.setGlobalSearchPatternValue("dummydaemon.sh");
       dg.setExecutableName(ExecutableName.SH);
       dg.setHaPattern(HaPattern.AA);
       dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
       dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
       dg.setDaemons(dmlist);

       ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
               .clickPortalAdminButton().clickEcosystemConfiguration()
               .clickDaemons();
       ManageDaemonGroupsPage p6=null;        
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }else{
           p6=p5;
       }        
       ManageDaemonGroupsPage p8 = p6.add(dg);
//       p5.gotoPageNumber("2")
//       .gotoPreviousPage()
//       .gotoLastPage()
//       .gotoFirstPage()
//       .gotoNextPage();
       p8.close();
   }

   @Test
   public void test_add_daemon_BASH_CMDARGS() throws InterruptedException{
                   
           Daemon dm = new Daemon();
           dm.setName("DG-AA-BASH-CMDARGS");
           dm.setServer("angel");
           dm.setCurrentState(DaemonState.Active);

           List<Daemon> dmlist = new ArrayList<Daemon>();
           dmlist.add(dm);
          
           DaemonGroup dg = new DaemonGroup();
           dg.setName("DG-AA-BASH-CMDARGS");
           dg.setExecuteUser("root");
           dg.setExecutePassword("emdev2015");
           dg.setGlobalSearchPatternValue("dummydaemon.sh");
           dg.setExecutableName(ExecutableName.BASH);
           dg.setHaPattern(HaPattern.AA);
           dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
           dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
           dg.setDaemons(dmlist);

           ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                   .clickPortalAdminButton().clickEcosystemConfiguration()
                   .clickDaemons();
           ManageDaemonGroupsPage p6=null;        
           if (p5.isExist(dg)) {
               p6 = p5.remove(dg);
           }else{
               p6=p5;
           }        
           ManageDaemonGroupsPage p8 = p6.add(dg);
//           p5.gotoPageNumber("2")
//           .gotoPreviousPage()
//           .gotoLastPage()
//           .gotoFirstPage()
//           .gotoNextPage();
           p8.close();
   }

   @Test
   public void test_add_daemon_BASH_CMDARGS_WILDCARDBOTH() throws InterruptedException{
                       
           Daemon dm = new Daemon();
           dm.setName("AA-BASH-CMDARGS-WILDCARDBOTH");
           dm.setServer("angel");
           dm.setCurrentState(DaemonState.Active);

           List<Daemon> dmlist = new ArrayList<Daemon>();
           dmlist.add(dm);
          
           DaemonGroup dg = new DaemonGroup();
           dg.setName("DG-AA-BASH-CMDARGS-WILDCARDBOTH");
           dg.setExecuteUser("root");
           dg.setExecutePassword("emdev2015");
           dg.setGlobalSearchPatternValue("dummydaemon*.sh");
           dg.setExecutableName(ExecutableName.BASH);
           dg.setHaPattern(HaPattern.AA);
           dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
           dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
           dg.setDaemons(dmlist);

           ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                   .clickPortalAdminButton().clickEcosystemConfiguration()
                   .clickDaemons();
           ManageDaemonGroupsPage p6=null;        
           if (p5.isExist(dg)) {
               p6 = p5.remove(dg);
           }else{
               p6=p5;
           }        
           ManageDaemonGroupsPage p8 = p6.add(dg);
//           p5.gotoPageNumber("2")
//           .gotoPreviousPage()
//           .gotoLastPage()
//           .gotoFirstPage()
//           .gotoNextPage();
           p8.close();
    
   }

   @Test
   public void test_add_daemon_BASH_CMDARGS_WILDCARDPREFIX() throws InterruptedException{
              
           Daemon dm = new Daemon();
           dm.setName("AA-BASH-CMDARGS-WILDCARDPREFIX");
           dm.setServer("angel");
           dm.setCurrentState(DaemonState.Active);

           List<Daemon> dmlist = new ArrayList<Daemon>();
           dmlist.add(dm);
          
           DaemonGroup dg = new DaemonGroup();
           dg.setName("DG-AA-BASH-CMDARGS-WILDCARDPREFIX");
           dg.setExecuteUser("root");
           dg.setExecutePassword("emdev2015");
           dg.setGlobalSearchPatternValue("*dummydaemon.sh");
           dg.setExecutableName(ExecutableName.BASH);
           dg.setHaPattern(HaPattern.AA);
           dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
           dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
           dg.setDaemons(dmlist);

           ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                   .clickPortalAdminButton().clickEcosystemConfiguration()
                   .clickDaemons();
           ManageDaemonGroupsPage p6=null;        
           if (p5.isExist(dg)) {
               p6 = p5.remove(dg);
           }else{
               p6=p5;
           }        
           ManageDaemonGroupsPage p8 = p6.add(dg);
//           p5.gotoPageNumber("2")
//           .gotoPreviousPage()
//           .gotoLastPage()
//           .gotoFirstPage()
//           .gotoNextPage();
           p8.close();
     
   }
   
   //ESM-44225
   @Test
   public void test_add_daemon_CMDARGS_WILDCARDSUFFIX() throws InterruptedException{
           
           Daemon dm = new Daemon();
           dm.setName("AA-BASH-CMDARGS-WILDCARDSUFFIX");
           dm.setServer("angel");
           dm.setCurrentState(DaemonState.Active);

           List<Daemon> dmlist = new ArrayList<Daemon>();
           dmlist.add(dm);
          
           DaemonGroup dg = new DaemonGroup();
           dg.setName("DG-AA-BASH-CMDARGS-WILDCARDSUFFIX");
           dg.setExecuteUser("root");
           dg.setExecutePassword("emdev2015");
           dg.setGlobalSearchPatternValue("dummydaemon*.sh");
           dg.setExecutableName(ExecutableName.BASH);
           dg.setHaPattern(HaPattern.AA);
           dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
           dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
           dg.setDaemons(dmlist);

           ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                   .clickPortalAdminButton().clickEcosystemConfiguration()
                   .clickDaemons();
           ManageDaemonGroupsPage p6=null;        
           if (p5.isExist(dg)) {
               p6 = p5.remove(dg);
           }else{
               p6=p5;
           }        
           ManageDaemonGroupsPage p8 = p6.add(dg);
//           p5.gotoPageNumber("2")
//           .gotoPreviousPage()
//           .gotoLastPage()
//           .gotoFirstPage()
//           .gotoNextPage();
           p8.close();
   
   }


   @Test
   public void test_add_daemon_BASH_FULLPATH() throws InterruptedException {
           
           Daemon dm = new Daemon();
           dm.setName("AA-BASH-FULLPATH");
           dm.setServer("angel");
           dm.setCurrentState(DaemonState.Active);

           List<Daemon> dmlist = new ArrayList<Daemon>();
           dmlist.add(dm);
          
           DaemonGroup dg = new DaemonGroup();
           dg.setName("DG-AA-BASH-FULLPATH");
           dg.setExecuteUser("root");
           dg.setExecutePassword("emdev2015");
           dg.setGlobalSearchPatternValue("/var/lib/dummydaemon.sh");
           dg.setExecutableName(ExecutableName.BASH);
           dg.setHaPattern(HaPattern.AA);
           dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
           dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
           dg.setDaemons(dmlist);

           ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                   .clickPortalAdminButton().clickEcosystemConfiguration()
                   .clickDaemons();
           ManageDaemonGroupsPage p6=null;        
           if (p5.isExist(dg)) {
               p6 = p5.remove(dg);
           }else{
               p6=p5;
           }        
           ManageDaemonGroupsPage p8 = p6.add(dg);
//           p5.gotoPageNumber("2")
//           .gotoPreviousPage()
//           .gotoLastPage()
//           .gotoFirstPage()
//           .gotoNextPage();
           p8.close();
    
   }
   
   @Test
   public void test_add_daemon_OTHER_CMDARGS() throws InterruptedException{
                
           Daemon dm = new Daemon();
           dm.setName("AA-OTHER-CMDARGS");
           dm.setServer("angel");
           dm.setCurrentState(DaemonState.Active);

           List<Daemon> dmlist = new ArrayList<Daemon>();
           dmlist.add(dm);
          
           DaemonGroup dg = new DaemonGroup();
           dg.setName("DG-AA-OTHER-CMDARGS");
           dg.setExecuteUser("root");
           dg.setExecutePassword("emdev2015");
           dg.setGlobalSearchPatternValue("dummydaemon.sh");
           dg.setExecutableName(ExecutableName.OTHER);
           dg.setHaPattern(HaPattern.AA);
           dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
           dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.EXECNAME);
           dg.setDaemons(dmlist);
           dg.setOtherInput("ThisIsTest");

           ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                   .clickPortalAdminButton().clickEcosystemConfiguration()
                   .clickDaemons();
           ManageDaemonGroupsPage p6=null;        
           if (p5.isExist(dg)) {
               p6 = p5.remove(dg);
           }else{
               p6=p5;
           }        
           ManageDaemonGroupsPage p8 = p6.add(dg);
//           p5.gotoPageNumber("2")
//           .gotoPreviousPage()
//           .gotoLastPage()
//           .gotoFirstPage()
//           .gotoNextPage();
           p8.close();
      
   }
   @Test
   public void test_add_daemon_OTHER_FULLPATH() throws InterruptedException {
              
           Daemon dm = new Daemon();
           dm.setName("AA-OTHER-FULLPATH");
           dm.setServer("angel");
           dm.setCurrentState(DaemonState.Active);

           List<Daemon> dmlist = new ArrayList<Daemon>();
           dmlist.add(dm);
          
           DaemonGroup dg = new DaemonGroup();
           dg.setName("DG-AA-OTHER-FULLPATH");
           dg.setExecuteUser("root");
           dg.setExecutePassword("emdev2015");
           dg.setGlobalSearchPatternValue("/var/lib/dummydaemon.sh");
           dg.setExecutableName(ExecutableName.OTHER);
           dg.setHaPattern(HaPattern.AA);
           dg.setStateChangeControlScript("/opt/teradata/client/em/bin/HAStateControlWrapperTemplate.sh");
           dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
           dg.setDaemons(dmlist);
           dg.setOtherInput("ThisIsTest");

           ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                   .clickPortalAdminButton().clickEcosystemConfiguration()
                   .clickDaemons();
           ManageDaemonGroupsPage p6=null;        
           if (p5.isExist(dg)) {
               p6 = p5.remove(dg);
           }else{
               p6=p5;
           }        
           ManageDaemonGroupsPage p8 = p6.add(dg);
//           p5.gotoPageNumber("2")
//           .gotoPreviousPage()
//           .gotoLastPage()
//           .gotoFirstPage()
//           .gotoNextPage();
           p8.close();
      
   }
   
   @Test
   public void test_add_action_rules_1() {
       Action a1 = new Action();
       a1.setName("email_me");
       List<Action> alist = new ArrayList<Action>();
       alist.add(a1);
       Rule r1 = new Rule();
       r1.setActions(alist);
       r1.setSeverity(Severity.Warning);

       Map<String, Rule> rules = new LinkedHashMap<String, Rule>();
       rules.put("Rules for Daemon Group", r1);
       rules.put("Rules for daemon on angel", r1);

       DaemonGroup dg = new DaemonGroup();
       dg.setName("DG-AA-OTHER-FULLPATH");
       
       ManageDaemonGroupsPage p5 = new LoginPage(d, u)
               .signIn()
               .clickPortalAdminButton()
               .clickEcosystemConfiguration()
               .clickDaemons();
       ActionRulesPage p6 = p5.updateActionRules(dg);
       p6.setRules(rules);
       p6.clickAddRules();
   }
   
   //ESM-44203
   @Test
   public void test_update_daemons() {
       //Step 1 add DaemonGroup
       Daemon dm = new Daemon();
       dm.setName("AA-SH-FULLPATH");
       dm.setServer("angel");
       dm.setCurrentState(DaemonState.Active);

       List<Daemon> dmlist = new ArrayList<Daemon>();
       dmlist.add(dm);

       DaemonGroup dg = new DaemonGroup();
       dg.setName("DG-AA-SH-FULLPATH");
       dg.setExecuteUser("root");
       dg.setExecutePassword("emdev2015");
       dg.setGlobalSearchPatternValue("/var/lib/dummydaemon.sh");
       dg.setExecutableName(ExecutableName.SH);
       dg.setHaPattern(HaPattern.AA);
       dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
       dg.setDaemons(dmlist);

       ManageDaemonGroupsPage p5 = new LoginPage(d, u)
               .signIn()
               .clickPortalAdminButton()
               .clickEcosystemConfiguration()
               .clickDaemons();
       ManageDaemonGroupsPage p6=null;        
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }else{
           p6=p5;
       }        
       ManageDaemonGroupsPage p8 = p6.add(dg);
       
       //Step2 update Daemons in DaemonGroup
       Daemon dm1 = new Daemon();
       dm1.setName("AA-SH-FULLPATH");
       dm1.setServer("xorn");
       dm1.setCurrentState(DaemonState.Active);
       dm1.setCustomSearchPatternValue("testSearchPattern");

       List<Daemon> dmlist1 = new ArrayList<Daemon>();
       dmlist1.add(dm1);
       dg.setDaemons(dmlist1);
       p8.update(dg);
       p8.close();
   }
   
   //ESM-44204
   @Test
   public void test_remove_daemons() {
       //Step 1 add DaemonGroup
       Daemon dm = new Daemon();
       dm.setName("RM-AA-SH-FULLPATH");
       dm.setServer("angel");
       dm.setCurrentState(DaemonState.Active);

       List<Daemon> dmlist = new ArrayList<Daemon>();
       dmlist.add(dm);

       DaemonGroup dg = new DaemonGroup();
       dg.setName("DG-RM-AA-SH-FULLPATH");
       dg.setExecuteUser("root");
       dg.setExecutePassword("emdev2015");
       dg.setGlobalSearchPatternValue("/var/lib/dummydaemon.sh");
       dg.setExecutableName(ExecutableName.SH);
       dg.setHaPattern(HaPattern.AA);
       dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
       dg.setDaemons(dmlist);

       ManageDaemonGroupsPage p5 = new LoginPage(d, u)
               .signIn()
               .clickPortalAdminButton()
               .clickEcosystemConfiguration()
               .clickDaemons();
       ManageDaemonGroupsPage p6=null;        
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }else{
           p6=p5;
       }        
       ManageDaemonGroupsPage p8 = p6.add(dg);
       
       if (p5.isExist(dg)) {
           p6 = p5.remove(dg);
       }
       
       p8.close();
   }

 



}
