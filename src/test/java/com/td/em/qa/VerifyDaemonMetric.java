/**
 * 
 */
package com.td.em.qa;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.td.em.qa.constants.DaemonState;
import com.td.em.qa.constants.ExecutableName;
import com.td.em.qa.constants.HaPattern;
import com.td.em.qa.constants.HaTransition;
import com.td.em.qa.constants.IdentifyDaemonsBy;
import com.td.em.qa.domain.Daemon;
import com.td.em.qa.domain.DaemonGroup;
import com.td.em.qa.pages.LoginPage;
import com.td.em.qa.pages.ManageDaemonGroupsPage;
import com.td.em.qa.pages.ViewpointPage;
import com.td.em.qa.pages.ee.EEDaemonListPage;
import com.td.em.qa.pages.ee.EEDaemonMetricPage;

/**
 * @author Jin Chen
 *
 */
public class VerifyDaemonMetric extends AbstractTestCaseBase {

    /*
     * (non-Javadoc)
     * 
     * @see com.td.em.qa.AbstractTestCaseBase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.td.em.qa.AbstractTestCaseBase#tearDown()
     */
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void test() {
        List<HaTransition> h = new ArrayList<HaTransition>();
        h.add(HaTransition.OOS2P);
        h.add(HaTransition.OOS2S);
        h.add(HaTransition.P2S);
        h.add(HaTransition.S2P);
        h.add(HaTransition.A2P);
        Daemon dm1 = new Daemon();
        dm1.setServer("xorn");
        dm1.setName("daemon12345");
        dm1.setCurrentState(DaemonState.Active);
        Daemon dm2 = new Daemon();
        dm2.setServer("angel");
        dm2.setName("daemon12345");
        dm2.setCurrentState(DaemonState.Standby);

        List<Daemon> dmlist = new ArrayList<Daemon>();
        dmlist.add(dm1);
        dmlist.add(dm2);
        DaemonGroup dg = new DaemonGroup();
        dg.setName("DGdaemon12345");
        dg.setExecuteUser("root");
        dg.setExecutePassword("TCAMPass123");
        dg.setGlobalSearchPatternValue("/var/lib/dummydaemon.sh");
        dg.setExecutableName(ExecutableName.SH);
        dg.setHaPattern(HaPattern.AS);
        dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
        dg.setDaemons(dmlist);
        dg.setValidHaTransitions(h);
        ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
                .clickPortalAdminButton().clickEcosystemConfiguration()
                .clickDaemons();
        ManageDaemonGroupsPage p6 = null;
        if (p5.isExist(dg)) {
            p6 = p5.remove(dg);
        } else {
            p6 = p5;
        }
        ManageDaemonGroupsPage p8 = p6.add(dg);
        ViewpointPage p9 = p8.close();
        EEDaemonListPage p10 = (EEDaemonListPage) p9.clickTabExplorer()
                .clickTab(Daemon.class);
        p10.portletOption("REFRESH");
        p10.portletOption("PAUSE");
        p10.setDaemonGroup(dg);
        //p10.changeDaemonState(dm1, DaemonState.Passive);
        EEDaemonMetricPage p11 = p10.clickDaemon(dm2);
        p11.verifyDaemon();
        p11.goBack();
    }

}
