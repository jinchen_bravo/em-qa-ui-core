/**
 * 
 */
package com.td.em.qa;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.td.em.qa.constants.DaemonState;
import com.td.em.qa.constants.ExecutableName;
import com.td.em.qa.constants.HaPattern;
import com.td.em.qa.constants.IdentifyDaemonsBy;
import com.td.em.qa.domain.Daemon;
import com.td.em.qa.domain.DaemonGroup;
import com.td.em.qa.pages.LoginPage;
import com.td.em.qa.pages.ManageDaemonGroupsPage;

/**
 * @author Jin Chen
 *
 */
public class UpdateDaemonsTest extends AbstractTestCaseBase {

    /* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    /* (non-Javadoc)
     * @see com.td.em.qa.AbstractTestCaseBase#tearDown()
     */
    @After
    public void tearDown() throws Exception {
        super.tearDown();
    }

    @Test
    public void test_update_daemons() {
        //Step 1 add DaemonGroup
        Daemon dm = new Daemon();
        dm.setName("demoTest1");
        dm.setServer("angel");
        dm.setCurrentState(DaemonState.Active);

        List<Daemon> dmlist = new ArrayList<Daemon>();
        dmlist.add(dm);

        DaemonGroup dg = new DaemonGroup();
        dg.setName("DemodaemonGroup1");
        dg.setExecuteUser("root");
        dg.setExecutePassword("emdev2015");
        dg.setGlobalSearchPatternValue("/bin/dash/myscript.sh");
        dg.setExecutableName(ExecutableName.SH);
        dg.setHaPattern(HaPattern.AA);
        dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
        dg.setDaemons(dmlist);

        ManageDaemonGroupsPage p5 = new LoginPage(d, u)
                .signIn()
                .clickPortalAdminButton()
                .clickEcosystemConfiguration()
                .clickDaemons();
        ManageDaemonGroupsPage p6=null;        
        if (p5.isExist(dg)) {
            p6 = p5.remove(dg);
        }else{
            p6=p5;
        }        
        ManageDaemonGroupsPage p8 = p6.add(dg);
        
        //Step2 update Daemons in DaemonGroup
        Daemon dm1 = new Daemon();
        dm1.setName("demoTest1");
        dm1.setServer("xorn");
        dm1.setCurrentState(DaemonState.Active);
        dm1.setCustomSearchPatternValue("testSearchPattern");

        List<Daemon> dmlist1 = new ArrayList<Daemon>();
        dmlist1.add(dm1);
        dg.setDaemons(dmlist1);
        p8.update(dg);
        p8.close();
    }

}
