package com.td.em.qa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.td.em.qa.constants.DaemonState;
import com.td.em.qa.constants.ExecutableName;
import com.td.em.qa.constants.HaPattern;
import com.td.em.qa.constants.IdentifyDaemonsBy;
import com.td.em.qa.domain.Daemon;
import com.td.em.qa.domain.DaemonGroup;
import com.td.em.qa.pages.LoginPage;
import com.td.em.qa.pages.ManageDaemonGroupsPage;

@RunWith(value = Parameterized.class)
public class DaemonDataDrivenTest extends AbstractTestCaseBase{
	
	
	  @Before
	    public void setUp() throws Exception {
	        super.setUp();
	        //ManageDaemonGroupsPage p5 = navigateToDaemons();
	    }

	    /* (non-Javadoc)
	     * @see com.td.em.qa.AbstractTestCaseBase#tearDown()
	     */
	    @After
	    public void tearDown() throws Exception {
	        super.tearDown();
	    }
	    
	    private String daemonName;
	    private String daemonGroupName;

	    @Parameters
	    public static Collection<Object[]> testData() {
	        return Arrays
	                .asList(new Object[][] { 
	                    { "daemon1", "daemonGroup1" },
	                    { "daemon2", "daemonGroup2" },
	                    { "daemon3", "daemonGroup3" },
	                    { "daemon4", "daemonGroup4" },
	                    { "daemon5", "daemonGroup5" },
	                    { "daemon6", "daemonGroup6" },
	                    { "daemon7", "daemonGroup7" },
	                    { "daemon8", "daemonGroup8" },
	                    { "daemon9", "daemonGroup9" },
	                    { "daemon10", "daemonGroup10" },
	                    { "daemon11", "daemonGroup11" },
	                    { "daemon12", "daemonGroup12" },
	                    { "daemon13", "daemonGroup13" },
	                    { "daemon14", "daemonGroup14" },
	                    { "daemon15", "daemonGroup15" },
	                    { "daemon16", "daemonGroup16" },
	                    { "daemon17", "daemonGroup17" },
	                    { "daemon18", "daemonGroup18" },
	                    { "daemon19", "daemonGroup19" },
	                    { "daemon20", "daemonGroup20" },
	                    { "daemon21", "daemonGroup21" },
	                    { "daemon22", "daemonGroup22" },
	                    { "daemon23", "daemonGroup23" },
	                    { "daemon24", "daemonGroup24" },
	                    { "daemon25", "daemonGroup25" },
	                    { "daemon26", "daemonGroup26" },
	                    { "daemon27", "daemonGroup27" },
	                    { "daemon28", "daemonGroup28" },
	                    { "daemon29", "daemonGroup29" },
	                    { "daemon30", "daemonGroup30" },
	                    { "daemon31", "daemonGroup31" },
	                    { "daemon32", "daemonGroup32" },
	                    { "daemon33", "daemonGroup33" },
	                    { "daemon34", "daemonGroup34" },
	                    { "daemon35", "daemonGroup35" },
	                    { "daemon36", "daemonGroup36" },
	                    { "daemon37", "daemonGroup37" },
	                    { "daemon38", "daemonGroup38" },
	                    { "daemon39", "daemonGroup39" },
	                    { "daemon40", "daemonGroup40" },
	                    { "daemon41", "daemonGroup41" },
	                    { "daemon42", "daemonGroup42" },
	                    { "daemon43", "daemonGroup43" },
	                    { "daemon44", "daemonGroup44" },
	                    { "daemon45", "daemonGroup45" },
	                    { "daemon46", "daemonGroup46" },
	                    { "daemon47", "daemonGroup47" },
	                    { "daemon48", "daemonGroup48" },
	                    { "daemon49", "daemonGroup49" },
	                    { "daemon50", "daemonGroup50" },
	                    { "daemon51", "daemonGroup51" },
	                });
	    }

	    public DaemonDataDrivenTest(String dnm, String dgnm) {
	        this.daemonName = dnm;
	        this.daemonGroupName = dgnm;
	    }

	    
	    @Test
	    public void test_data_driven(){

	        Daemon dm = new Daemon();
	        dm.setName(daemonName);
	        dm.setServer("angel");
	        dm.setCurrentState(DaemonState.Active);
	        List<Daemon> dmlist = new ArrayList<Daemon>();
	        dmlist.add(dm);
	        DaemonGroup dg = new DaemonGroup();
	        dg.setName(daemonGroupName);
	        dg.setExecuteUser("root");
	        dg.setExecutePassword("emdev2015");
	        dg.setGlobalSearchPatternValue("aaa");
	        dg.setExecutableName(ExecutableName.SH);
	        dg.setHaPattern(HaPattern.AA);
	        dg.setIdentifyDaemonsBy(IdentifyDaemonsBy.CMDARGS);
	        dg.setDaemons(dmlist);

	        ManageDaemonGroupsPage p5 = new LoginPage(d, u).signIn()
	                .clickPortalAdminButton().clickEcosystemConfiguration()
	                .clickDaemons();
	        ManageDaemonGroupsPage p6=null;        
	        if (p5.isExist(dg)) {
	            p6 = p5.remove(dg);
	        }else{
	            p6=p5;
	        }        
	        ManageDaemonGroupsPage p8 = p6.add(dg);
	        p8.close();
	    }


}
