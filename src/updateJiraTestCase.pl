#!/usr/bin/perl
#
# Tool to update test result for a particular test case
#
 
use SOAP::Lite;
use Data::Dumper;

# TODO - how do we hide these?

#if ($#ARGV != 3) {
#    print STDERR "Usage: perl updateJiraTestCase.pl <uid> <pwd> <case-id> " .
#                                                   "<pass|fail|untested>\n";
#    exit 1;
#}
my($userid) = $ARGV[0];
my($password) = $ARGV[1];
my($case) = $ARGV[2];
my($status) = $ARGV[3];
my($comment) = $ARGV[4];
if (($status !~ /^pass$/i) && ($status !~ /^fail$/i) &&
        ($status !~ /^untested$/i) && ($status !~ /^exempt$/i)) {
    print STDERR "Error: Invalid test result, must be pass, fail or untested\n";
    exit 1;
}
 
#
# Connect to the RPC SOAP proxy
#
my($soap) = SOAP::Lite->proxy(
    "http://continuum.td.teradata.com/jira/rpc/soap/jirasoapservice-v2?wsdl");
my($auth) = $soap->login(SOAP::Data->type(string => $userid),
                         SOAP::Data->type(string => $password));
die "Failed to connect to JIRA: " . $auth->faultstring if ($auth->faultcode);
Dumper($auth);

#
# Validate the current state of the test case
#
my($res) = $soap->getSubTaskIssueTypes($auth->result());
die "Failed to retrieve issue types: " . $res->faultstring if ($res->faultcode);

my($caseTypeId, $entry) = 0;
foreach $entry (@{$res->result}) {
    $caseTypeId = $entry->{"id"} + 0 if ($entry->{"name"} =~ /test case/i);
}

if ($caseTypeId == 0) {
    print STDERR "Error: Failed to retrieve test case identifier\n";
    exit 1;
}


my($res) = $soap->getIssue($auth->result(), 
                           SOAP::Data->type(string => $case));
die "Failed to retrieve issue data: " . $res->faultstring if ($res->faultcode);
if ($res->result->{"type"} != $caseTypeId) {
    print STDERR "Error: JIRA issue identifier $case is not a Test Case\n";
    exit 1;
}
my($stat, $currentStatus) = $res->result->{"status"};
my($res) = $soap->getStatuses($auth->result());
die "Failed to retrieve status list: " . $res->faultstring if ($res->faultcode);
foreach $entry (@{$res->result}) {
    $currentStatus = $entry->{"name"} if ($entry->{"id"} == $stat);
}
if (!defined $currentStatus) {
    print STDERR "Error: Unable to determine current case status\n";
    exit 1;
}

#
# Find the target action
#
my($res) = $soap->getAvailableActions($auth->result(), 
                                      SOAP::Data->type(string => $case));
die "Failed to get action list: " . $res->faultstring if ($res->faultcode);
my($actionId) = 0;
foreach $entry (@{$res->result}) {
   $actionId = $entry->{"id"} + 0 if ($entry->{"name"} =~ /$status/i);
   #print STDOUT "actionID:" . $actionId . "\n";
   #print STDOUT "entryID:" . $entry->{"author"} . "\n";
   #print STDOUT "entryName:" . $entry->{"name"} . " ----- " . $status . "\n";
}
if ($actionId == 0) {
    # Perhaps it's already in the target state
    if ($currentStatus !~ /$status/i) {
        print STDERR "Error: Failed to retrieve result action for $status\n";
        exit 1;
    }
} else {
    # Change the state
    my($res) = $soap->progressWorkflowAction($auth->result(), 
                                  SOAP::Data->type(string => $case),
                                  SOAP::Data->type(string => "$actionId"));
    die "Failed to change state: " . $res->faultstring if ($res->faultcode);
	if(!$res->faultcode)
	{
		print $case . " changed to " . $status;
	}
}
# Remove the following to generate a comment onto the test case

#
# Update/add the comment
#
#$status = $status . "ed" if (($status =~ /pass/i) || ($status =~ /fail/i));
#my($comment) = "Autotest: test case $status on " . localtime;
#my($res) = $soap->getComments($auth->result(), 
#                              SOAP::Data->type(string => $case));
#die "Failed to read comments: " . $res->faultstring if ($res->faultcode);
#my($commentId) = 0;
#foreach $entry (@{$res->result}) {
#   $commentId = $entry->{"id"} + 0 if ($entry->{"body"} =~ /^Autotest:/i);
#}
if ($comment !=~ //i) {
    my($res) = $soap->addComment($auth->result(),
                                 SOAP::Data->type(string => $case),
                                 SOAP::Data->type('RemoteComment' =>
                                                       {'body' => $comment}));
} 
#else {
#    my($res) = $soap->editComment($auth->result(),
#                                  SOAP::Data->type('RemoteComment' =>
#                                                       {'body' => $comment,
#                                                        'id' => $commentId }));
#}
die "Failed to update comment: " . $res->faultstring if ($res->faultcode);
exit;