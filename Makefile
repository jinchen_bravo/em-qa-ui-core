clean :
	rm -rf .settings .gradle build bin .project .classpath
cleanEclipse :
	gradle cleanEclipse
eclipse :
	gradle eclipse
publish :
	gradle -b build.gradle.artifactory clean publish
