# building Web UI test cases using Java/Selenium for ViewPoint #

- Install Groovy plugin for Eclipse
- Install Buildship gradle plugin for Eclipse
- Install make utility ( install command line tool for Mac OS X)
- Import into eclipse as a gradle project
- Provide gradle.properties file

before using artifactory build file (build.gradle.artifactory), download gradle.properties from artifactory server and put into the project directory

- sample contents of gradle.properties

```
artifactory_user=xx111000
artifactory_password=AKCp5aUF3qrU2NKaJeyD2kN6YBdp2bBaMB25b5Yh8vwUQk7aRvR6fYA3Dx8cxtY2ANbPoxfBo
artifactory_contextUrl=https://sdartifact.td.teradata.com:443/artifactory
```

- publish to Artifactory

```
make publish
```
- generate eclipse project files

```
make eclipse
```